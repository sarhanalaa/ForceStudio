﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForceLib
{

    namespace ForceLib3D
    {
        // Enumerations
        public enum EActingLevel { WORLD, OBJECT };
        //-------------------------

        public interface I3dForce
        {

            // Properties

            /// <summary>
            /// The Magnitude Vector of the World Relative to Point of Act.
            /// </summary>
            Vector3 Magnitude { get; set; }

            /// <summary>
            /// The Offset of the Point of Act Relative to Center of Mass.
            /// </summary>
            Vector3 Offset { get; set; }

            /// <summary>
            /// The Affected Object by the Force. It's set to null if the Acting Frame
            /// of the Force is set to EActingFrame.WORLD.
            /// </summary>
            I3dObject AffObject { get; set;  }

            /// <summary>
            /// Indicates the level that the force acts in.
            /// </summary>
            EActingLevel ActingLevel { get; }

            /// <summary>
            /// Reference to World that the Force Acts in.
            /// </summary>
            I3dWorld World { get; }

            ///////////////////////////////////


            // Operations

            /// <summary>
            /// Modifies the Force.
            /// </summary>
            void Modify();

            /// <summary>
            /// Resets the Offset to Zero.
            /// </summary>
            void ResetOffset();

            /// <summary>
            /// Sets the Offset.
            /// </summary>
            /// <param name="offset"></param>
            void SetOffset(Vector3 offset);

            ///////////////////////////////////
        }

    }

}
