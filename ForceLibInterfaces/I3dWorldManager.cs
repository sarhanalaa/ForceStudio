﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ForceLib
{
    
    namespace ForceLib3D
    {

        public interface I3dWorldManager
        {

            // Properties

            /// <summary>
            /// The World's Cycle Duration in seconds.
            /// </summary>
            float CycleDuration { get; set; }

            /// <summary>
            /// Objects of the World.
            /// </summary>
            List<I3dObject> AllObjects { get; set; }

            /// <summary>
            /// World Forces of the World.
            /// </summary>
            List<I3dForce> WorldForces { get; set; }

            /// <summary>
            /// Reference to the World that is Managed with this instance.
            /// </summary>
            I3dWorld World { get; }

            ////////////////////////////////////////////


            // Operations

            /// <summary>
            /// Initializes the World Manager and Return the World to it's Initial State.
            /// </summary>
            void Initialize();

            /// <summary>
            /// Handles the Operations Done in each World Cycle.
            /// </summary>
            /// <param name="cycle">Cycle Number</param>
            void Cycle(long cycle);

            /// <summary>
            /// Seeks the world state and objects to a specified cycle
            /// </summary>
            /// <param name="c">the cycle to seek to</param>
            void Seek(long c);

            ////////////////////////////////////////////

        }

    }

}
