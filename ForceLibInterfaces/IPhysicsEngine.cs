﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForceLib
{
    public interface IPhysicsEngine <OT,FT>
    {

        // Properties

        /// <summary>
        /// Gets the count of Physical Laws in the Engine
        /// </summary>
        int Count { get; }

        //****************************


        // Methods

        /// <summary>
        /// Checks whether a physical law was added to the engine or not.
        /// </summary>
        /// <param name="law">physical law to check</param>
        /// <returns>true if the law found, false if not.</returns>
        bool HasLaw(IPhysicalLaw<OT,FT> law);

        /// <summary>
        /// Adds a physical law to the engine.
        /// </summary>
        /// <param name="law">physical law to add</param>
        /// <returns>true on success, false on failure.</returns>
        bool AddLaw(IPhysicalLaw<OT, FT> law);

        /// <summary>
        /// Removes a physical law from the engine
        /// </summary>
        /// <param name="law">physical law to remove</param>
        /// <returns>true on success, false on failure.</returns>
        bool RemoveLaw(IPhysicalLaw<OT, FT> law);

        /// <summary>
        /// Applies the Physical Laws in the Engine on the Objects in the Objects' list
        /// Referece using the World Forces and the Object's Forces.
        /// </summary>
        /// <param name="objectRef">Reference to the Object to Apply the physics on.</param>
        /// <param name="excludedLaws">Excluded Laws that won't be applied.</param>
        void ApplyPhysics(OT objectRef, IPhysicalLaw<OT,FT>[] excludedLaws);

        //****************************

    }
}
