﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForceLib
{
    public interface IAnalyzable
    {

        // Properties

        /// <summary>
        /// Name of the Instance.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// Gets the State History of the Object
        /// </summary>
        Dictionary<long, Dictionary<String, Object>> StateHistory { get; }

        /// <summary>
        /// Array of State History Keys which can be viewed as Histogram Graph Lines.
        /// </summary>
        String[] HistogramElements { get; }
        //***************************


        // Methods

        /// <summary>
        /// Generates the History Entry based on the current object's state.
        /// </summary>
        /// <returns>Current State History Entry</returns>
        Dictionary<String, Object> GenerateCurrentState();

        /// <summary>
        /// Adds an entry of specified cycle to the History.
        /// </summary>
        /// <param name="cycle">Cycle Number which the Entry was generateds in.</param>
        /// <param name="state">State History Entry</param>
        void AddState(long cycle, Dictionary<String, Object> state);

        /// <summary>
        /// Sets the Object state to it's state in a specified cycle
        /// </summary>
        /// <param name="cycle">the cycle in which the state will be generated</param>
        void SetState(long cycle);

        /// <summary>
        /// Queries the object's state for its state in a specified cycle.
        /// </summary>
        /// <param name="cycle">The cycle to query the state in.</param>
        /// <returns>String Describing the queried state of the object.</returns>
        String QueryState(long cycle);

        //***************************

    }
}
