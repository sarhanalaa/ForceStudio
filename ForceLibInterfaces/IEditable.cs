﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ForceLib
{
    public interface IEditable
    {

        // Properties
        object[] EditingInfo { get; set; }
        //-----------------------


        // Methods

        Panel GenerateEditingPanel(object[] info);

        //-----------------------

    }
}
