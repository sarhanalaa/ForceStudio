﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib.ForceLib3D;

namespace ForceLib
{
    public interface IRenderable
    {

        // Properties

        //***********************


        // Methods

        /// <summary>
        /// Generates the Primitives used in rendering to simulation view.
        /// </summary>
        /// <returns>Rendering Primitives</returns>
        Primitive3[] Render();
        
        //***********************

    }
}
