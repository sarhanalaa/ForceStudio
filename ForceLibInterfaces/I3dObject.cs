﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForceLib
{

    namespace ForceLib3D
    {

        public interface I3dObject
        {
            
            // Prperties

            /// <summary>
            /// The Mass of the Object.
            /// </summary>
            float Mass { get; set; }

            /// <summary>
            /// The Center of Mass Position Relative to the Center of the World.
            /// </summary>
            Vector3 Position { get; set; }

            /// <summary>
            /// The Vertices of the object.
            /// </summary>
            Vertex3[] Vertices { get; set; }

            /// <summary>
            /// Linear Velocity of the Object.
            /// </summary>
            Vector3 Velocity { get; set; }

            /// <summary>
            /// Linear Acceleration of the Object.
            /// </summary>
            Vector3 Acceleration { get; set; }

            /// <summary>
            /// Angular Velocity of the Object.
            /// </summary>
            Vector3 AngVelocity { get; set; }

            /// <summary>
            /// Angular Acceleration of the Object.
            /// </summary>
            Vector3 AngAcceleration { get; set; }

            /// <summary>
            /// Spin Velocity of the Object.
            /// </summary>
            Vector3 SpinVelocity { get; set; }

            /// <summary>
            /// Indicates whether the object is hanget to a point { Going in Circular motions arround it }
            /// or not.
            /// </summary>
            bool Hanged { get; }

            /// <summary>
            /// Gets the Hang Point Coord. in a hanged object.
            /// </summary>
            Vector3 HangPoint { get; }

            /// <summary>
            /// List of Forces Acting on the Object which it broughts in its implementation.
            /// </summary>
            List<I3dForce> Forces { get; set; }

            /// <summary>
            /// Reference to the World that the Object Exists in.
            /// </summary>
            I3dWorld World { get; }

            ///////////////////////////////////


            // Operations

            /// <summary>
            /// Transforms the Object by a given transformation Matrix.
            /// </summary>
            /// <param name="transMatrix">The Transformation Matrix.</param>
            void Transform(Matrix transMatrix);

            /// <summary>
            /// Gets the Area of The Object Meshes that are facing the given normal vector.
            /// </summary>
            /// <param name="normal">The Facing Vector.</param>
            /// <returns>Face Area.</returns>
            float GetFaceArea(Vector3 normal);

            /// <summary>
            /// Gets the Drag Coefficient of the Object Facing a given normal vector.
            /// </summary>
            /// <param name="normal">The Facing Vector.</param>
            /// <returns>Drag Coefficient.</returns>
            float GetDragCoefficient(Vector3 normal);

            /// <summary>
            /// Gets the Objects Inertia.
            /// </summary>
            /// <returns>Object's Inertia</returns>
            float GetInertia();

            /// <summary>
            /// Gets the Inertia Tensor.
            /// </summary>
            /// <returns>Inertia Tensor of the Object</returns>
            Matrix GetInertiaTensor();

            /// <summary>
            /// Hangs the object to a specified point.
            /// </summary>
            /// <param name="hangPoint">The coord. of the hang point</param>
            void Hang(Vector3 hangPoint);

            /// <summary>
            /// Unhangs a hanged object.
            /// </summary>
            void UnHang();

            ///////////////////////////////////
        }

    }

}
