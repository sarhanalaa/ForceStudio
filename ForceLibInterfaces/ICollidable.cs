﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;

namespace ForceLib
{
    public interface ICollidable
    {

        // Properties

        /// <summary>
        /// Gets or Sets the Velocity Vector of the Object
        /// </summary>
        Vector3 colV { get; set; }

        /// <summary>
        /// Gets or Sets the Spin Velocity of the Object.
        /// </summary>
        Vector3 colW { get; set; }

        /// <summary>
        /// Gets the Position Vector of the Object
        /// </summary>
        Vector3 colP { get; set; }

        /// <summary>
        /// Gets the Radius of the Sphere that contains the whole Object
        /// </summary>
        float colR { get; }

        /// <summary>
        /// Gets the Mass of the Object.
        /// </summary>
        float colM { get; }

        /// <summary>
        /// Gets the Inversed Inertia Tensor of the Object.
        /// </summary>
        Matrix colIinverse { get; }

        //------------------------------

    }
}
