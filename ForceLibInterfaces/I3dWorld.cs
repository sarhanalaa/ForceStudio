﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForceLib
{

    namespace ForceLib3D
    {

        public interface I3dWorld
        {
            
            // Properties

            /// <summary>
            /// The Viscosity of the material that fills the world.
            /// </summary>
            float Viscosity { get; set; }

            /// <summary>
            /// Vector Representing The Center of the World.
            /// </summary>
            Vector3 WorldCenter { get; set; }

            /// <summary>
            /// Gravity Acceleration.
            /// </summary>
            float Gravity { get; set; }

            /// <summary>
            /// The Temprature of the World.
            /// </summary>
            float Temprature { get; set; }
        }
    }

}
