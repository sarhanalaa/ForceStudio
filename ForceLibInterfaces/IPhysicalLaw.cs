﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ForceLib
{
    public interface IPhysicalLaw<OT,FT>
    {

        // Properties

        /// <summary>
        /// Gets or Sets Law's Additional Info Objects.
        /// </summary>
        Object[] LawInfo { get; set; }

        //***********************


        // Methods

        /// <summary>
        /// Applies the Law on an Object using the World and Object's Forces.
        /// </summary>
        /// <param name="objectRef">The Object to apply the law on.</param>
        void ApplyLaw(OT[] objectRef);

        //***********************

    }
}
