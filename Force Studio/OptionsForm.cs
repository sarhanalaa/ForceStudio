﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharpGL;

namespace Force_Studio
{
    public partial class OptionsForm : Form
    {
        //*********************Attributes*****************
        private Simulator simulator;
        private Timer label5_timer;
        //************************************************
        //------------------------------------------------


        //*********************Priorties******************
        public Simulator Simulator
        {
            get { return this.simulator; }
            set { this.simulator = value; }
        }

        public Timer Label5_Timer
        {
            get { return this.label5_timer; }
            set { this.label5_timer = value; }
        }

        //*************************************************
        //-------------------------------------------------


        //*******************Constructor*******************
        public OptionsForm(Simulator simulator)
        {
            InitializeComponent();
            this.simulator = simulator;
            this.label5_timer = new Timer();
            this.label5_timer.Interval = 2000;
            this.label5_timer.Tick += new EventHandler(label5_timer_Tick);
        }


        //**************************************************
        //-------------------------------------------
        void label5_timer_Tick(object sender, EventArgs e)
        {
            this.label5.Text = "";
            this.label5_timer.Stop();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }


        private void OptionsForm_Load(object sender, EventArgs e)
        {

        }




        //****************************fields*************************



        private void RedBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.simulator.GL.ClearColor((float)this.RedBox.SelectedItem, (float)this.GreenBox.SelectedItem, (float)this.BlueBox.SelectedItem, 0f);
        }

        private void GreenBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.simulator.GL.ClearColor((float)this.RedBox.SelectedItem, (float)this.GreenBox.SelectedItem, (float)this.BlueBox.SelectedItem, 0f);
        }

        private void BlueBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.simulator.GL.ClearColor((float)this.RedBox.SelectedItem, (float)this.GreenBox.SelectedItem, (float)this.BlueBox.SelectedItem, 0f);
        }

        private void ObjectsBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.ObjectsBox.Checked)
            {
                if (!this.simulator.AddItemToDraw(Simulator.DrawItems.OBJECTS))
                {
                    this.ObjectsBox.Checked = false;
                    this.label5.Text = "No Objects Was Found";
                    this.label5_timer.Start();
                }
            }
            else
                this.simulator.RemoveItemToDraw(Simulator.DrawItems.OBJECTS);
        }

        private void ForcesBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.ForcesBox.Checked)
            {
                if (!this.simulator.AddItemToDraw(Simulator.DrawItems.FORCES))
                {
                    this.ForcesBox.Checked = false;
                    this.label5.Text = "No Forces Was Found";
                    this.label5_timer.Stop();
                    this.label5_timer.Start();
                }
            }
            else
                this.simulator.RemoveItemToDraw(Simulator.DrawItems.FORCES);
        }

        private void VelocitiesBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.VelocitiesBox.Checked)
            {
                if (!this.simulator.AddItemToDraw(Simulator.DrawItems.VELOCITIES))
                {
                    this.VelocitiesBox.Checked = false;
                    this.label5.Text = "No Velocities Was Found";
                    this.label5_timer.Stop();
                    this.label5_timer.Start();
                }

            }
            else
                this.simulator.RemoveItemToDraw(Simulator.DrawItems.VELOCITIES);
        }

        private void AccBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.AccBox.Checked)
            {
                if (!this.simulator.AddItemToDraw(Simulator.DrawItems.ACCELERATIONS))
                {
                    this.AccBox.Checked = false;
                    this.label5.Text = "No Accelerations Was Found";
                    this.label5_timer.Stop();
                    this.label5_timer.Start();
                }
            }
            else
                this.simulator.RemoveItemToDraw(Simulator.DrawItems.ACCELERATIONS);
        }

        private void AngVelBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.AngVelBox.Checked)
            {
                if (!this.simulator.AddItemToDraw(Simulator.DrawItems.ANGVELOCITIES))
                {
                    this.AngVelBox.Checked = false;
                    this.label5.Text = "No Angular Velocities Was Found";
                    this.label5_timer.Stop();
                    this.label5_timer.Start();
                }
            }
            else
                this.simulator.RemoveItemToDraw(Simulator.DrawItems.ANGVELOCITIES);
        }

        private void AngAccBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.AngAccBox.Checked)
            {
                if (!this.simulator.AddItemToDraw(Simulator.DrawItems.ANGACCELERATIONS))
                {
                    this.AngAccBox.Checked = false;
                    this.label5.Text = "No Angular Accelerationse Was Found";
                    this.label5_timer.Stop();
                    this.label5_timer.Start();
                }
            }
            else
                this.simulator.RemoveItemToDraw(Simulator.DrawItems.ANGACCELERATIONS);
        }

        private void FrameRateBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.simulator.GL_CTRL.FrameRate = (float)this.FrameRateBox.SelectedItem;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox1.Checked)
            {
                this.simulator.DrawStyle = OpenGL.GLU_LINE;
            }
            else
            {
                this.simulator.DrawStyle = OpenGL.GLU_FILL;
            }
        }

        private void OptionsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }














    }
}