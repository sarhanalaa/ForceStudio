﻿using SharpGL;

namespace Force_Studio
{
    partial class MainAppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainAppForm));
            this.simulatorView = new SharpGL.OpenGLCtrl();
            this.ViewPort2 = new System.Windows.Forms.Panel();
            this.MainAppMenu = new System.Windows.Forms.MenuStrip();
            this.MainMenu_NewExp = new System.Windows.Forms.ToolStripMenuItem();
            this.newExpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_OpenExp = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_SaveExp = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_SaveExpAs = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu_CloseExp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.preferencesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.initialStateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.histogramToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.simulatorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.physicsEngineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.objectsForcesManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forcesManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newExpirementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expirementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openExpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveExpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveExpAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeExpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.expSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stateAnalyzerPreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.histogramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simulatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.physicsEnginePreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.worldManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutForceStudioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpContentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tutorialsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.visitForceStudioWebsiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forceStudionetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forceLibnetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MainAppBottomPanel = new System.Windows.Forms.Panel();
            this.ControlerCycleSlider = new System.Windows.Forms.TrackBar();
            this.ControlerToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.ControlerExpLength = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel7 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.ControlerCyclesNum = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ControlerPreviousCycle = new System.Windows.Forms.ToolStripButton();
            this.ControlerStartStop = new System.Windows.Forms.ToolStripButton();
            this.ControlerNextCycle = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel8 = new System.Windows.Forms.ToolStripLabel();
            this.ControlerCurrentCycle = new System.Windows.Forms.ToolStripLabel();
            this.MainAppTopPanel = new System.Windows.Forms.Panel();
            this.ObjectsToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.MainAppLeftPanel = new System.Windows.Forms.Panel();
            this.Exp_Elements = new System.Windows.Forms.GroupBox();
            this.StateHistory_EditObject = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.Exp_EditForce = new System.Windows.Forms.Button();
            this.Exp_ForcesList = new System.Windows.Forms.ListBox();
            this.Exp_RemoveObject = new System.Windows.Forms.Button();
            this.Exp_NewObject = new System.Windows.Forms.Button();
            this.Exp_ObjectsList = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ProcessControl = new System.Windows.Forms.Button();
            this.ExpInfo_ProcessProgress = new System.Windows.Forms.ProgressBar();
            this.ExpInfo_TotalCyclesNumber = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ExpInfo_ProcessedCycles = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ExpInfo_ProcessState = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ExpInfo_ExpCPS = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ExpInfo_ExpLength = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.MainAppViewPort = new System.Windows.Forms.TableLayoutPanel();
            this.ViewPort1 = new System.Windows.Forms.Panel();
            this.StateHistory = new System.Windows.Forms.GroupBox();
            this.HistogramView = new System.Windows.Forms.Panel();
            this.Histogram_TimeScroll = new System.Windows.Forms.HScrollBar();
            this.Histogram_ToolStrip = new System.Windows.Forms.ToolStrip();
            this.Histogram_Elements = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.Histogram_ZoomIn = new System.Windows.Forms.ToolStripButton();
            this.Histogram_ZoomOut = new System.Windows.Forms.ToolStripButton();
            this.label9 = new System.Windows.Forms.Label();
            this.StateHistory_CycleTime = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.StateHistory_StateViewer = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.StateHistory_Cycle = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.StateHistory_ObjectsList = new System.Windows.Forms.ComboBox();
            this.NewObjectMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.newSphere = new System.Windows.Forms.ToolStripMenuItem();
            this.newPendulum = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewPort2.SuspendLayout();
            this.MainAppMenu.SuspendLayout();
            this.MainAppBottomPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ControlerCycleSlider)).BeginInit();
            this.ControlerToolStrip.SuspendLayout();
            this.MainAppTopPanel.SuspendLayout();
            this.MainAppLeftPanel.SuspendLayout();
            this.Exp_Elements.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.MainAppViewPort.SuspendLayout();
            this.ViewPort1.SuspendLayout();
            this.StateHistory.SuspendLayout();
            this.HistogramView.SuspendLayout();
            this.Histogram_ToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StateHistory_Cycle)).BeginInit();
            this.NewObjectMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // simulatorView
            // 
            this.simulatorView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.simulatorView.DrawRenderTime = false;
            this.simulatorView.FrameRate = 24.39024F;
            this.simulatorView.GDIEnabled = false;
            this.simulatorView.Location = new System.Drawing.Point(0, 0);
            this.simulatorView.Name = "simulatorView";
            this.simulatorView.Size = new System.Drawing.Size(400, 400);
            this.simulatorView.TabIndex = 0;
            // 
            // ViewPort2
            // 
            this.ViewPort2.Controls.Add(this.simulatorView);
            this.ViewPort2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ViewPort2.Location = new System.Drawing.Point(285, 4);
            this.ViewPort2.Name = "ViewPort2";
            this.ViewPort2.Size = new System.Drawing.Size(404, 446);
            this.ViewPort2.TabIndex = 1;
            // 
            // MainAppMenu
            // 
            this.MainAppMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MainMenu_NewExp,
            this.preferencesToolStripMenuItem1,
            this.helpToolStripMenuItem1});
            this.MainAppMenu.Location = new System.Drawing.Point(0, 0);
            this.MainAppMenu.Name = "MainAppMenu";
            this.MainAppMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.MainAppMenu.Size = new System.Drawing.Size(872, 24);
            this.MainAppMenu.TabIndex = 0;
            this.MainAppMenu.Text = "menuStrip1";
            // 
            // MainMenu_NewExp
            // 
            this.MainMenu_NewExp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newExpToolStripMenuItem,
            this.MainMenu_OpenExp,
            this.MainMenu_SaveExp,
            this.MainMenu_SaveExpAs,
            this.MainMenu_CloseExp,
            this.toolStripSeparator4});
            this.MainMenu_NewExp.Name = "MainMenu_NewExp";
            this.MainMenu_NewExp.Size = new System.Drawing.Size(73, 20);
            this.MainMenu_NewExp.Text = "Experiment";
            // 
            // newExpToolStripMenuItem
            // 
            this.newExpToolStripMenuItem.Name = "newExpToolStripMenuItem";
            this.newExpToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.newExpToolStripMenuItem.Text = "New Exp.";
            this.newExpToolStripMenuItem.Click += new System.EventHandler(this.newExpToolStripMenuItem_Click);
            // 
            // MainMenu_OpenExp
            // 
            this.MainMenu_OpenExp.Enabled = false;
            this.MainMenu_OpenExp.Name = "MainMenu_OpenExp";
            this.MainMenu_OpenExp.Size = new System.Drawing.Size(150, 22);
            this.MainMenu_OpenExp.Text = "Open Exp...";
            // 
            // MainMenu_SaveExp
            // 
            this.MainMenu_SaveExp.Enabled = false;
            this.MainMenu_SaveExp.Name = "MainMenu_SaveExp";
            this.MainMenu_SaveExp.Size = new System.Drawing.Size(150, 22);
            this.MainMenu_SaveExp.Text = "Save Exp...";
            // 
            // MainMenu_SaveExpAs
            // 
            this.MainMenu_SaveExpAs.Enabled = false;
            this.MainMenu_SaveExpAs.Name = "MainMenu_SaveExpAs";
            this.MainMenu_SaveExpAs.Size = new System.Drawing.Size(150, 22);
            this.MainMenu_SaveExpAs.Text = "Save Exp. As...";
            // 
            // MainMenu_CloseExp
            // 
            this.MainMenu_CloseExp.Name = "MainMenu_CloseExp";
            this.MainMenu_CloseExp.Size = new System.Drawing.Size(150, 22);
            this.MainMenu_CloseExp.Text = "Exit";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(147, 6);
            // 
            // preferencesToolStripMenuItem1
            // 
            this.preferencesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.initialStateToolStripMenuItem,
            this.histogramToolStripMenuItem1,
            this.simulatorToolStripMenuItem1,
            this.toolStripSeparator7,
            this.physicsEngineToolStripMenuItem,
            this.objectsForcesManagerToolStripMenuItem,
            this.forcesManagerToolStripMenuItem,
            this.toolStripMenuItem1});
            this.preferencesToolStripMenuItem1.Name = "preferencesToolStripMenuItem1";
            this.preferencesToolStripMenuItem1.Size = new System.Drawing.Size(77, 20);
            this.preferencesToolStripMenuItem1.Text = "Preferences";
            // 
            // initialStateToolStripMenuItem
            // 
            this.initialStateToolStripMenuItem.Name = "initialStateToolStripMenuItem";
            this.initialStateToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.initialStateToolStripMenuItem.Text = "Initial State...";
            // 
            // histogramToolStripMenuItem1
            // 
            this.histogramToolStripMenuItem1.Name = "histogramToolStripMenuItem1";
            this.histogramToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.histogramToolStripMenuItem1.Text = "Histogram...";
            // 
            // simulatorToolStripMenuItem1
            // 
            this.simulatorToolStripMenuItem1.Name = "simulatorToolStripMenuItem1";
            this.simulatorToolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.simulatorToolStripMenuItem1.Text = "Simulator...";
            this.simulatorToolStripMenuItem1.Click += new System.EventHandler(this.simulatorToolStripMenuItem1_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(165, 6);
            // 
            // physicsEngineToolStripMenuItem
            // 
            this.physicsEngineToolStripMenuItem.Name = "physicsEngineToolStripMenuItem";
            this.physicsEngineToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.physicsEngineToolStripMenuItem.Text = "Physics Engine...";
            // 
            // objectsForcesManagerToolStripMenuItem
            // 
            this.objectsForcesManagerToolStripMenuItem.Name = "objectsForcesManagerToolStripMenuItem";
            this.objectsForcesManagerToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.objectsForcesManagerToolStripMenuItem.Text = "Objects Manager...";
            // 
            // forcesManagerToolStripMenuItem
            // 
            this.forcesManagerToolStripMenuItem.Name = "forcesManagerToolStripMenuItem";
            this.forcesManagerToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.forcesManagerToolStripMenuItem.Text = "Forces Manager...";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(168, 22);
            this.toolStripMenuItem1.Text = "World Variables...";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem1.Text = "Help";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newExpirementToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newExpirementToolStripMenuItem
            // 
            this.newExpirementToolStripMenuItem.Name = "newExpirementToolStripMenuItem";
            this.newExpirementToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.newExpirementToolStripMenuItem.Text = "Exit";
            // 
            // expirementToolStripMenuItem
            // 
            this.expirementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openExpToolStripMenuItem,
            this.saveExpToolStripMenuItem,
            this.saveExpAsToolStripMenuItem,
            this.closeExpToolStripMenuItem,
            this.toolStripSeparator1,
            this.expSettingsToolStripMenuItem});
            this.expirementToolStripMenuItem.Name = "expirementToolStripMenuItem";
            this.expirementToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.expirementToolStripMenuItem.Text = "Expirement";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.newToolStripMenuItem.Text = "New Exp.";
            // 
            // openExpToolStripMenuItem
            // 
            this.openExpToolStripMenuItem.Name = "openExpToolStripMenuItem";
            this.openExpToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.openExpToolStripMenuItem.Text = "Open Exp. ...";
            // 
            // saveExpToolStripMenuItem
            // 
            this.saveExpToolStripMenuItem.Name = "saveExpToolStripMenuItem";
            this.saveExpToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.saveExpToolStripMenuItem.Text = "Save Exp.";
            // 
            // saveExpAsToolStripMenuItem
            // 
            this.saveExpAsToolStripMenuItem.Name = "saveExpAsToolStripMenuItem";
            this.saveExpAsToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.saveExpAsToolStripMenuItem.Text = "Save Exp. As...";
            // 
            // closeExpToolStripMenuItem
            // 
            this.closeExpToolStripMenuItem.Name = "closeExpToolStripMenuItem";
            this.closeExpToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.closeExpToolStripMenuItem.Text = "Close Exp.";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(147, 6);
            // 
            // expSettingsToolStripMenuItem
            // 
            this.expSettingsToolStripMenuItem.Name = "expSettingsToolStripMenuItem";
            this.expSettingsToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.expSettingsToolStripMenuItem.Text = "Exp. Settings";
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stateAnalyzerPreferencesToolStripMenuItem,
            this.physicsEnginePreferencesToolStripMenuItem,
            this.worldManagerToolStripMenuItem});
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.preferencesToolStripMenuItem.Text = "Preferences";
            // 
            // stateAnalyzerPreferencesToolStripMenuItem
            // 
            this.stateAnalyzerPreferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalToolStripMenuItem,
            this.histogramToolStripMenuItem,
            this.simulatorToolStripMenuItem});
            this.stateAnalyzerPreferencesToolStripMenuItem.Name = "stateAnalyzerPreferencesToolStripMenuItem";
            this.stateAnalyzerPreferencesToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.stateAnalyzerPreferencesToolStripMenuItem.Text = "State Analyzer";
            // 
            // generalToolStripMenuItem
            // 
            this.generalToolStripMenuItem.Name = "generalToolStripMenuItem";
            this.generalToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.generalToolStripMenuItem.Text = "General...";
            // 
            // histogramToolStripMenuItem
            // 
            this.histogramToolStripMenuItem.Name = "histogramToolStripMenuItem";
            this.histogramToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.histogramToolStripMenuItem.Text = "Histogram...";
            // 
            // simulatorToolStripMenuItem
            // 
            this.simulatorToolStripMenuItem.Name = "simulatorToolStripMenuItem";
            this.simulatorToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.simulatorToolStripMenuItem.Text = "Simulator...";
            // 
            // physicsEnginePreferencesToolStripMenuItem
            // 
            this.physicsEnginePreferencesToolStripMenuItem.Name = "physicsEnginePreferencesToolStripMenuItem";
            this.physicsEnginePreferencesToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.physicsEnginePreferencesToolStripMenuItem.Text = "Physics Engine...";
            // 
            // worldManagerToolStripMenuItem
            // 
            this.worldManagerToolStripMenuItem.Name = "worldManagerToolStripMenuItem";
            this.worldManagerToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.worldManagerToolStripMenuItem.Text = "World Manager...";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutForceStudioToolStripMenuItem,
            this.helpContentsToolStripMenuItem,
            this.tutorialsToolStripMenuItem,
            this.toolStripSeparator2,
            this.visitForceStudioWebsiteToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutForceStudioToolStripMenuItem
            // 
            this.aboutForceStudioToolStripMenuItem.Name = "aboutForceStudioToolStripMenuItem";
            this.aboutForceStudioToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.aboutForceStudioToolStripMenuItem.Text = "About ForceStudio";
            // 
            // helpContentsToolStripMenuItem
            // 
            this.helpContentsToolStripMenuItem.Name = "helpContentsToolStripMenuItem";
            this.helpContentsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.helpContentsToolStripMenuItem.Text = "Help Contents...";
            // 
            // tutorialsToolStripMenuItem
            // 
            this.tutorialsToolStripMenuItem.Name = "tutorialsToolStripMenuItem";
            this.tutorialsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.tutorialsToolStripMenuItem.Text = "Tutorials...";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(160, 6);
            // 
            // visitForceStudioWebsiteToolStripMenuItem
            // 
            this.visitForceStudioWebsiteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.forceStudionetToolStripMenuItem,
            this.forceLibnetToolStripMenuItem});
            this.visitForceStudioWebsiteToolStripMenuItem.Name = "visitForceStudioWebsiteToolStripMenuItem";
            this.visitForceStudioWebsiteToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.visitForceStudioWebsiteToolStripMenuItem.Text = "Online Resources";
            // 
            // forceStudionetToolStripMenuItem
            // 
            this.forceStudionetToolStripMenuItem.Name = "forceStudionetToolStripMenuItem";
            this.forceStudionetToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.forceStudionetToolStripMenuItem.Text = "Force Studio Website";
            // 
            // forceLibnetToolStripMenuItem
            // 
            this.forceLibnetToolStripMenuItem.Name = "forceLibnetToolStripMenuItem";
            this.forceLibnetToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.forceLibnetToolStripMenuItem.Text = "Force Lib Website";
            // 
            // MainAppBottomPanel
            // 
            this.MainAppBottomPanel.Controls.Add(this.ControlerCycleSlider);
            this.MainAppBottomPanel.Controls.Add(this.ControlerToolStrip);
            this.MainAppBottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.MainAppBottomPanel.Location = new System.Drawing.Point(179, 504);
            this.MainAppBottomPanel.Name = "MainAppBottomPanel";
            this.MainAppBottomPanel.Size = new System.Drawing.Size(693, 69);
            this.MainAppBottomPanel.TabIndex = 2;
            // 
            // ControlerCycleSlider
            // 
            this.ControlerCycleSlider.Dock = System.Windows.Forms.DockStyle.Top;
            this.ControlerCycleSlider.LargeChange = 2;
            this.ControlerCycleSlider.Location = new System.Drawing.Point(0, 25);
            this.ControlerCycleSlider.Maximum = 0;
            this.ControlerCycleSlider.Name = "ControlerCycleSlider";
            this.ControlerCycleSlider.Size = new System.Drawing.Size(693, 42);
            this.ControlerCycleSlider.TabIndex = 2;
            this.ControlerCycleSlider.TickFrequency = 100;
            this.ControlerCycleSlider.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.ControlerCycleSlider.ValueChanged += new System.EventHandler(this.ControlerCycleSlider_ValueChanged);
            // 
            // ControlerToolStrip
            // 
            this.ControlerToolStrip.CanOverflow = false;
            this.ControlerToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ControlerToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel4,
            this.ControlerExpLength,
            this.toolStripLabel7,
            this.toolStripLabel5,
            this.toolStripLabel6,
            this.ControlerCyclesNum,
            this.toolStripSeparator5,
            this.ControlerPreviousCycle,
            this.ControlerStartStop,
            this.ControlerNextCycle,
            this.toolStripSeparator6,
            this.toolStripLabel8,
            this.ControlerCurrentCycle});
            this.ControlerToolStrip.Location = new System.Drawing.Point(0, 0);
            this.ControlerToolStrip.Name = "ControlerToolStrip";
            this.ControlerToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.ControlerToolStrip.Size = new System.Drawing.Size(693, 25);
            this.ControlerToolStrip.TabIndex = 0;
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(101, 22);
            this.toolStripLabel4.Text = "Experiment Length:";
            // 
            // ControlerExpLength
            // 
            this.ControlerExpLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ControlerExpLength.Font = new System.Drawing.Font("Tahoma", 8F);
            this.ControlerExpLength.MaxLength = 3;
            this.ControlerExpLength.Name = "ControlerExpLength";
            this.ControlerExpLength.Size = new System.Drawing.Size(50, 25);
            this.ControlerExpLength.Text = "0";
            this.ControlerExpLength.TextChanged += new System.EventHandler(this.ControlerExpLength_TextChanged);
            // 
            // toolStripLabel7
            // 
            this.toolStripLabel7.Name = "toolStripLabel7";
            this.toolStripLabel7.Size = new System.Drawing.Size(47, 22);
            this.toolStripLabel7.Text = "Seconds";
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(11, 22);
            this.toolStripLabel5.Text = "-";
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(95, 22);
            this.toolStripLabel6.Text = "Cycles Per Second";
            // 
            // ControlerCyclesNum
            // 
            this.ControlerCyclesNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ControlerCyclesNum.Font = new System.Drawing.Font("Tahoma", 8F);
            this.ControlerCyclesNum.MaxLength = 6;
            this.ControlerCyclesNum.Name = "ControlerCyclesNum";
            this.ControlerCyclesNum.Size = new System.Drawing.Size(60, 25);
            this.ControlerCyclesNum.TextChanged += new System.EventHandler(this.ControlerCyclesNum_TextChanged);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // ControlerPreviousCycle
            // 
            this.ControlerPreviousCycle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ControlerPreviousCycle.Image = global::Force_Studio.Properties.Resources.Step_Backward;
            this.ControlerPreviousCycle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ControlerPreviousCycle.Name = "ControlerPreviousCycle";
            this.ControlerPreviousCycle.Size = new System.Drawing.Size(23, 22);
            this.ControlerPreviousCycle.Text = "Previous Cycle";
            this.ControlerPreviousCycle.Click += new System.EventHandler(this.ControlerPreviousCycle_Click);
            // 
            // ControlerStartStop
            // 
            this.ControlerStartStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ControlerStartStop.Image = global::Force_Studio.Properties.Resources.Start;
            this.ControlerStartStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ControlerStartStop.Name = "ControlerStartStop";
            this.ControlerStartStop.Size = new System.Drawing.Size(23, 22);
            this.ControlerStartStop.Text = "Start";
            this.ControlerStartStop.Click += new System.EventHandler(this.ControlerStartStop_Click);
            // 
            // ControlerNextCycle
            // 
            this.ControlerNextCycle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ControlerNextCycle.Image = global::Force_Studio.Properties.Resources.Step_Forward;
            this.ControlerNextCycle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ControlerNextCycle.Name = "ControlerNextCycle";
            this.ControlerNextCycle.Size = new System.Drawing.Size(23, 22);
            this.ControlerNextCycle.Text = "Next Cycle";
            this.ControlerNextCycle.Click += new System.EventHandler(this.ControlerNextCycle_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel8
            // 
            this.toolStripLabel8.Name = "toolStripLabel8";
            this.toolStripLabel8.Size = new System.Drawing.Size(77, 22);
            this.toolStripLabel8.Text = "Current Cycle:";
            // 
            // ControlerCurrentCycle
            // 
            this.ControlerCurrentCycle.Name = "ControlerCurrentCycle";
            this.ControlerCurrentCycle.Size = new System.Drawing.Size(13, 22);
            this.ControlerCurrentCycle.Text = "0";
            // 
            // MainAppTopPanel
            // 
            this.MainAppTopPanel.Controls.Add(this.ObjectsToolStrip);
            this.MainAppTopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.MainAppTopPanel.Location = new System.Drawing.Point(179, 24);
            this.MainAppTopPanel.Name = "MainAppTopPanel";
            this.MainAppTopPanel.Size = new System.Drawing.Size(693, 26);
            this.MainAppTopPanel.TabIndex = 4;
            // 
            // ObjectsToolStrip
            // 
            this.ObjectsToolStrip.CanOverflow = false;
            this.ObjectsToolStrip.Enabled = false;
            this.ObjectsToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ObjectsToolStrip.Location = new System.Drawing.Point(0, 0);
            this.ObjectsToolStrip.Name = "ObjectsToolStrip";
            this.ObjectsToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.ObjectsToolStrip.Size = new System.Drawing.Size(693, 25);
            this.ObjectsToolStrip.TabIndex = 0;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(60, 22);
            this.toolStripLabel1.Text = "Start Time:";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(23, 22);
            this.toolStripLabel2.Text = "-->";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(54, 22);
            this.toolStripLabel3.Text = "End Time:";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // MainAppLeftPanel
            // 
            this.MainAppLeftPanel.AutoScroll = true;
            this.MainAppLeftPanel.Controls.Add(this.Exp_Elements);
            this.MainAppLeftPanel.Controls.Add(this.groupBox1);
            this.MainAppLeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.MainAppLeftPanel.Location = new System.Drawing.Point(0, 24);
            this.MainAppLeftPanel.Name = "MainAppLeftPanel";
            this.MainAppLeftPanel.Size = new System.Drawing.Size(179, 549);
            this.MainAppLeftPanel.TabIndex = 3;
            // 
            // Exp_Elements
            // 
            this.Exp_Elements.Controls.Add(this.StateHistory_EditObject);
            this.Exp_Elements.Controls.Add(this.label10);
            this.Exp_Elements.Controls.Add(this.Exp_EditForce);
            this.Exp_Elements.Controls.Add(this.Exp_ForcesList);
            this.Exp_Elements.Controls.Add(this.Exp_RemoveObject);
            this.Exp_Elements.Controls.Add(this.Exp_NewObject);
            this.Exp_Elements.Controls.Add(this.Exp_ObjectsList);
            this.Exp_Elements.Dock = System.Windows.Forms.DockStyle.Top;
            this.Exp_Elements.Location = new System.Drawing.Point(0, 181);
            this.Exp_Elements.Name = "Exp_Elements";
            this.Exp_Elements.Size = new System.Drawing.Size(179, 365);
            this.Exp_Elements.TabIndex = 1;
            this.Exp_Elements.TabStop = false;
            this.Exp_Elements.Text = "Experiment Elements";
            // 
            // StateHistory_EditObject
            // 
            this.StateHistory_EditObject.Enabled = false;
            this.StateHistory_EditObject.Location = new System.Drawing.Point(9, 159);
            this.StateHistory_EditObject.Name = "StateHistory_EditObject";
            this.StateHistory_EditObject.Size = new System.Drawing.Size(57, 21);
            this.StateHistory_EditObject.TabIndex = 9;
            this.StateHistory_EditObject.Text = "Edit";
            this.StateHistory_EditObject.UseVisualStyleBackColor = true;
            this.StateHistory_EditObject.Click += new System.EventHandler(this.StateHistory_EditObject_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 201);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Forces:";
            // 
            // Exp_EditForce
            // 
            this.Exp_EditForce.Enabled = false;
            this.Exp_EditForce.Font = new System.Drawing.Font("Tahoma", 7F);
            this.Exp_EditForce.Location = new System.Drawing.Point(104, 198);
            this.Exp_EditForce.Name = "Exp_EditForce";
            this.Exp_EditForce.Size = new System.Drawing.Size(69, 20);
            this.Exp_EditForce.TabIndex = 4;
            this.Exp_EditForce.Text = "Edit Force";
            this.Exp_EditForce.UseVisualStyleBackColor = true;
            this.Exp_EditForce.Click += new System.EventHandler(this.Exp_EditForce_Click);
            // 
            // Exp_ForcesList
            // 
            this.Exp_ForcesList.FormattingEnabled = true;
            this.Exp_ForcesList.Location = new System.Drawing.Point(9, 224);
            this.Exp_ForcesList.Name = "Exp_ForcesList";
            this.Exp_ForcesList.Size = new System.Drawing.Size(164, 134);
            this.Exp_ForcesList.TabIndex = 3;
            this.Exp_ForcesList.SelectedIndexChanged += new System.EventHandler(this.Exp_ForcesList_SelectedIndexChanged);
            // 
            // Exp_RemoveObject
            // 
            this.Exp_RemoveObject.Enabled = false;
            this.Exp_RemoveObject.Font = new System.Drawing.Font("Tahoma", 7F);
            this.Exp_RemoveObject.Location = new System.Drawing.Point(104, 19);
            this.Exp_RemoveObject.Name = "Exp_RemoveObject";
            this.Exp_RemoveObject.Size = new System.Drawing.Size(69, 20);
            this.Exp_RemoveObject.TabIndex = 2;
            this.Exp_RemoveObject.Text = "Remove";
            this.Exp_RemoveObject.UseVisualStyleBackColor = true;
            this.Exp_RemoveObject.Click += new System.EventHandler(this.Exp_RemoveObject_Click);
            // 
            // Exp_NewObject
            // 
            this.Exp_NewObject.ContextMenuStrip = this.NewObjectMenu;
            this.Exp_NewObject.Font = new System.Drawing.Font("Tahoma", 7F);
            this.Exp_NewObject.Location = new System.Drawing.Point(9, 19);
            this.Exp_NewObject.Name = "Exp_NewObject";
            this.Exp_NewObject.Size = new System.Drawing.Size(69, 20);
            this.Exp_NewObject.TabIndex = 1;
            this.Exp_NewObject.Text = "New Object";
            this.Exp_NewObject.UseVisualStyleBackColor = true;
            this.Exp_NewObject.Click += new System.EventHandler(this.Exp_NewObject_Click);
            // 
            // Exp_ObjectsList
            // 
            this.Exp_ObjectsList.FormattingEnabled = true;
            this.Exp_ObjectsList.Location = new System.Drawing.Point(9, 45);
            this.Exp_ObjectsList.Name = "Exp_ObjectsList";
            this.Exp_ObjectsList.Size = new System.Drawing.Size(164, 108);
            this.Exp_ObjectsList.TabIndex = 0;
            this.Exp_ObjectsList.SelectedIndexChanged += new System.EventHandler(this.Exp_ObjectsList_SelectedIndexChanged);
            this.Exp_ObjectsList.DoubleClick += new System.EventHandler(this.Exp_ObjectsList_DoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ProcessControl);
            this.groupBox1.Controls.Add(this.ExpInfo_ProcessProgress);
            this.groupBox1.Controls.Add(this.ExpInfo_TotalCyclesNumber);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ExpInfo_ProcessedCycles);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ExpInfo_ProcessState);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ExpInfo_ExpCPS);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ExpInfo_ExpLength);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(179, 181);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Experiment Info.";
            // 
            // ProcessControl
            // 
            this.ProcessControl.FlatAppearance.BorderSize = 0;
            this.ProcessControl.Font = new System.Drawing.Font("Tahoma", 8F);
            this.ProcessControl.Location = new System.Drawing.Point(110, 135);
            this.ProcessControl.Margin = new System.Windows.Forms.Padding(0);
            this.ProcessControl.Name = "ProcessControl";
            this.ProcessControl.Size = new System.Drawing.Size(63, 21);
            this.ProcessControl.TabIndex = 11;
            this.ProcessControl.Text = "Process";
            this.ProcessControl.UseVisualStyleBackColor = false;
            this.ProcessControl.Click += new System.EventHandler(this.ProcessControl_Click);
            // 
            // ExpInfo_ProcessProgress
            // 
            this.ExpInfo_ProcessProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ExpInfo_ProcessProgress.ForeColor = System.Drawing.Color.Brown;
            this.ExpInfo_ProcessProgress.Location = new System.Drawing.Point(3, 160);
            this.ExpInfo_ProcessProgress.Name = "ExpInfo_ProcessProgress";
            this.ExpInfo_ProcessProgress.Size = new System.Drawing.Size(173, 18);
            this.ExpInfo_ProcessProgress.Step = 1;
            this.ExpInfo_ProcessProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.ExpInfo_ProcessProgress.TabIndex = 10;
            // 
            // ExpInfo_TotalCyclesNumber
            // 
            this.ExpInfo_TotalCyclesNumber.AutoSize = true;
            this.ExpInfo_TotalCyclesNumber.Location = new System.Drawing.Point(117, 113);
            this.ExpInfo_TotalCyclesNumber.Name = "ExpInfo_TotalCyclesNumber";
            this.ExpInfo_TotalCyclesNumber.Size = new System.Drawing.Size(13, 13);
            this.ExpInfo_TotalCyclesNumber.TabIndex = 9;
            this.ExpInfo_TotalCyclesNumber.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Total Cycles Number:";
            // 
            // ExpInfo_ProcessedCycles
            // 
            this.ExpInfo_ProcessedCycles.AutoSize = true;
            this.ExpInfo_ProcessedCycles.Location = new System.Drawing.Point(104, 91);
            this.ExpInfo_ProcessedCycles.Name = "ExpInfo_ProcessedCycles";
            this.ExpInfo_ProcessedCycles.Size = new System.Drawing.Size(13, 13);
            this.ExpInfo_ProcessedCycles.TabIndex = 7;
            this.ExpInfo_ProcessedCycles.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Processed Cycles:";
            // 
            // ExpInfo_ProcessState
            // 
            this.ExpInfo_ProcessState.AutoSize = true;
            this.ExpInfo_ProcessState.Location = new System.Drawing.Point(85, 69);
            this.ExpInfo_ProcessState.Name = "ExpInfo_ProcessState";
            this.ExpInfo_ProcessState.Size = new System.Drawing.Size(13, 13);
            this.ExpInfo_ProcessState.TabIndex = 5;
            this.ExpInfo_ProcessState.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Process State:";
            // 
            // ExpInfo_ExpCPS
            // 
            this.ExpInfo_ExpCPS.AutoSize = true;
            this.ExpInfo_ExpCPS.Location = new System.Drawing.Point(107, 47);
            this.ExpInfo_ExpCPS.Name = "ExpInfo_ExpCPS";
            this.ExpInfo_ExpCPS.Size = new System.Drawing.Size(13, 13);
            this.ExpInfo_ExpCPS.TabIndex = 3;
            this.ExpInfo_ExpCPS.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cycles per Second:";
            // 
            // ExpInfo_ExpLength
            // 
            this.ExpInfo_ExpLength.AutoSize = true;
            this.ExpInfo_ExpLength.Location = new System.Drawing.Point(73, 25);
            this.ExpInfo_ExpLength.Name = "ExpInfo_ExpLength";
            this.ExpInfo_ExpLength.Size = new System.Drawing.Size(13, 13);
            this.ExpInfo_ExpLength.TabIndex = 1;
            this.ExpInfo_ExpLength.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Exp Length:";
            // 
            // MainAppViewPort
            // 
            this.MainAppViewPort.BackColor = System.Drawing.SystemColors.ControlLight;
            this.MainAppViewPort.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MainAppViewPort.ColumnCount = 2;
            this.MainAppViewPort.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MainAppViewPort.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 410F));
            this.MainAppViewPort.Controls.Add(this.ViewPort1, 0, 0);
            this.MainAppViewPort.Controls.Add(this.ViewPort2, 1, 0);
            this.MainAppViewPort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainAppViewPort.Location = new System.Drawing.Point(179, 50);
            this.MainAppViewPort.Margin = new System.Windows.Forms.Padding(0);
            this.MainAppViewPort.Name = "MainAppViewPort";
            this.MainAppViewPort.RowCount = 1;
            this.MainAppViewPort.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 240F));
            this.MainAppViewPort.Size = new System.Drawing.Size(693, 454);
            this.MainAppViewPort.TabIndex = 7;
            // 
            // ViewPort1
            // 
            this.ViewPort1.Controls.Add(this.StateHistory);
            this.ViewPort1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ViewPort1.Location = new System.Drawing.Point(4, 4);
            this.ViewPort1.Name = "ViewPort1";
            this.ViewPort1.Size = new System.Drawing.Size(274, 446);
            this.ViewPort1.TabIndex = 0;
            // 
            // StateHistory
            // 
            this.StateHistory.BackColor = System.Drawing.SystemColors.Control;
            this.StateHistory.Controls.Add(this.HistogramView);
            this.StateHistory.Controls.Add(this.label9);
            this.StateHistory.Controls.Add(this.StateHistory_CycleTime);
            this.StateHistory.Controls.Add(this.label8);
            this.StateHistory.Controls.Add(this.StateHistory_StateViewer);
            this.StateHistory.Controls.Add(this.label7);
            this.StateHistory.Controls.Add(this.StateHistory_Cycle);
            this.StateHistory.Controls.Add(this.label6);
            this.StateHistory.Controls.Add(this.StateHistory_ObjectsList);
            this.StateHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StateHistory.Location = new System.Drawing.Point(0, 0);
            this.StateHistory.Name = "StateHistory";
            this.StateHistory.Size = new System.Drawing.Size(274, 446);
            this.StateHistory.TabIndex = 1;
            this.StateHistory.TabStop = false;
            this.StateHistory.Text = "State History";
            // 
            // HistogramView
            // 
            this.HistogramView.BackColor = System.Drawing.Color.GhostWhite;
            this.HistogramView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HistogramView.Controls.Add(this.Histogram_TimeScroll);
            this.HistogramView.Controls.Add(this.Histogram_ToolStrip);
            this.HistogramView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.HistogramView.Location = new System.Drawing.Point(3, 276);
            this.HistogramView.Name = "HistogramView";
            this.HistogramView.Size = new System.Drawing.Size(268, 167);
            this.HistogramView.TabIndex = 8;
            this.HistogramView.Paint += new System.Windows.Forms.PaintEventHandler(this.HistogramView_Paint);
            // 
            // Histogram_TimeScroll
            // 
            this.Histogram_TimeScroll.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Histogram_TimeScroll.Location = new System.Drawing.Point(0, 151);
            this.Histogram_TimeScroll.Name = "Histogram_TimeScroll";
            this.Histogram_TimeScroll.Size = new System.Drawing.Size(266, 14);
            this.Histogram_TimeScroll.TabIndex = 1;
            this.Histogram_TimeScroll.ValueChanged += new System.EventHandler(this.Histogram_TimeScroll_ValueChanged);
            // 
            // Histogram_ToolStrip
            // 
            this.Histogram_ToolStrip.BackColor = System.Drawing.Color.Transparent;
            this.Histogram_ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.Histogram_ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Histogram_Elements,
            this.toolStripSeparator8,
            this.Histogram_ZoomIn,
            this.Histogram_ZoomOut});
            this.Histogram_ToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.Histogram_ToolStrip.Location = new System.Drawing.Point(0, 0);
            this.Histogram_ToolStrip.Name = "Histogram_ToolStrip";
            this.Histogram_ToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.Histogram_ToolStrip.Size = new System.Drawing.Size(266, 23);
            this.Histogram_ToolStrip.TabIndex = 0;
            // 
            // Histogram_Elements
            // 
            this.Histogram_Elements.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Histogram_Elements.Image = ((System.Drawing.Image)(resources.GetObject("Histogram_Elements.Image")));
            this.Histogram_Elements.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Histogram_Elements.Name = "Histogram_Elements";
            this.Histogram_Elements.Size = new System.Drawing.Size(29, 20);
            this.Histogram_Elements.ToolTipText = "Graph Line";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 23);
            // 
            // Histogram_ZoomIn
            // 
            this.Histogram_ZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Histogram_ZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("Histogram_ZoomIn.Image")));
            this.Histogram_ZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Histogram_ZoomIn.Name = "Histogram_ZoomIn";
            this.Histogram_ZoomIn.Size = new System.Drawing.Size(23, 20);
            this.Histogram_ZoomIn.ToolTipText = "Zoop In";
            this.Histogram_ZoomIn.Click += new System.EventHandler(this.Histogram_ZoomIn_Click);
            // 
            // Histogram_ZoomOut
            // 
            this.Histogram_ZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Histogram_ZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("Histogram_ZoomOut.Image")));
            this.Histogram_ZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Histogram_ZoomOut.Name = "Histogram_ZoomOut";
            this.Histogram_ZoomOut.Size = new System.Drawing.Size(23, 20);
            this.Histogram_ZoomOut.ToolTipText = "Zoom Out";
            this.Histogram_ZoomOut.Click += new System.EventHandler(this.Histogram_ZoomOut_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(109, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "=";
            // 
            // StateHistory_CycleTime
            // 
            this.StateHistory_CycleTime.AutoSize = true;
            this.StateHistory_CycleTime.Location = new System.Drawing.Point(130, 52);
            this.StateHistory_CycleTime.Name = "StateHistory_CycleTime";
            this.StateHistory_CycleTime.Size = new System.Drawing.Size(21, 13);
            this.StateHistory_CycleTime.TabIndex = 6;
            this.StateHistory_CycleTime.Text = "0 s";
            this.StateHistory_CycleTime.EnabledChanged += new System.EventHandler(this.StateHistory_CycleTime_EnabledChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "State Variables:";
            // 
            // StateHistory_StateViewer
            // 
            this.StateHistory_StateViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.StateHistory_StateViewer.BackColor = System.Drawing.Color.WhiteSmoke;
            this.StateHistory_StateViewer.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StateHistory_StateViewer.Location = new System.Drawing.Point(9, 97);
            this.StateHistory_StateViewer.Multiline = true;
            this.StateHistory_StateViewer.Name = "StateHistory_StateViewer";
            this.StateHistory_StateViewer.ReadOnly = true;
            this.StateHistory_StateViewer.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.StateHistory_StateViewer.Size = new System.Drawing.Size(259, 173);
            this.StateHistory_StateViewer.TabIndex = 4;
            this.StateHistory_StateViewer.WordWrap = false;
            this.StateHistory_StateViewer.EnabledChanged += new System.EventHandler(this.StateHistory_StateViewer_EnabledChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Cycle:";
            // 
            // StateHistory_Cycle
            // 
            this.StateHistory_Cycle.Location = new System.Drawing.Point(55, 50);
            this.StateHistory_Cycle.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StateHistory_Cycle.Name = "StateHistory_Cycle";
            this.StateHistory_Cycle.Size = new System.Drawing.Size(48, 20);
            this.StateHistory_Cycle.TabIndex = 2;
            this.StateHistory_Cycle.ValueChanged += new System.EventHandler(this.StateHistory_Cycle_ValueChanged);
            this.StateHistory_Cycle.EnabledChanged += new System.EventHandler(this.StateHistory_Cycle_EnabledChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Object:";
            // 
            // StateHistory_ObjectsList
            // 
            this.StateHistory_ObjectsList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StateHistory_ObjectsList.FormattingEnabled = true;
            this.StateHistory_ObjectsList.Location = new System.Drawing.Point(55, 19);
            this.StateHistory_ObjectsList.Name = "StateHistory_ObjectsList";
            this.StateHistory_ObjectsList.Size = new System.Drawing.Size(208, 21);
            this.StateHistory_ObjectsList.TabIndex = 0;
            this.StateHistory_ObjectsList.EnabledChanged += new System.EventHandler(this.StateHistory_ObjectsList_EnabledChanged);
            this.StateHistory_ObjectsList.SelectedValueChanged += new System.EventHandler(this.StateHistory_ObjectsList_SelectedValueChanged);
            // 
            // NewObjectMenu
            // 
            this.NewObjectMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSphere,
            this.newPendulum});
            this.NewObjectMenu.Name = "NewObjectMenu";
            this.NewObjectMenu.ShowImageMargin = false;
            this.NewObjectMenu.Size = new System.Drawing.Size(128, 70);
            // 
            // newSphere
            // 
            this.newSphere.Name = "newSphere";
            this.newSphere.Size = new System.Drawing.Size(127, 22);
            this.newSphere.Text = "Sphere";
            this.newSphere.Click += new System.EventHandler(this.newSphere_Click);
            // 
            // newPendulum
            // 
            this.newPendulum.Name = "newPendulum";
            this.newPendulum.Size = new System.Drawing.Size(127, 22);
            this.newPendulum.Text = "Pendulum";
            this.newPendulum.Click += new System.EventHandler(this.newPendulum_Click);
            // 
            // MainAppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 573);
            this.Controls.Add(this.MainAppViewPort);
            this.Controls.Add(this.MainAppBottomPanel);
            this.Controls.Add(this.MainAppTopPanel);
            this.Controls.Add(this.MainAppLeftPanel);
            this.Controls.Add(this.MainAppMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainAppMenu;
            this.MinimumSize = new System.Drawing.Size(880, 600);
            this.Name = "MainAppForm";
            this.Load += new System.EventHandler(this.MainAppForm_Load);
            this.ViewPort2.ResumeLayout(false);
            this.MainAppMenu.ResumeLayout(false);
            this.MainAppMenu.PerformLayout();
            this.MainAppBottomPanel.ResumeLayout(false);
            this.MainAppBottomPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ControlerCycleSlider)).EndInit();
            this.ControlerToolStrip.ResumeLayout(false);
            this.ControlerToolStrip.PerformLayout();
            this.MainAppTopPanel.ResumeLayout(false);
            this.MainAppTopPanel.PerformLayout();
            this.MainAppLeftPanel.ResumeLayout(false);
            this.Exp_Elements.ResumeLayout(false);
            this.Exp_Elements.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.MainAppViewPort.ResumeLayout(false);
            this.ViewPort1.ResumeLayout(false);
            this.StateHistory.ResumeLayout(false);
            this.StateHistory.PerformLayout();
            this.HistogramView.ResumeLayout(false);
            this.HistogramView.PerformLayout();
            this.Histogram_ToolStrip.ResumeLayout(false);
            this.Histogram_ToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StateHistory_Cycle)).EndInit();
            this.NewObjectMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SharpGL.OpenGLCtrl simulatorView;
        private System.Windows.Forms.MenuStrip MainAppMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newExpirementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expirementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openExpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveExpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveExpAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeExpToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem expSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stateAnalyzerPreferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem histogramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simulatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem physicsEnginePreferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem worldManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpContentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tutorialsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem visitForceStudioWebsiteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem forceStudionetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem forceLibnetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutForceStudioToolStripMenuItem;
        private System.Windows.Forms.Panel MainAppBottomPanel;
        private System.Windows.Forms.Panel MainAppTopPanel;
        private System.Windows.Forms.ToolStrip ObjectsToolStrip;
        private System.Windows.Forms.ToolStrip ControlerToolStrip;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_NewExp;
        private System.Windows.Forms.ToolStripMenuItem newExpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_OpenExp;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_SaveExp;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_SaveExpAs;
        private System.Windows.Forms.ToolStripMenuItem MainMenu_CloseExp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem physicsEngineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripTextBox ControlerExpLength;
        private System.Windows.Forms.ToolStripLabel toolStripLabel6;
        private System.Windows.Forms.ToolStripTextBox ControlerCyclesNum;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton ControlerStartStop;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.TrackBar ControlerCycleSlider;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.Panel MainAppLeftPanel;
        private System.Windows.Forms.ToolStripMenuItem objectsForcesManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem forcesManagerToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel MainAppViewPort;
        private System.Windows.Forms.ToolStripMenuItem simulatorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem histogramToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem initialStateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton ControlerPreviousCycle;
        private System.Windows.Forms.ToolStripButton ControlerNextCycle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ExpInfo_ExpLength;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label ExpInfo_ExpCPS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label ExpInfo_ProcessedCycles;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label ExpInfo_TotalCyclesNumber;
        private System.Windows.Forms.ToolStripLabel toolStripLabel7;
        private System.Windows.Forms.ToolStripLabel ControlerCurrentCycle;
        private System.Windows.Forms.ToolStripLabel toolStripLabel8;
        private System.Windows.Forms.ProgressBar ExpInfo_ProcessProgress;
        private System.Windows.Forms.Button ProcessControl;
        private System.Windows.Forms.Label ExpInfo_ProcessState;
        private System.Windows.Forms.GroupBox Exp_Elements;
        private System.Windows.Forms.Panel ViewPort1;
        private System.Windows.Forms.GroupBox StateHistory;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox StateHistory_StateViewer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown StateHistory_Cycle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox StateHistory_ObjectsList;
        private System.Windows.Forms.Panel ViewPort2;
        private System.Windows.Forms.Label StateHistory_CycleTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel HistogramView;
        private System.Windows.Forms.ToolStrip Histogram_ToolStrip;
        private System.Windows.Forms.ToolStripButton Histogram_ZoomIn;
        private System.Windows.Forms.ToolStripButton Histogram_ZoomOut;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripDropDownButton Histogram_Elements;
        private System.Windows.Forms.HScrollBar Histogram_TimeScroll;
        private System.Windows.Forms.ListBox Exp_ObjectsList;
        private System.Windows.Forms.Button Exp_NewObject;
        private System.Windows.Forms.Button Exp_RemoveObject;
        private System.Windows.Forms.ListBox Exp_ForcesList;
        private System.Windows.Forms.Button Exp_EditForce;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button StateHistory_EditObject;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip NewObjectMenu;
        private System.Windows.Forms.ToolStripMenuItem newSphere;
        private System.Windows.Forms.ToolStripMenuItem newPendulum;
    }
}