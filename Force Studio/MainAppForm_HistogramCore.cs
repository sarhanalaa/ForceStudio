﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using ForceLib;

namespace Force_Studio
{

    public partial class MainAppForm
    {

        // Fields
        long hisLength;
        long hisFirstCycle;
        float hisMaxValue;
        Color[] hisElemColors;
        String[] hisElements;

        //-----------------------------


        // Properties

        public long HisLength
        {
            get { return this.hisLength; }
            set
            {
                if (value < 2 || value >= (this.ProcessedCycles - this.HisFirstCycle))
                    return;
                this.hisLength = value;
                if (this.HisLengthChanged != null)
                    this.HisLengthChanged();
            }
        }


        public long HisFirstCycle
        {
            get { return this.hisFirstCycle; }
            set
            {
                if(value < 0 || value > (this.ProcessedCycles - this.HisLength - 1))
                    return;
                this.hisFirstCycle = value;
                if (this.HisFirstCycleChanged != null)
                    this.HisFirstCycleChanged();
            }
        }


        public float HisMaxValue
        {
            get { return this.hisMaxValue; }
            set
            {
                if (value < 0)
                    value *= -1;
                this.hisMaxValue = value;
                if (this.HisMaxValueChanged != null)
                    this.HisMaxValueChanged();
            }
        }

        public String[] HisElements
        {
            get { return this.hisElements; }
            set
            {
                this.hisElements = value;
                if (this.HisElementsChanged != null)
                    this.HisElementsChanged();
            }
        }

        public Color[] HisElemColors
        {
            get { return this.hisElemColors; }
            set
            {
                this.hisElemColors = value;
                if (this.HisElemColorsChanged != null)
                    this.HisElemColorsChanged();
            }
        }

        //-----------------------------


        // Events
        public event PropertyChanged HisLengthChanged;
        public event PropertyChanged HisFirstCycleChanged;
        public event PropertyChanged HisMaxValueChanged;
        public event PropertyChanged HisElementsChanged;
        public event PropertyChanged HisElemColorsChanged;
        //-----------------------------


        // Methods

        private void HistogramLength_Changed()
        {
            this.Histogram_TimeScroll.Maximum = (int)(this.ProcessedCycles - this.HisLength - 1);

            UpdateHistogramView((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem,
                this.HisElements, this.HisElemColors, this.HisFirstCycle, this.HisLength,
                this.HistogramView.Width, this.HistogramView.Height,this.HisMaxValue);
        }

        private void HistogramFirstCycle_Changed()
        {
            UpdateHistogramView((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem,
                this.HisElements, this.HisElemColors, this.HisFirstCycle, this.HisLength,
                this.HistogramView.Width, this.HistogramView.Height, this.HisMaxValue);
        }

        private void HistogramMaxValue_Changed()
        {
            UpdateHistogramView((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem,
                this.HisElements, this.HisElemColors, this.HisFirstCycle, this.HisLength,
                this.HistogramView.Width, this.HistogramView.Height, this.HisMaxValue);
        }

        private void HistogramElemColors_Changed()
        {
            UpdateHistogramView((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem,
                this.HisElements, this.HisElemColors, this.HisFirstCycle, this.HisLength,
                this.HistogramView.Width, this.HistogramView.Height, this.HisMaxValue);
        }

        private void HistogramElements_Changed()
        {
            UpdateHistogramView((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem,
                this.HisElements, this.HisElemColors, this.HisFirstCycle, this.HisLength,
                this.HistogramView.Width, this.HistogramView.Height, this.HisMaxValue);
        }


        private void HistogramElement_Click(object sender, EventArgs e)
        {
            if (!(sender is ToolStripMenuItem))
                return;

            ((ToolStripMenuItem)sender).Checked = !((ToolStripMenuItem)sender).Checked;
            RefreshHistogramElements();
        }


        // Calculates the HisMaxValue for a given object.
        private void CalculateHistogramMaxValue(IAnalyzable objRef, string[] elements)
        {
            float maxValue = float.MinValue;

            foreach (string element in elements)
                for (int i = 0; i < this.ProcessedCycles; i++)
                {
                    if (!objRef.StateHistory.ContainsKey(i))
                        continue;
                    if (!objRef.StateHistory[i].ContainsKey(element))
                        break;
                    if (Math.Abs((float)objRef.StateHistory[i][element]) > maxValue)
                        maxValue = Math.Abs((float)objRef.StateHistory[i][element]);
                }

            this.HisMaxValue = maxValue;
        }

        
        // Generates the List of Elements that object offer as Graph Lines
        public void GenerateHistogramObjectElementsList(IAnalyzable objRef)
        {
            List<String> elements = new List<string>(objRef.HistogramElements);

            List<ToolStripItem> newItems = new List<ToolStripItem>(elements.Count);

            foreach (ToolStripItem item in this.Histogram_Elements.DropDownItems)
            {
                if (elements.Contains(item.Text))
                {
                    elements.Remove(item.Text);
                    newItems.Add(item);
                }
            }

            foreach (string element in elements)
            {
                ToolStripMenuItem eItem = new ToolStripMenuItem(element);
                eItem.Click += new EventHandler(HistogramElement_Click);
                newItems.Add(eItem);
            }

            this.Histogram_Elements.DropDownItems.Clear();
            this.Histogram_Elements.DropDownItems.AddRange(newItems.ToArray());

            RefreshHistogramElements();

        }


        // Refreshes the Histogram Elements that will be drawn
        public void RefreshHistogramElements()
        {
            List<String> elements = new List<string>(this.Histogram_Elements.DropDownItems.Count);

            foreach (ToolStripMenuItem item in this.Histogram_Elements.DropDownItems)
                if (item.Checked)
                    elements.Add(item.Text);

            this.HisElements = elements.ToArray();
        }

        //-----------------------------

    }

}