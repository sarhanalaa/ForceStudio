﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.Remoting.Contexts;
using Force_Studio_Lib;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio
{
    [Synchronization]
    public partial class MainAppForm
    {
       
        // Enumerations
        public enum EPlaybackState { PLAYING, PAUSED };
        //----------------------------
        

        // Events
        public event StateChanged PlaybackStateChanged;
        //----------------------------


        // Fields

        // Studio Core
        World3D world;
        WorldManager3D worldManager;
        Simulator simulator;
        WorldManagerEditor worldManagerEditor;

        
        // Initial-State Handlers
        World3D initialStateWorld;
        WorldManager3D initialStateWorldManager;

        // Simulation History Handlers
        Dictionary<long, List<Primitive3>> simulatorForcesHistory;
        Dictionary<long, List<Primitive3>> simulatorObjectsHistory;
        Dictionary<long, List<Primitive3>> simulatorVelocityHistory;
        Dictionary<long, List<Primitive3>> simulatorAccelerationHistory;
        Dictionary<long, List<Primitive3>> simulatorAngVelHistory;
        Dictionary<long, List<Primitive3>> simulatorAngAccHistory;

        EPlaybackState playbackState;

        Thread TProcess;

        //--------------------------------


        // Properties
        public EPlaybackState PlaybackState
        {
            get { return this.playbackState; }
            set
            {
                this.playbackState = value;
                if (this.PlaybackStateChanged != null)
                    this.PlaybackStateChanged();
            }
        }
        //--------------------------------


        // Constructors

        public MainAppForm()
        {

            InitializeComponent();

            // Initializing Fields
            this.world = new World3D(1.204F, 9.8F, 20, new Vector3());
            this.worldManager = new WorldManager3D(this.world, 1 / 50F);
            this.worldManager.ObjectChanging += new WorldManager3D.operationOnObject(RenderForces);
            this.editingObjectsPanel = new Dictionary<I3dObject, Panel>();
            this.editingForcesPanel = new Dictionary<I3dForce, Panel>();
            this.motionSpeed = 2;
            this.worldManagerEditor = new WorldManagerEditor(this.world);

            // Simulator Initialization
            this.simulator = new Simulator(this.worldManager, this.simulatorView);
            this.simulatorObjectsHistory = new Dictionary<long, List<Primitive3>>();
            this.simulatorForcesHistory = new Dictionary<long, List<Primitive3>>();
            this.simulatorVelocityHistory = new Dictionary<long, List<Primitive3>>();
            this.simulatorAccelerationHistory = new Dictionary<long, List<Primitive3>>();
            this.simulatorAngVelHistory = new Dictionary<long, List<Primitive3>>();
            this.simulatorAngAccHistory = new Dictionary<long, List<Primitive3>>();

            simulator.AddDictionary(Simulator.DrawItems.OBJECTS, this.simulatorObjectsHistory);
            simulator.AddDictionary(Simulator.DrawItems.FORCES, this.simulatorForcesHistory);
            simulator.AddDictionary(Simulator.DrawItems.VELOCITIES, this.simulatorVelocityHistory);
            simulator.AddDictionary(Simulator.DrawItems.ACCELERATIONS, this.simulatorAccelerationHistory);
            simulator.AddDictionary(Simulator.DrawItems.ANGVELOCITIES, this.simulatorAngVelHistory);
            simulator.AddDictionary(Simulator.DrawItems.ANGACCELERATIONS, this.simulatorAngAccHistory);

            // Initializing Threads
            // Creating Threads
            TProcess = new Thread(new ThreadStart(Process));


            // Handling Events
            this.ProcessStateChanged += new StateChanged(ProcessState_Changed);
            this.ProcessedCyclesChanged += new StateChanged(ProcessedCycles_Changed);
            this.ExperimentLengthChanged += new PropertyChanged(ExperimentLength_Changed);
            this.CyclesPerSecondChanged += new PropertyChanged(CyclesPerSecond_Changed);
            this.TotalCyclesNumberChanged += new PropertyChanged(TotalCyclesNumber_Changed);
            this.PlaybackStateChanged += new StateChanged(PlaybackState_Changed);
            this.CurrentCycleChanged += new PropertyChanged(CurrentCycle_Changed);
            this.HisLengthChanged += new PropertyChanged(HistogramLength_Changed);
            this.HisFirstCycleChanged += new PropertyChanged(HistogramFirstCycle_Changed);
            this.HisMaxValueChanged += new PropertyChanged(HistogramMaxValue_Changed);
            this.HisElementsChanged += new PropertyChanged(HistogramElements_Changed);
            this.HisElemColorsChanged += new PropertyChanged(HistogramElemColors_Changed);

            // Creating a new Experiment
            CreateNewExperiment();

            // * * * Temporary * * * //

            Control.CheckForIllegalCrossThreadCalls = false;

            //-------------------------

        }

        //--------------------------------


        // Methods

        private void CreateNewExperiment()
        {

            // Preparing Fields
            this.worldManager.AllObjects.Clear();
            this.worldManager.WorldForces.Clear();
            this.editingForcesPanel.Clear();
            this.editingObjectsPanel.Clear();

            this.simulatorForcesHistory.Clear();
            this.simulatorObjectsHistory.Clear();

            this.simulatorVelocityHistory.Clear();
            this.simulatorAccelerationHistory.Clear();
            this.simulatorAngVelHistory.Clear();
            this.simulatorAngAccHistory.Clear();

            // Initializing Experiment
            this.CurrentCycle = -1;
            this.ProcessedCycles = 0;
            this.ExperimentLength = 30;
            this.CyclesPerSecond = 100;
            this.CurrentCycle = 0;
            this.ProcessState = EProcessState.INITIALIZED;

            this.PlaybackState = EPlaybackState.PAUSED;

            this.worldManager.Initialize();

            TProcess = new Thread(new ThreadStart(Process));

            UpdateObjectsList();

        }

        private void Process()
        {
            if (this.worldManager.AllObjects.Count < 1)
            {
                alert("There is no objects yet", MessageBoxIcon.Exclamation);
                return;
            }

            this.ProcessState = EProcessState.PROCESSING;

            // Cycle Primitive Handlers
            List<Primitive3> objectsPrims = new List<Primitive3>();
            List<Primitive3> velocitiesPrims = new List<Primitive3>();
            List<Primitive3> accelerationsPrims = new List<Primitive3>();
            List<Primitive3> angVelPrims = new List<Primitive3>();
            List<Primitive3> angAccPrims = new List<Primitive3>();
            List<Primitive3> forcesPrims = new List<Primitive3>();

            for (long i = this.ProcessedCycles; i < this.TotalCyclesNumber; i++)
            {
                
                // Clearing Primitves Handler Lists
                objectsPrims.Clear();
                velocitiesPrims.Clear();
                accelerationsPrims.Clear();
                angVelPrims.Clear();
                angAccPrims.Clear();
                forcesPrims.Clear();

                // Saving Histories
                Vertex3 centerOfMass = new Vertex3();
                Vertex3 hangPoint = new Vertex3();
                Vertex3 endPoint = new Vertex3();
                Primitive3 arrow = new Primitive3("", EPrimitiveType.ARROW);

                foreach (I3dObject objRef in this.worldManager.AllObjects)
                {
                    lock (this.worldManager.AllObjects)
                    {
                        // Object History
                        if (objRef is IAnalyzable)
                            ((IAnalyzable)objRef).AddState(i, ((IAnalyzable)objRef).GenerateCurrentState());

                        // Simulation History
                        if (objRef is IRenderable)
                        {

                            // Saving Object Primitives
                            objectsPrims.AddRange(((IRenderable)objRef).Render());

                            
                                centerOfMass.Position = objRef.Position;

                                // Saving Velocity Vector
                                endPoint.Position = objRef.Velocity;
                                arrow.Name = (~objRef.Velocity).ToString();
                                arrow.Vertices = new List<Vertex3>();
                                centerOfMass.DiffuseColor = centerOfMass.SpecularColor = Color.SkyBlue;
                                arrow.Vertices.Add(centerOfMass);
                                arrow.Vertices.Add(endPoint);
                                velocitiesPrims.Add(arrow);

                                // Saving Acceleration Vector
                                endPoint.Position = objRef.Acceleration;
                                arrow.Name = (~objRef.Acceleration).ToString();
                                centerOfMass.DiffuseColor = centerOfMass.SpecularColor = Color.YellowGreen;
                                arrow.Vertices = new List<Vertex3>();
                                arrow.Vertices.Add(centerOfMass);
                                arrow.Vertices.Add(endPoint);
                                accelerationsPrims.Add(arrow);
                            
                            if(objRef.Hanged)
                            {
                                hangPoint.Position = objRef.HangPoint;

                                // Saving Angular Velocity Vector
                                endPoint.Position = objRef.AngVelocity;
                                arrow.Name = (~objRef.AngVelocity).ToString();
                                arrow.Vertices = new List<Vertex3>();
                                hangPoint.DiffuseColor = hangPoint.SpecularColor = Color.DarkBlue;
                                arrow.Vertices.Add(hangPoint);
                                arrow.Vertices.Add(endPoint);
                                angVelPrims.Add(arrow);

                                // Saving Angular Acceleration Vector
                                endPoint.Position = objRef.AngAcceleration;
                                arrow.Name = (~objRef.AngAcceleration).ToString();
                                hangPoint.DiffuseColor = hangPoint.SpecularColor = Color.LightYellow;
                                arrow.Vertices = new List<Vertex3>();
                                arrow.Vertices.Add(hangPoint);
                                arrow.Vertices.Add(endPoint);
                                angAccPrims.Add(arrow);
                            }
                            
                        }
                    }
                }

                lock (this)
                {

                    // Adding Simulation Primitives to Simulation History
                    simulatorObjectsHistory.Add(i, new List<Primitive3>(objectsPrims));
                    simulatorVelocityHistory.Add(i, new List<Primitive3>(velocitiesPrims));
                    simulatorAccelerationHistory.Add(i, new List<Primitive3>(accelerationsPrims));
                    simulatorAngVelHistory.Add(i, new List<Primitive3>(angVelPrims));
                    simulatorAngAccHistory.Add(i, new List<Primitive3>(angAccPrims));

                    // Preparing an empty entry for current cycle in Forces Primitives History
                    simulatorForcesHistory.Add(i, new List<Primitive3>());

                    // Do World Cycle
                    this.worldManager.Cycle(i);

                    // Updating...
                    this.ProcessedCycles++;
                }

            }

            this.ProcessState = EProcessState.PROCESSED;
        }


        private void Play()
        {

            if (this.world == null || this.worldManager == null)
            {
                alert("Fatal Error!, Restarting the Application so the error may be resolved.", MessageBoxIcon.Error);
                Application.Restart();
            }

            if (this.worldManager.AllObjects.Count == 0)
            {
                alert("There is no Objects yet.", MessageBoxIcon.Information);
                return;
            }

            switch (this.PlaybackState)
            {
                case EPlaybackState.PAUSED:
                    switch (this.ProcessState)
                    {
                        case EProcessState.INITIALIZED:
                            TProcess.Start();
                            break;
                        case EProcessState.MODIFIED:
                            TProcess.Start();
                            break;
                        case EProcessState.PROCESSING:
                            int i = 0;
                            while ((this.ProcessedCycles - this.CurrentCycle) / (this.TotalCyclesNumber - this.CurrentCycle) < 0.1 && i < 4)
                            { Thread.Sleep(500 * ++i); }
                            break;
                    }
                    // * * * * Start Simulation * * * * //
                    simulator.Start();
                    this.PlaybackState = EPlaybackState.PLAYING;
                    break;

                case EPlaybackState.PLAYING:
                    // * * * * Pause Simulation * * * * //
                    simulator.Pause();
                    this.PlaybackState = EPlaybackState.PAUSED;
                    break;
            }
        }

        private void Seek(long cycle)
        {
            if (cycle < 0)
                cycle = 0;
            // Seeking the World Manager
            this.worldManager.Seek(cycle);
            // Clearing Simulation History
            for (long i = cycle; i < this.ProcessedCycles; i++)
            {
                simulatorObjectsHistory.Remove(i);
                simulatorForcesHistory.Remove(i);
                simulatorVelocityHistory.Remove(i);
                simulatorAccelerationHistory.Remove(i);
                simulatorAngVelHistory.Remove(i);
                simulatorAngAccHistory.Remove(i);
            }

            this.ProcessedCycles = cycle;
        }

        private void PlaybackState_Changed()
        {

            if (this.playbackState == EPlaybackState.PLAYING)
            {
                this.ControlerStartStop.Image = Force_Studio.Properties.Resources.Pause;
                this.ControlerCycleSlider.Enabled = false;
            }
            else
            {
                this.ControlerStartStop.Image = Force_Studio.Properties.Resources.Start;
                this.ControlerCycleSlider.Enabled = true;
            }

        }

        private void RenderForces(I3dObject objRef, long cycle)
        {
            if (objRef == null)
                return;

            Vertex3 centerOfMass = new Vertex3();
            centerOfMass.Position = objRef.Position;
            centerOfMass.DiffuseColor = centerOfMass.SpecularColor = Color.OrangeRed;
            Vertex3 endPoint = new Vertex3();
            Primitive3 arrow = new Primitive3("", EPrimitiveType.ARROW);

            // Saving Forces
            foreach(I3dForce force in objRef.Forces)
            {
                if (force.ActingLevel == EActingLevel.WORLD || force.AffObject == objRef)
                {
                    endPoint.Position = force.Magnitude;
                    arrow.Name = (~force.Magnitude).ToString();
                    arrow.Vertices = new List<Vertex3>();
                    arrow.Vertices.Add(centerOfMass);
                    arrow.Vertices.Add(endPoint);
                    simulatorForcesHistory[cycle].Add(arrow);
                }
            }  
        }

        //--------------------------------

    }
}