﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpGL;
using ForceLib;
using ForceLib.ForceLib3D;
using System.Windows.Forms;

namespace Force_Studio
{
    public class Simulator
    {
        
        public delegate void PropertyChanged();

        public enum DrawItems { OBJECTS, FORCES, VELOCITIES, ACCELERATIONS, ANGVELOCITIES, ANGACCELERATIONS };
        //***************Attributes******************
        private long currentCycle;
        private Dictionary<DrawItems, Dictionary<long, List<Primitive3>>> cycleDictionaries;
        private Timer timer;
        private OptionsForm option_Form;
        private OpenGL gl;
        private OpenGLCtrl gl_ctrl;
        private List<Primitive3> list;
        private Dictionary<long, List<Primitive3>> dic;
        private List<DrawItems> itemsToDraw;
        private long stopCycle;
        private uint drawStyle = OpenGL.GLU_FILL;
        I3dWorldManager worldManager;

        //*******************************************
        //-------------------------------------------

        //****************Events******************
        public event PropertyChanged CurrentCycleChanged;
        //-------------------------------------------

        //****************Priorties******************

        public I3dWorldManager WorldManager
        {
            get { return this.worldManager; }
            set { this.worldManager = value; }
        }

        public long CurrentCycle
        {
            get { return this.currentCycle; }
            set
            {
                this.currentCycle = value;
                if (this.CurrentCycleChanged != null)
                    this.CurrentCycleChanged();
            }
        }

        public long StopCycle
        {
            get { return this.stopCycle; }
            set { this.stopCycle = value; }

        }

        public Dictionary<DrawItems, Dictionary<long, List<Primitive3>>> CycleDictionary
        {
            get { return this.cycleDictionaries; }
        }

        public OptionsForm Option_Form
        {
            get { return this.option_Form; }
            set { this.option_Form = value; }
        }

        public OpenGL GL
        {
            get { return this.gl; }
        }

        public OpenGLCtrl GL_CTRL
        {
            get { return this.gl_ctrl; }
            set { this.gl_ctrl = value; }
        }

        public uint DrawStyle
        {
            get { return this.drawStyle; }
            set { this.drawStyle = value; }
        }

        //**********************************************
        //----------------------------------------------


        //*******************Constructor****************
        public Simulator(I3dWorldManager worldManager, OpenGLCtrl opengl_ctrl)
        {
            this.worldManager = worldManager;
            this.CurrentCycle = 0;
            this.itemsToDraw = new List<DrawItems>();
            this.gl_ctrl = opengl_ctrl;
            this.gl_ctrl.OpenGLDraw += new PaintEventHandler(draw);
            this.gl = opengl_ctrl.OpenGL;
            this.stopCycle = 0;
            this.option_Form = new OptionsForm(this);
            this.dic = new Dictionary<long, List<Primitive3>>();
            this.list = new List<Primitive3>();
            this.cycleDictionaries = new Dictionary<DrawItems, Dictionary<long, List<Primitive3>>>();
            //initialoze the timer 
            this.timer = new Timer();
            setMotionSpeed(1);
            //set the timer main function
            timer.Tick += new EventHandler(this.toNextCycle);
            this.CurrentCycleChanged += new PropertyChanged(LoadCurrentCycle);
            this.setView();
        }
        //************************************************
        //------------------------------------------------


        //*************************Control Methods*******************
        //add dictionary of items to the dictionaries  to draw
        public void AddDictionary(Simulator.DrawItems drawItem, Dictionary<long, List<Primitive3>> dictionary)
        {
            if (this.cycleDictionaries.ContainsKey(drawItem))
                this.cycleDictionaries.Remove(drawItem);
            this.cycleDictionaries.Add(drawItem, dictionary);
        }

        //remove dictionary from the dictionaries to draw
        public void RemoveDictionary(Simulator.DrawItems drawItem)
        {
            if (this.cycleDictionaries.ContainsKey(drawItem))
            {
                this.cycleDictionaries.Remove(drawItem);
                this.RemoveItemToDraw(drawItem);
            }
        }
        //add item to the items to be drawn from the dictinary 
        public bool AddItemToDraw(Simulator.DrawItems drawItem)
        {
            if (this.itemsToDraw.Contains(drawItem))
                return true;
            if (this.cycleDictionaries.ContainsKey(drawItem))
            {
                this.itemsToDraw.Add(drawItem);
                return true;
            }
            else
            {
                return false;
            }
        }

        //remove item from the items to be drawn
        public void RemoveItemToDraw(Simulator.DrawItems drawItem)
        {
            if (this.itemsToDraw.Contains(drawItem))
                this.itemsToDraw.Remove(drawItem);
        }

        //go to the next cycle
        public void toNextCycle(object sender, EventArgs e)
        {
            if (currentCycle <= stopCycle)
                this.CurrentCycle++;

            this.option_Form.cycleNum.Text = currentCycle.ToString();
            this.option_Form.time.Text = (currentCycle * worldManager.CycleDuration).ToString();
        }

        //specify mostion speed
        public void setMotionSpeed(float x)
        {
            this.timer.Interval = (int)((1/x) * worldManager.CycleDuration * 1000);
        }
        //go to specific second 
        public void GoToSecond(float second)
        {
            if (second < 0)
                throw new Exception("Zero is not valid value for time");
            else
            {
                CurrentCycle = (int)(second / (worldManager.CycleDuration / 1000));
            }
        }
        //Start the show
        public void Start()
        {
            this.timer.Start();
        }

        //pause the show
        public void Pause()
        {
            this.timer.Stop();
        }

        //stop the show
        public void Stop()
        {
            this.timer.Stop();
            this.CurrentCycle = 0;
        }


        //set the frame rate of drawing
        public void setFrameRate(float framerate)
        {
            this.gl_ctrl.FrameRate = framerate;
        }

        //rotete the scene
        public void RotateScene(float angleX, float angleY, float angleZ)
        {
            gl.Rotate(angleX, angleY, angleZ);

        }

        //translate
        public void TranslateScene(float stepX, float stepY, float stepZ)
        {
            gl.Translate(stepX, stepY, stepZ);
        }

        //scale the scene 
        public void Scale(float scale)
        {
            gl.Scale(scale, scale, scale);
        }

        //***********************************************************


        //*************************Draw methods**********************
        //main draw method 
        public void draw(object sender, PaintEventArgs e)
        {
            try
            {
                if (this.cycleDictionaries.Count == 0)
                {
                    //draw the initialize frame
                    return;
                }


                GL.Clear(OpenGL.COLOR_BUFFER_BIT | OpenGL.DEPTH_BUFFER_BIT);
                drawCoordinates(0f, 0f, 0f, 100);
                foreach (Simulator.DrawItems drawItem in itemsToDraw)
                {
                    dic = this.cycleDictionaries[drawItem];
                    list = dic[currentCycle];

                    foreach (Primitive3 prim3 in list)
                    {
                        gl.Color(prim3.Vertices[0].DiffuseColor.R, prim3.Vertices[0].DiffuseColor.G, prim3.Vertices[0].DiffuseColor.B);
                        switch (prim3.Type)
                        {
                            case EPrimitiveType.SPHERE:
                                {
                                    drawSphere(prim3.Vertices[0].Position, new Vector3(0, 10, 0), (float)prim3.PrimitiveInfo[0], drawStyle, OpenGL.GLU_SMOOTH);
                                    break;
                                }

                            case EPrimitiveType.DEFAULT:
                                {
                                    
                                    switch (prim3.VertexOrder)
                                    {
                                        case EVertexOrder.LINELIST:
                                            gl.Begin(OpenGL.LINES);
                                            break;
                                        case EVertexOrder.LINELOOP:
                                            gl.Begin(OpenGL.LINE_LOOP);
                                            break;
                                        case EVertexOrder.LINESTRIP:
                                            gl.Begin(OpenGL.LINE_STRIP);
                                            break;
                                        case EVertexOrder.POINTS:
                                            gl.Begin(OpenGL.POINTS);
                                            break;
                                        case EVertexOrder.POLYGON:
                                            gl.Begin(OpenGL.POLYGON);
                                            break;
                                        case EVertexOrder.QUADLIST:
                                            gl.Begin(OpenGL.QUADS);
                                            break;
                                        case EVertexOrder.QUADSTRIP:
                                            gl.Begin(OpenGL.QUAD_STRIP);
                                            break;
                                        case EVertexOrder.TRIANGLELIST:
                                            gl.Begin(OpenGL.TRIANGLES);
                                            break;
                                        case EVertexOrder.TRIANGLESTRIP:
                                            gl.Begin(OpenGL.TRIANGLE_STRIP);
                                            break;
                                        case EVertexOrder.TRIANGLEFAN:
                                            gl.Begin(OpenGL.TRIANGLE_FAN);
                                            break;
                                        default:
                                            gl.Begin(OpenGL.LINES);
                                            break;
                                    }

                                    for (int i = 0; i < prim3.Vertices.Count; i++)
                                        gl.Vertex(prim3.Vertices[i].Position.X, prim3.Vertices[i].Position.Y, prim3.Vertices[i].Position.Z);
                                    gl.End();
                                    break;
                                }

                            case EPrimitiveType.ARROW:
                                this.drawArrow(prim3.Vertices[0].Position, prim3.Vertices[1].Position, 0.2, 0.08, OpenGL.GLU_FILL);
                                break;
                            case EPrimitiveType.CYLINDAR:
                                break;
                            case EPrimitiveType.DISK:
                                break;
                            case EPrimitiveType.PARTIALDISK:
                                break;
                        }
                    }
                }
                gl.Flush();
            }
            catch (Exception excep)
            {

            }
        }


        //method to set the view
        public void setView()
        {
            // Set viewport to window dimensions.
            gl.MatrixMode(OpenGL.MODELVIEW);
            gl.LoadIdentity();
            gl.Viewport(0, 0, this.gl_ctrl.Size.Width, this.gl_ctrl.Size.Height);
            gl.Ortho(0, 0, this.gl_ctrl.Size.Width, this.gl_ctrl.Size.Height, 0, 0);
            gl.MatrixMode(OpenGL.PROJECTION);
            gl.LoadIdentity();
            gl.Perspective(45, 1, 1, 1000);
            gl.Translate(0, 0, -2);
            //*************************************

            //Camera position
            gl.LookAt(5, 5, -5, 0, 0, 0, 0, 1, 0);
            //lightning
            GL.Clear(OpenGL.COLOR_MATERIAL);
            gl.ColorMaterial(OpenGL.FRONT, OpenGL.SPECULAR);
            gl.ColorMaterial(OpenGL.BACK, OpenGL.SPECULAR);
            gl.Enable(OpenGL.LIGHTING);

            gl.Enable(OpenGL.LIGHT0);
            gl.Enable(OpenGL.COLOR_MATERIAL);

            float[] n = { 2f, 2f, -2f };
            float[] c = { 1.0f, 1.0f, 1.0f, 1.0f };

            gl.Light(OpenGL.LIGHT0, OpenGL.POSITION, n);
            gl.Light(OpenGL.LIGHT0, OpenGL.SPECULAR, c);
            //****************************************


            //scale
            gl.Scale(1.0, 1.0, 1.0);

            //clear
            gl.ClearColor(1, 1, 1, 0);
            gl.ClearDepth(1.0f);
            gl.Clear(OpenGL.COLOR_BUFFER_BIT | OpenGL.DEPTH_BUFFER_BIT | OpenGL.COLOR_MATERIAL);

        }


        //method to draw coordinats
        public void drawCoordinates(float x, float y, float z, float axisSize)
        {
            #region Draw Axes

            // draw a line along the z-axis
            gl.Color(1d, 0d, 0d);
            GL.Begin(OpenGL.LINES);
            gl.Normal3d(10d, 10d, 10d);
            GL.Vertex(x, y, z - axisSize);
            gl.Normal3d(10d, 10d, 0);
            GL.Vertex(x, y, z + axisSize);
            GL.End();
            // draw a line along the y-axis
            GL.Color(0d, 1d, 0d);
            GL.Begin(OpenGL.LINES);
            GL.Vertex(x, y - axisSize, z);
            GL.Vertex(x, y + axisSize, z);
            GL.End();
            // draw a line along the x-axis
            GL.Color(0d, 0d, 1d);
            GL.Begin(OpenGL.LINES);
            GL.Vertex(x - axisSize, y, z);
            GL.Vertex(x + axisSize, y, z);

            GL.End();
            GL.PopMatrix();
            #endregion Draw Axes
        }

        //method to draw sphere
        public void drawSphere(Vector3 centerposition, Vector3 direction, double radius, uint drawStyle, uint normal)
        {
            GL.PushMatrix();
            GL.Translate(centerposition.X, centerposition.Y, centerposition.Z);
            //rotating
            /*System.Windows.Forms.MessageBox.Show((Math.Acos((double)Vector3.GetCos(direction, new Vector3(0, 0, 1))) * 180 / Math.PI).ToString());
            if (direction.X <= 0)        
                GL.glRotatef(-(float)(Math.Acos((double)Vector3.GetCos(direction, new Vector3(0, 0, 1))) * 180 / Math.PI), 0, 0, 1);
            else
                GL.glRotatef((float)(Math.Acos((double)Vector3.GetCos(direction, new Vector3(0, 0, 1))) * 180 / Math.PI), 0, 0, 1);
            if (direction.X <= 0)
                GL.glRotatef(-(float)(Math.Acos((double)Vector3.GetCos(direction, new Vector3(0, 0, 1))) * 180 / Math.PI), 0, 1, 0);
            else
                GL.glRotatef((float)(Math.Acos((double)Vector3.GetCos(direction, new Vector3(0, 0, 1))) * 180 / Math.PI), 0, 1, 0);

            
            /*/
            IntPtr n = GL.NewQuadric();
            GL.QuadricDrawStyle(n, drawStyle);
            GL.QuadricNormals(n, 16);
            GL.Sphere(n, radius, 16, 16);

            GL.DeleteQuadric(n);
            GL.PopMatrix();
        }


        //method to draw an arrow
        public void drawArrow(Vector3 point1, Vector3 point2, double heightOfHead, double radiusOfHead, uint drawStyle)
        {
            if (~(point1 - point2) == 0)
                return;
            GL.PushMatrix();
            if (point2.Y != 0 || point2.X != 0)
            {
                GL.Translate(point1.X, point1.Y, point1.Z);
                double angZ = (Math.Acos(Vector3.GetCos(new Vector3(0f, 0f, 1f), point2)) * 180 / 3.1415926);
                double angy = (Math.Acos(Vector3.GetCos(new Vector3(0f, 1f, 0), new Vector3(point2.X, point2.Y, 0))) * 180 / 3.1415926);

                if (point2.X < 0)
                    gl.Rotate(angy, 0, 0, 1);
                else
                    gl.Rotate(angy, 0, 0, -1);
                gl.Rotate(angZ, -1, 0, 0);

            }
            else if (point2.Z == 0)
                return;

            else if (point2.Z < 0)
                gl.Rotate(180, 1, 0, 0);

            gl.Translate(0, 0, (~point2));
            IntPtr n = GL.NewQuadric();
            GL.QuadricDrawStyle(n, drawStyle);
            GL.QuadricNormals(n, 40);
            GL.Cylinder(n, radiusOfHead, 0, heightOfHead, 32, 32);
            GL.DeleteQuadric(n);
            gl.PopMatrix();
            gl.LineWidth(3);
            GL.Begin(OpenGL.LINES);
            gl.Vertex(point1.X, point1.Y, point1.Z);
            gl.Vertex(point2.X + point1.X, point2.Y + point1.Y, point2.Z + point1.Z);
            gl.End();
            gl.LineWidth(1);

        }

        private void LoadCurrentCycle()
        {
            if(this.gl_ctrl == null)
                return;
            ((MainAppForm)this.gl_ctrl.FindForm()).CurrentCycle = this.currentCycle;
        }

    }
}