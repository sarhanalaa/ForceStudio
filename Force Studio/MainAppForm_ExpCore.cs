﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using Force_Studio_Lib;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio
{
    public partial class MainAppForm
    {

        // Enumerations
        public enum EProcessState { INITIALIZED, MODIFIED, PROCESSING, PROCESSED, STOPPED };
        //----------------------------


        // Fields

        EProcessState processState;
        long processedCycles;

        float experimentLength;
        float cyclesPerSecond;
        long totalCyclesNumber;

        long currentCycle;

        float motionSpeed;

        Dictionary<I3dObject, Panel> editingObjectsPanel;
        Dictionary<I3dForce, Panel> editingForcesPanel;

        //----------------------------


        // Delegates
        public delegate void PropertyChanged();
        public delegate void StateChanged();
        //----------------------------


        // Events
        public event PropertyChanged ExperimentLengthChanged;
        public event PropertyChanged CyclesPerSecondChanged;
        public event PropertyChanged TotalCyclesNumberChanged;
        public event StateChanged ProcessStateChanged;
        public event StateChanged ProcessedCyclesChanged;
        public event PropertyChanged CurrentCycleChanged;
        //----------------------------


        // Properties

        private EProcessState ProcessState
        {
            get { return this.processState; }
            set
            {
                this.processState = value;
                if (this.ProcessStateChanged != null)
                    this.ProcessStateChanged();
            }
        }

        private long ProcessedCycles
        {
            get { return this.processedCycles; }
            set
            {
                if (value < 0 || value > this.totalCyclesNumber)
                    return;
                this.processedCycles = value;
                if (this.ProcessedCyclesChanged != null)
                    this.ProcessedCyclesChanged();
            }
        }

        private float ExperimentLength
        {
            get { return this.experimentLength; }
            set
            {
                if (value < 0)
                    return;
                this.experimentLength = value;
                if (this.ExperimentLengthChanged != null)
                    this.ExperimentLengthChanged();
            }
        }

        private float CyclesPerSecond
        {
            get { return this.cyclesPerSecond; }
            set
            {
                if (value < 0)
                    return;
                this.cyclesPerSecond = value;
                if (this.CyclesPerSecondChanged != null)
                    this.CyclesPerSecondChanged();
            }
        }

        private long TotalCyclesNumber
        {
            get { return this.totalCyclesNumber; }
            set
            {
                if (value < 0)
                    return;
                    this.totalCyclesNumber = value;
                if (this.TotalCyclesNumberChanged != null)
                    this.TotalCyclesNumberChanged();
            }
        }

        public long CurrentCycle
        {
            get { return this.currentCycle; }
            set
            {
                if (value < 0 || value >= processedCycles)
                    return;
                this.currentCycle = value;
                if (this.CurrentCycleChanged != null)
                    this.CurrentCycleChanged();
            }
        }

        //----------------------------


        // Methods

        private void ProcessState_Changed()
        {

            this.ExpInfo_ProcessState.Text = this.ProcessState.ToString();

            switch (this.ProcessState)
            {
                case EProcessState.INITIALIZED:
                    this.StateHistory.Enabled = false;
                    this.Exp_Elements.Enabled = true;
                    this.ControlerExpLength.ReadOnly = false;
                    this.ControlerCyclesNum.ReadOnly = false;
                    this.ProcessControl.Text = "Process";

                    break;

                case EProcessState.MODIFIED:
                    this.StateHistory.Enabled = true;
                    this.Exp_Elements.Enabled = true;
                    this.ControlerExpLength.ReadOnly = false;
                    this.ControlerCyclesNum.ReadOnly = false;
                    this.ProcessControl.Text = "Process";

                    break;

                case EProcessState.PROCESSED:
                    this.StateHistory.Enabled = true;
                    this.Exp_Elements.Enabled = true;
                    this.ControlerExpLength.ReadOnly = false;
                    this.ControlerCyclesNum.ReadOnly = false;
                    this.ProcessControl.Text = "Reprocess";

                    break;

                case EProcessState.STOPPED:
                    this.StateHistory.Enabled = true;
                    this.Exp_Elements.Enabled = true;
                    this.ControlerExpLength.ReadOnly = false;
                    this.ControlerCyclesNum.ReadOnly = false;
                    this.ProcessControl.Text = "Resume";

                    break;

                case EProcessState.PROCESSING:
                    this.StateHistory.Enabled = false;
                    this.Exp_Elements.Enabled = false;
                    this.ControlerExpLength.ReadOnly = true;
                    this.ControlerCyclesNum.ReadOnly = true;
                    this.ProcessControl.Text = "Stop";

                    break;
            }
            
        }

        private void ProcessedCycles_Changed()
        {
            this.ExpInfo_ProcessedCycles.Text = this.ProcessedCycles.ToString();
            this.ControlerCycleSlider.Maximum = (int)this.ProcessedCycles;
            this.ExpInfo_ProcessProgress.Value = (int)this.ProcessedCycles;
            this.StateHistory_Cycle.Maximum = this.ProcessedCycles - 1;
            this.Histogram_TimeScroll.Maximum = (int)(this.ProcessedCycles - this.HisLength - 1);
            if (this.HisLength > this.ProcessedCycles - this.HisFirstCycle)
                this.HisLength = (this.ProcessedCycles - this.HisFirstCycle - 1) / 2;
            this.simulator.StopCycle = this.ProcessedCycles - 1;
            if (this.ProcessedCycles <= this.simulator.CurrentCycle)
                this.simulator.CurrentCycle = this.ProcessedCycles - 1;
        }

        private void ExperimentLength_Changed()
        {
            if (this.ControlerExpLength.Text != this.ExperimentLength.ToString())
                this.ControlerExpLength.Text = this.ExperimentLength.ToString();
            else
            {
                this.ExpInfo_ExpLength.Text = this.ExperimentLength.ToString() + " Sec.";
                this.TotalCyclesNumber = (long)(this.CyclesPerSecond * this.ExperimentLength);
            }
        }

        private void CyclesPerSecond_Changed()
        {
            if (this.ControlerCyclesNum.Text != this.CyclesPerSecond.ToString())
                this.ControlerCyclesNum.Text = this.CyclesPerSecond.ToString();

            this.worldManager.CycleDuration = 1F / this.CyclesPerSecond;
            this.ExpInfo_ExpCPS.Text = this.CyclesPerSecond.ToString() + " CPS";
            this.TotalCyclesNumber = (long)(this.CyclesPerSecond * this.ExperimentLength);
            this.ControlerCycleSlider.LargeChange = (int)this.CyclesPerSecond;
            this.simulator.setMotionSpeed(motionSpeed);

        }

        private void TotalCyclesNumber_Changed()
        {
            this.ExpInfo_TotalCyclesNumber.Text = this.TotalCyclesNumber.ToString();
            this.ControlerCycleSlider.TickFrequency = (int)(this.TotalCyclesNumber / 10);
            this.ExpInfo_ProcessProgress.Maximum = (int)(this.TotalCyclesNumber);
        }

        private void CurrentCycle_Changed()
        {
            this.ControlerCurrentCycle.Text = this.CurrentCycle.ToString();
            this.ControlerCycleSlider.Value = (int)this.CurrentCycle;
        }
        //----------------------------


        // Objects Methods

        private void AddNewObject(I3dObject objRef)
        {
            if (objRef == null)
                return;

            this.worldManager.AllObjects.Add(objRef);
            if (objRef is IEditable)
            {
                if (editingObjectsPanel.ContainsKey(objRef))
                    editingObjectsPanel.Remove(objRef);
                editingObjectsPanel.Add(objRef, ((IEditable)objRef).GenerateEditingPanel(null));

                // Editing Object
                ((IEditable)objRef).EditingInfo = new object[] { (object)0, new SeekDelegate(this.Seek) };
                EditingForm frmEdt = new EditingForm();
                frmEdt.Text = "Editing Object.. " + objRef.ToString() + " On Cycle " + this.StateHistory_Cycle.Value.ToString();
                editingObjectsPanel[objRef].Location = new Point(0, 0);
                frmEdt.Controls.Add(editingObjectsPanel[objRef]);
                frmEdt.Show(this);

            }

            UpdateObjectsList();

        }

        private void RemoveObject(I3dObject objRef)
        {
            if (objRef == null)
                return;

            if (objRef is IEditable)
                editingObjectsPanel.Remove(objRef);

            foreach (I3dForce force in this.worldManager.WorldForces)
                if (force.ActingLevel == EActingLevel.OBJECT && force.AffObject == objRef)
                    this.worldManager.WorldForces.Remove(force);

            worldManager.AllObjects.Remove(objRef);

            UpdateObjectsList();
        }

        private void UpdateObjectsList()
        {
            this.StateHistory_ObjectsList.Items.Clear();
            this.StateHistory_ObjectsList.Items.AddRange(this.worldManager.AllObjects.ToArray());

            this.Exp_ObjectsList.Items.Clear();
            this.Exp_ObjectsList.Items.AddRange(this.worldManager.AllObjects.ToArray());

            if (worldManager.AllObjects.Count > 0)
            {
                this.StateHistory_ObjectsList.SelectedIndex = 0;
                this.Exp_ObjectsList.SelectedIndex = 0;
            }

        }

    }

}