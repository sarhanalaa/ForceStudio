﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ForceLib.ForceLib3D;

namespace Force_Studio
{
    public partial class WorldManagerEditor : Form
    {
        I3dWorld world;

        public WorldManagerEditor(I3dWorld world)
        {
            InitializeComponent();
            this.world = world;
        }

        private void WorldManagerEditor_Shown(object sender, EventArgs e)
        {
            this.GravityAcceleration.Text = world.Gravity.ToString();
            this.WorldVisocity.Text = world.Viscosity.ToString();
        }

        private void WorldManagerEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            bool done = true;

            try
            {
                this.world.Viscosity = Convert.ToSingle(this.WorldVisocity.Text);
            }catch{ done = false;}

            try { this.world.Gravity = Convert.ToSingle(this.GravityAcceleration.Text); }
            catch{done = false;}

            if (!done)
            {
                if (MessageBox.Show("Some of the Values are invalid and They will not be saved." + Environment.NewLine
                        + "Do you want to continue anyway?", "Editing World", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    this.Hide();
            }
            else
                this.Hide();

        }
    }
}
