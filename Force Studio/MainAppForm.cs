﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using Force_Studio_Lib;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio
{
    public partial class MainAppForm : Form
    {

        private void alert(String message, MessageBoxIcon icon)
        {
            MessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, icon);
        }

        private void MainAppForm_Load(object sender, EventArgs e)
        {
            /*
            MainAppForm Initialization
             */
            this.Text = Application.ProductName + " " + Application.ProductVersion;
            //------------------------------------

        }

        private void newExpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CreateNewExperiment();
        }

        private void ControlerExpLength_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.ExperimentLength = Convert.ToSingle(this.ControlerExpLength.Text);
            }
            catch
            {
                this.ExperimentLength = 30;
            }
        }

        private void ControlerCyclesNum_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.CyclesPerSecond = Convert.ToSingle(this.ControlerCyclesNum.Text);
                if (this.CyclesPerSecond > 10000)
                    this.CyclesPerSecond = 10000;
            }
            catch
            {
                this.CyclesPerSecond = 100;
            }
        }

        private void ControlerStartStop_Click(object sender, EventArgs e)
        {
            Play();
        }

        private void ControlerLengthUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ExperimentLengthChanged();
        }

        private void StateHistory_ObjectsList_SelectedValueChanged(object sender, EventArgs e)
        {
            UpdateObjectState((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem, (int)this.StateHistory_Cycle.Value);
            GenerateHistogramObjectElementsList((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem);
            CalculateHistogramMaxValue((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem, this.HisElements);
            this.StateHistory_EditObject.Enabled = (this.StateHistory_ObjectsList.SelectedItem is IEditable); 

        }

        private void StateHistory_Cycle_ValueChanged(object sender, EventArgs e)
        {
            UpdateObjectState((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem, (int)this.StateHistory_Cycle.Value);
            StateHistory_CycleTime.Text = (Math.Round((int)StateHistory_Cycle.Value * this.worldManager.CycleDuration,4)).ToString() + " s";
        }

        private void UpdateObjectState(IAnalyzable objRef, int cycle)
        {
            Dictionary<String, Object> objState = new Dictionary<string,object>();
            if (objRef == null || !objRef.StateHistory.TryGetValue(cycle, out objState))
                return;

            this.StateHistory_StateViewer.Text = objRef.QueryState(cycle);
        }


        private void UpdateHistogramView(IAnalyzable objRef, String[] elements, Color[] elemColors, long firstCycle, long length, int W, int H, float maxValue)
        {
            System.Drawing.Graphics HV = this.HistogramView.CreateGraphics();

            long lastCycle = firstCycle + length - 1;

            if (length == 0 || maxValue == 0 || elements.Length < 1)
                return;

            float offset = W / length;
            float Y = H / 2;
            float HMX = H / (2 * maxValue);

            // Checking for Colors
            if (elements.Length > elemColors.Length)
            {
                elemColors = new Color[elements.Length];

                Random colorRandomizer = new Random(elements.Length);

                for (int j = 0; j < elements.Length; j++)
                    elemColors[j] = Color.FromArgb(colorRandomizer.Next(0, 255), 
                                                   colorRandomizer.Next(0, 255), 
                                                   colorRandomizer.Next(0, 255));
            }


            // Drawing Graphs
            PointF[] points;
            for (int j = 0; j < elements.Length; j++ )
            {
                points = new PointF[length];

                for (long i = firstCycle; i <= lastCycle && objRef.StateHistory.ContainsKey(i); i++)
                {
                    points[i] = new PointF();
                    points[i].X = i * offset;

                    if (objRef.StateHistory[i].ContainsKey(elements[i]))
                        points[i].Y = Y - (float)objRef.StateHistory[i][elements[i]] * HMX;
                    else if (i != 0)
                        points[i].Y = points[i - 1].Y;
                    else
                        points[i].Y = 0;
                }

                //Drawing Graph Element
                HV.DrawLines(new Pen(elemColors[j]), points);
            }
            
        }

        private void ProcessControl_Click(object sender, EventArgs e)
        {

            switch (this.ProcessState)
            {

                case EProcessState.INITIALIZED:
                    if (TProcess.ThreadState == ThreadState.Unstarted)
                        TProcess.Start();
                    else if (TProcess.ThreadState == ThreadState.Stopped)
                    {
                        TProcess = new Thread(new ThreadStart(Process));
                        TProcess.Start();
                    }
                    else if (TProcess.ThreadState == ThreadState.Suspended)
                    {
                        TProcess = new Thread(new ThreadStart(Process));
                        TProcess.Start();
                    }
                    break;

                case EProcessState.MODIFIED:
                    TProcess = new Thread(new ThreadStart(Process));
                    TProcess.Start();
                    break;

                case EProcessState.PROCESSED:
                    Seek(0);
                    this.ProcessState = EProcessState.INITIALIZED;
                    TProcess = new Thread(new ThreadStart(Process));
                    TProcess.Start();
                    break;

                case EProcessState.PROCESSING:
                    TProcess.Abort();
                    this.ProcessState = EProcessState.STOPPED;
                    break;

                case EProcessState.STOPPED:
                    TProcess = new Thread(new ThreadStart(Process));
                    TProcess.Start();
                    this.ProcessState = EProcessState.PROCESSING;
                    break;

            }

        }

        private void StateHistory_StateViewer_EnabledChanged(object sender, EventArgs e)
        {
            if(this.StateHistory_StateViewer.Enabled == true)
                UpdateObjectState((IAnalyzable)this.StateHistory_ObjectsList.SelectedItem, (int)this.StateHistory_Cycle.Value);
        }

        private void StateHistory_Cycle_EnabledChanged(object sender, EventArgs e)
        {
            if (this.StateHistory_Cycle.Enabled == true)
                this.StateHistory_Cycle.Value = this.StateHistory_Cycle.Maximum;
        }

        private void StateHistory_ObjectsList_EnabledChanged(object sender, EventArgs e)
        {
            if (this.StateHistory_ObjectsList.Enabled == true)
                this.StateHistory_ObjectsList.SelectedIndex = this.StateHistory_ObjectsList.Items.Count - 1;
        }

        private void StateHistory_CycleTime_EnabledChanged(object sender, EventArgs e)
        {
            if (!StateHistory_CycleTime.Enabled)
                this.Text = "#";
        }

        private void HistogramView_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Histogram_TimeScroll_ValueChanged(object sender, EventArgs e)
        {
            this.HisFirstCycle = this.Histogram_TimeScroll.Value;
        }

        private void Histogram_ZoomIn_Click(object sender, EventArgs e)
        {
            this.HisLength = (long)Math.Ceiling(0.9 * this.HisLength);
        }

        private void Histogram_ZoomOut_Click(object sender, EventArgs e)
        {
            this.HisLength = (long)Math.Floor(1.111 * this.HisLength);
        }

        private void simulatorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.simulator.Option_Form.Show();
        }

        private void ControlerCycleSlider_ValueChanged(object sender, EventArgs e)
        {
            if (this.ControlerCycleSlider.Value != this.CurrentCycle)
            {
                this.CurrentCycle = this.ControlerCycleSlider.Value;
                this.simulator.CurrentCycle = this.CurrentCycle;
            }
        }

        private void ControlerNextCycle_Click(object sender, EventArgs e)
        {
            this.CurrentCycle++;
            this.simulator.CurrentCycle = this.CurrentCycle;
        }

        private void ControlerPreviousCycle_Click(object sender, EventArgs e)
        {
            this.CurrentCycle--;
            this.simulator.CurrentCycle = this.CurrentCycle;
        }

        private void Exp_ObjectsList_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Exp_ObjectsList.Items.Clear();
            this.Exp_ObjectsList.Items.AddRange(this.worldManager.AllObjects.ToArray());
            this.Exp_ForcesList.Invalidate();
        }

        private void Exp_ForcesList_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }

        private void Exp_ObjectsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Exp_RemoveObject.Enabled = !(this.Exp_ObjectsList.SelectedItem == null);

            this.Exp_ForcesList.Items.Clear();
            if (Exp_ObjectsList.SelectedItem == null)
                this.Exp_ForcesList.Items.AddRange(this.worldManager.WorldForces.ToArray());
            else
            {
                foreach (I3dForce force in this.worldManager.WorldForces)
                    if (force.ActingLevel == EActingLevel.WORLD || force.AffObject == this.Exp_ObjectsList.SelectedItem)
                        this.Exp_ForcesList.Items.Add(force);
            }

        }

        private void Exp_ForcesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Exp_EditForce.Enabled = (this.Exp_ForcesList.SelectedItem != null && this.Exp_ForcesList.SelectedItem is IEditable);

        }

        private void Exp_RemoveObject_Click(object sender, EventArgs e)
        {
            if (this.Exp_ObjectsList.SelectedItem != null)
                RemoveObject((I3dObject)this.Exp_ObjectsList.SelectedItem);
        }

        private void Exp_EditForce_Click(object sender, EventArgs e)
        {
            if (this.Exp_ForcesList.SelectedItem == null || !(this.Exp_ForcesList.SelectedItem is IEditable))
                return;

            I3dForce forceRef = (I3dForce)this.Exp_ForcesList.SelectedItem;
            if (!this.editingForcesPanel.ContainsKey(forceRef))
                this.editingForcesPanel.Add(forceRef, ((IEditable)forceRef).GenerateEditingPanel(null));

            ((IEditable)forceRef).EditingInfo = new object[] {(object)forceRef, (object)0 };
            EditingForm edtFrm = new EditingForm();
            edtFrm.Text = "Editing Force.. " + forceRef.ToString();
            edtFrm.Controls.Add((Control)editingForcesPanel[forceRef]);
            edtFrm.Show(this);

        }

        private void StateHistory_EditObject_Click(object sender, EventArgs e)
        {
            if (this.StateHistory_ObjectsList.SelectedItem == null || !(this.StateHistory_ObjectsList.SelectedItem is IEditable))
                return;
            if (!editingObjectsPanel.ContainsKey((I3dObject)this.StateHistory_ObjectsList.SelectedItem))
                editingObjectsPanel.Add((I3dObject)this.StateHistory_ObjectsList.SelectedItem, ((IEditable)this.StateHistory_ObjectsList.SelectedItem).GenerateEditingPanel(null));

            ((IEditable)this.StateHistory_ObjectsList.SelectedItem).EditingInfo = new object[] {this.StateHistory_Cycle.Value, new SeekDelegate(this.Seek) };
            EditingForm edtFrm = new EditingForm();
            edtFrm.Text = "Editing Object.. " + this.StateHistory_ObjectsList.SelectedItem.ToString() + " on Cycle " + this.StateHistory_Cycle.Value.ToString();
            editingObjectsPanel[(I3dObject)this.StateHistory_ObjectsList.SelectedItem].Location = new Point(0, 0);
            edtFrm.Controls.Add((Control)editingObjectsPanel[(I3dObject)this.StateHistory_ObjectsList.SelectedItem]);
            edtFrm.Show(this);
        }

        private void Exp_ObjectsList_DoubleClick(object sender, EventArgs e)
        {
            UpdateObjectsList();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.worldManagerEditor.Show(this);
        }

        private void Exp_NewObject_Click(object sender, EventArgs e)
        {
            this.NewObjectMenu.Show(this.Exp_NewObject, new Point(0, 22));
        }

        private void newSphere_Click(object sender, EventArgs e)
        {
            AddNewObject(new CSphereObject("Sphere " + this.worldManager.AllObjects.Count, 0.1f, 0.1f,
                new Vector3(0,0,0), new Vector3(0,0,0), new Vector3(0,0,0), new Vector3(0,0,0),
                new Vector3(0,0,0),this.worldManager.WorldForces, this.world));
        }

        private void newPendulum_Click(object sender, EventArgs e)
        {
            AddNewObject(new CPendulumObject("Pendulum " + this.worldManager.AllObjects.Count, 0.1f, 0.1f,
                new Vector3(0, 0, 0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), new Vector3(0, 0, 0),
                new Vector3(0, 0, 0), new Vector3(0, 0, 0), this.worldManager.WorldForces, this.world));
        }

    }
}