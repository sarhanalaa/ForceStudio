﻿using System.Drawing;

namespace Force_Studio
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BlueBox = new System.Windows.Forms.ComboBox();
            this.GreenBox = new System.Windows.Forms.ComboBox();
            this.RedBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.AngAccBox = new System.Windows.Forms.CheckBox();
            this.AngVelBox = new System.Windows.Forms.CheckBox();
            this.AccBox = new System.Windows.Forms.CheckBox();
            this.VelocitiesBox = new System.Windows.Forms.CheckBox();
            this.ForcesBox = new System.Windows.Forms.CheckBox();
            this.ObjectsBox = new System.Windows.Forms.CheckBox();
            this.FrameRateBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cycleNum = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.BlueBox);
            this.groupBox1.Controls.Add(this.GreenBox);
            this.groupBox1.Controls.Add(this.RedBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(106, 123);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "BackGround Color";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Blue";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Green";
            // 
            // BlueBox
            // 
            this.BlueBox.FormattingEnabled = true;
            this.BlueBox.Items.AddRange(new object[] {
            0F,
            0.1F,
            0.2F,
            0.3F,
            0.4F,
            0.5F,
            0.6F,
            0.7F,
            0.8F,
            0.9F,
            1F});
            this.BlueBox.Location = new System.Drawing.Point(48, 95);
            this.BlueBox.Name = "BlueBox";
            this.BlueBox.Size = new System.Drawing.Size(41, 21);
            this.BlueBox.TabIndex = 6;
            this.BlueBox.Text = "0";
            this.BlueBox.SelectedIndexChanged += new System.EventHandler(this.BlueBox_SelectedIndexChanged);
            // 
            // GreenBox
            // 
            this.GreenBox.FormattingEnabled = true;
            this.GreenBox.Items.AddRange(new object[] {
            0F,
            0.1F,
            0.2F,
            0.3F,
            0.4F,
            0.5F,
            0.6F,
            0.7F,
            0.8F,
            0.9F,
            1F});
            this.GreenBox.Location = new System.Drawing.Point(48, 56);
            this.GreenBox.Name = "GreenBox";
            this.GreenBox.Size = new System.Drawing.Size(41, 21);
            this.GreenBox.TabIndex = 5;
            this.GreenBox.Text = "0";
            this.GreenBox.SelectedIndexChanged += new System.EventHandler(this.GreenBox_SelectedIndexChanged);
            // 
            // RedBox
            // 
            this.RedBox.FormattingEnabled = true;
            this.RedBox.Items.AddRange(new object[] {
            0F,
            0.1F,
            0.2F,
            0.3F,
            0.4F,
            0.5F,
            0.6F,
            0.7F,
            0.8F,
            0.9F,
            1F});
            this.RedBox.Location = new System.Drawing.Point(48, 21);
            this.RedBox.Name = "RedBox";
            this.RedBox.Size = new System.Drawing.Size(41, 21);
            this.RedBox.TabIndex = 4;
            this.RedBox.Text = "0";
            this.RedBox.SelectedIndexChanged += new System.EventHandler(this.RedBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Red";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.AngAccBox);
            this.groupBox2.Controls.Add(this.AngVelBox);
            this.groupBox2.Controls.Add(this.AccBox);
            this.groupBox2.Controls.Add(this.VelocitiesBox);
            this.groupBox2.Controls.Add(this.ForcesBox);
            this.groupBox2.Controls.Add(this.ObjectsBox);
            this.groupBox2.Location = new System.Drawing.Point(10, 151);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(170, 165);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Drawing Items";
            // 
            // AngAccBox
            // 
            this.AngAccBox.AutoSize = true;
            this.AngAccBox.Location = new System.Drawing.Point(9, 134);
            this.AngAccBox.Name = "AngAccBox";
            this.AngAccBox.Size = new System.Drawing.Size(158, 17);
            this.AngAccBox.TabIndex = 5;
            this.AngAccBox.Text = "Draw Angular Accelerations";
            this.AngAccBox.UseVisualStyleBackColor = true;
            this.AngAccBox.CheckedChanged += new System.EventHandler(this.AngAccBox_CheckedChanged);
            // 
            // AngVelBox
            // 
            this.AngVelBox.AutoSize = true;
            this.AngVelBox.Location = new System.Drawing.Point(9, 111);
            this.AngVelBox.Name = "AngVelBox";
            this.AngVelBox.Size = new System.Drawing.Size(138, 17);
            this.AngVelBox.TabIndex = 4;
            this.AngVelBox.Text = "Draw Angular Velocities";
            this.AngVelBox.UseVisualStyleBackColor = true;
            this.AngVelBox.CheckedChanged += new System.EventHandler(this.AngVelBox_CheckedChanged);
            // 
            // AccBox
            // 
            this.AccBox.AutoSize = true;
            this.AccBox.Location = new System.Drawing.Point(9, 88);
            this.AccBox.Name = "AccBox";
            this.AccBox.Size = new System.Drawing.Size(118, 17);
            this.AccBox.TabIndex = 3;
            this.AccBox.Text = "Draw Accelerations";
            this.AccBox.UseVisualStyleBackColor = true;
            this.AccBox.CheckedChanged += new System.EventHandler(this.AccBox_CheckedChanged);
            // 
            // VelocitiesBox
            // 
            this.VelocitiesBox.AutoSize = true;
            this.VelocitiesBox.Location = new System.Drawing.Point(9, 65);
            this.VelocitiesBox.Name = "VelocitiesBox";
            this.VelocitiesBox.Size = new System.Drawing.Size(98, 17);
            this.VelocitiesBox.TabIndex = 2;
            this.VelocitiesBox.Text = "Draw Velocities";
            this.VelocitiesBox.UseVisualStyleBackColor = true;
            this.VelocitiesBox.CheckedChanged += new System.EventHandler(this.VelocitiesBox_CheckedChanged);
            // 
            // ForcesBox
            // 
            this.ForcesBox.AutoSize = true;
            this.ForcesBox.Location = new System.Drawing.Point(9, 42);
            this.ForcesBox.Name = "ForcesBox";
            this.ForcesBox.Size = new System.Drawing.Size(86, 17);
            this.ForcesBox.TabIndex = 1;
            this.ForcesBox.Text = "Draw Forces";
            this.ForcesBox.UseVisualStyleBackColor = true;
            this.ForcesBox.CheckedChanged += new System.EventHandler(this.ForcesBox_CheckedChanged);
            // 
            // ObjectsBox
            // 
            this.ObjectsBox.AutoSize = true;
            this.ObjectsBox.Location = new System.Drawing.Point(9, 19);
            this.ObjectsBox.Name = "ObjectsBox";
            this.ObjectsBox.Size = new System.Drawing.Size(91, 17);
            this.ObjectsBox.TabIndex = 0;
            this.ObjectsBox.Text = "Draw Objects";
            this.ObjectsBox.UseVisualStyleBackColor = true;
            this.ObjectsBox.CheckedChanged += new System.EventHandler(this.ObjectsBox_CheckedChanged);
            // 
            // FrameRateBox
            // 
            this.FrameRateBox.FormattingEnabled = true;
            this.FrameRateBox.Items.AddRange(new object[] {
            16F,
            24F});
            this.FrameRateBox.Location = new System.Drawing.Point(22, 38);
            this.FrameRateBox.Name = "FrameRateBox";
            this.FrameRateBox.Size = new System.Drawing.Size(61, 21);
            this.FrameRateBox.TabIndex = 7;
            this.FrameRateBox.Text = "24";
            this.FrameRateBox.SelectedIndexChanged += new System.EventHandler(this.FrameRateBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Fram Rate Per Second";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(8, 78);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(100, 17);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "Line Style Draw";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(7, 319);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 9;
            // 
            // cycleNum
            // 
            this.cycleNum.AutoSize = true;
            this.cycleNum.Location = new System.Drawing.Point(16, 331);
            this.cycleNum.Name = "cycleNum";
            this.cycleNum.Size = new System.Drawing.Size(36, 13);
            this.cycleNum.TabIndex = 9;
            this.cycleNum.Text = "Green";
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.Location = new System.Drawing.Point(153, 331);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(36, 13);
            this.time.TabIndex = 11;
            this.time.Text = "Green";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.FrameRateBox);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Location = new System.Drawing.Point(129, 11);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(128, 123);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Other";
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 383);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.time);
            this.Controls.Add(this.cycleNum);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsForm";
            this.Text = "OptionsForm";
            this.Load += new System.EventHandler(this.OptionsForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OptionsForm_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox RedBox;
        private System.Windows.Forms.ComboBox GreenBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox BlueBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox AccBox;
        private System.Windows.Forms.CheckBox VelocitiesBox;
        private System.Windows.Forms.CheckBox ForcesBox;
        private System.Windows.Forms.CheckBox ObjectsBox;
        private System.Windows.Forms.CheckBox AngAccBox;
        private System.Windows.Forms.CheckBox AngVelBox;
        private System.Windows.Forms.ComboBox FrameRateBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label cycleNum;
        public System.Windows.Forms.Label time;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}