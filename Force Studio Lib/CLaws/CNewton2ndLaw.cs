﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio_Lib
{
    public class CNewton2ndLaw : IPhysicalLaw<I3dObject, I3dForce>
    {
       

        // Properties

        public Object[] LawInfo
        {
            get { return null; }
            set { return; }
        }

        //------------------------------------------


        // Methods
        public void ApplyLaw(I3dObject[] objectRef)
        {

            if (objectRef == null || objectRef.Count() != 1 || objectRef[0] == null)
                throw new Exception("Can't Apply Newton's 2nd Law on a null Reference Object");

            Vector3 netForce = new Vector3(0, 0, 0);

            foreach (I3dForce force in objectRef[0].Forces)
            {
                if (force.ActingLevel == EActingLevel.WORLD || force.AffObject == objectRef[0])
                    netForce += force.Magnitude;
            }

            if (~netForce != 0)
            {
                if (!objectRef[0].Hanged)
                {
                    objectRef[0].Acceleration = netForce / objectRef[0].Mass;
                }
                else
                {
                    Vector3 axis = objectRef[0].HangPoint - objectRef[0].Position;

                    netForce /= objectRef[0].Mass * ~axis;

                    Vector3 angAccNormal = Vector3.LeftHandRotaionNormal(netForce, axis);

                    objectRef[0].AngAcceleration = (~(netForce * Vector3.GetSin(netForce, axis))) * (angAccNormal);

                }
            }
            else
            {
                objectRef[0].Acceleration = new Vector3(0, 0, 0);
                objectRef[0].AngAcceleration = new Vector3(0, 0, 0);
            }
            
        }

        //------------------------------------------
    }
}
