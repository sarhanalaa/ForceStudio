﻿using System;
using System.Collections.Generic;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio_Lib
{
    public class CMotionLaw : IPhysicalLaw<I3dObject,I3dForce>
    {
        // Fields
        Object[] objectInfo;
        //---------------------

        // Constructor

        public CMotionLaw(I3dWorldManager worldManager)
        {
            this.objectInfo = new Object[1];
            this.objectInfo[0] = worldManager;
        }
        //---------------------

        #region IPhysicalLaw<I3dObject,I3dForce> Members

        public List<I3dForce> WorldForces
        {
            get
            {
                return null;
            }
            set
            {
                return;
            }
        }

        public object[] LawInfo
        {
            get
            {
                return this.objectInfo;
            }
            set
            {
                this.objectInfo = value;
            }
        }

        public void ApplyLaw(I3dObject[] objectRef)
        {
            if (objectInfo == null || objectInfo.Length < 1 || !(objectInfo[0] is I3dWorldManager))
                return;

            if (objectRef == null || objectRef.Length < 1)
                return;

  
            // Linear Motion
            if (!objectRef[0].Hanged)
            {
                objectRef[0].Velocity += objectRef[0].Acceleration * ((I3dWorldManager)this.objectInfo[0]).CycleDuration;//<-cycle duration
                objectRef[0].Position += objectRef[0].Velocity * ((I3dWorldManager)this.objectInfo[0]).CycleDuration;//<-cycle duration
            }
            //---------------------------
            // Circular Motion
            else
            {
                objectRef[0].AngVelocity += objectRef[0].AngAcceleration * ((I3dWorldManager)this.objectInfo[0]).CycleDuration;
                
                // Rotation Matrix
                Matrix rotationMatrix = Matrix.RotationMatrix(objectRef[0].AngVelocity * ((I3dWorldManager)this.objectInfo[0]).CycleDuration);

                // Transforming Object Coordinates
                objectRef[0].Position -= objectRef[0].HangPoint;
                for(int i = 0; i < objectRef[0].Vertices.Length; i++)
                    objectRef[0].Vertices[i].Position -= objectRef[0].HangPoint;
                

                // Rotating the Object
                objectRef[0].Transform(rotationMatrix);


                // Retransforming Object Coordinates
                objectRef[0].Position += objectRef[0].HangPoint;
                for (int i = 0; i < objectRef[0].Vertices.Length; i++)
                    objectRef[0].Vertices[i].Position += objectRef[0].HangPoint;
            }
        }

        #endregion
    }
}
