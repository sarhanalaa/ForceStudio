﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio_Lib
{
    public class CCollisionImpulseLaw : IPhysicalLaw<I3dObject,I3dForce>
    {

        // Fields
        float elasticity;
        Object[] info;
        //----------------------------------------

        
        // Properties

        public float Elasticity
        {
            get { return this.elasticity; }
            set 
            {
                if (value < 0)
                    this.elasticity = 0;
                else if (value > 1)
                    this.elasticity = 1;
                else
                    this.elasticity = value;
            }
        }

        /// <summary>
        /// Needs the Collision Point relative to center of world represented by a vector.
        /// </summary>
        public Object[] LawInfo
        {
            get { return this.info; }
            set { this.info = value; }
        }

        //----------------------------------------


        // Constructors
        public CCollisionImpulseLaw(float elastisity)
        {
            this.Elasticity = elasticity;
        }
        //----------------------------------------


        // Methods

        public void ApplyLaw(I3dObject[] objectRef)
        {

            try
            {
                if (objectRef == null || objectRef.Count() < 2 || info == null || info.Count() < 1)
                    throw new Exception("Impulse-Based Collision Law Needs 2 Objects with Collision Points as Additional Info. to function properly.");

                Vector3 n = objectRef[1].Position - objectRef[0].Position;
                n /= ~n;
                Vector3 r0 = ((Vector3)info[0]) - ((Vector3)objectRef[0].Position);
                Vector3 r0n = r0 * n;
                Vector3 r1 = ((Vector3)info[0]) - ((Vector3)objectRef[1].Position);
                Vector3 r1n = r1 * n;
                Matrix I0 = objectRef[0].GetInertiaTensor();
                Matrix I1 = objectRef[1].GetInertiaTensor();

                objectRef[0].UnHang();
                objectRef[1].UnHang();

                float j = (-1 - elasticity)*( ((objectRef[0].Velocity - objectRef[1].Velocity)^n)   +  (r0n^objectRef[0].SpinVelocity)  -  (r1n^objectRef[1].SpinVelocity));
                Matrix _j0 = (I0 * r0n.GetMatrixVer());
                float j0 = r0n ^ (new Vector3(_j0.Cell[0][0], _j0.Cell[1][0], _j0.Cell[2][0]));
                Matrix _j1 = (I1 * r1n.GetMatrixVer());
                float j1 = r1n ^ (new Vector3(_j1.Cell[0][0], _j1.Cell[1][0], _j1.Cell[2][0]));
                j /= (1 / objectRef[0].Mass) + (1 / objectRef[1].Mass) + j0 + j1;

                int sign0 = 1, sign1 = 1;
                if (~objectRef[0].Velocity >= ~objectRef[1].Velocity)
                    sign0 = -1;
                else
                    sign1 = -1;

                Vector3 J = j * n;
                objectRef[0].Velocity += sign0 * (J / objectRef[0].Mass);
                Matrix i0 = I0 * ((Vector3)(J * r0)).GetMatrixVer();
                Vector3 ii0 = new Vector3(i0.Cell[0][0],i0.Cell[1][0],i0.Cell[2][0]);
                objectRef[0].SpinVelocity += sign0 * ii0;

                objectRef[1].Velocity += sign1 * (J / objectRef[1].Mass);
                Matrix i1 = I1 * ((Vector3)(J * r1)).GetMatrixVer();
                Vector3 ii1 = new Vector3(i1.Cell[0][0], i1.Cell[1][0], i1.Cell[2][0]);
                objectRef[1].SpinVelocity += sign1 * ii1;

            }
            catch (Exception e)
            {
                return;
            }
        }

        //----------------------------------------
    }
}
