﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;

namespace ForceStudioV2
{
    class CThreadTentionForce : I3dForce
    {
        //*********Attributes**********
        protected I3dObject affObject;
        protected Vector3 magnitude;
        protected Vector3 offset;
        protected bool pullable;
        protected I3dWorld world;

        //*********Properties***********
        #region I3dForce Members

        public I3dObject AffObject
        {
            get { return this.affObject; }
            set { this.affObject = value; }
        }

        public Vector3 Magnitude
        {
            get { return this.magnitude; }
            set { this.magnitude = value; }
        }

        public Vector3 Offset
        {
            get { return this.offset; }
            set { this.offset = value; }
        }

        public bool Pullable
        {
            get { return this.pullable; }
        }

        public I3dWorld World
        {
            get { return this.world; }
        }

        #endregion
        //********************************

        //***************Constructor
        public CThreadTentionForce(CPendulumObject affobject)
        {
            Magnitude = new Vector3(0, 0, 0);
            Offset = new Vector3(0, 0, 0);
            pullable = false;
            this.affObject = affobject;
            this.world = this.AffObject.World;
        }
        
        //***************Methods***************s
        public  void Modify()
        {
            //check if the thread is has force 
            //heck if the length of the thread equal the distance between the point and the ball
            if (~(((CPendulumObject)this.AffObject).HangPoint + (-this.AffObject.Position)) == ((CPendulumObject)this.AffObject).ThreadLength)
            {
                Vector3 sum = new Vector3(0, 0, 0);
                for (int i = 0; i < this.affObject.Forces.Count; i++)
                    if (this.affObject.Forces[0] is CDragForce)
                        break;
                    else
                        sum += this.affObject.Forces[i].Magnitude;

                Vector3 thread = ((CPendulumObject)this.affObject).HangPoint - ((CPendulumObject)this.AffObject).Sphere.Position;
                this.magnitude = (~(sum * Vector3.GetCos(sum, thread)) / ~thread) * thread;
            }
            else
                this.magnitude = new Vector3(0, 0, 0);
            
        }

        public  void ResetOffset()
        {
            this.Offset = new Vector3(0, 0, 0);
        }

        public  void SetOffset(Vector3 offset)
        {
            this.Offset = offset;
        }

       
    }
}
