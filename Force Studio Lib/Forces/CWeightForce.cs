﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;
namespace Force_Studio_Lib
{
    class CWeightForce : I3dForce
    {

        // Fields
        Vector3 magnitude;
        Vector3 offset;
        I3dObject affObject;
        I3dWorld world;
        //--------------------------------------------


        // Properties

        public Vector3 Magnitude
        {
            get
            {
                return this.magnitude;
            }
            set
            {
                this.magnitude = value;
            }
        }

        public Vector3 Offset
        {
            get
            {
                return this.offset;
            }
            set
            {
                this.offset = value;
            }
        }

        public I3dObject AffObject
        {
            get { return this.affObject; }
            set
            {
                this.affObject = value;
                this.Modify();
            }
        }

        public bool Pullable
        {
            get { return false; }
        }

        public I3dWorld World
        {
            get { return this.world; }
        }

        //-----------------------------------------------


        // Constructors
        public CWeightForce(I3dWorld world)
        {
            if (this.world == null)
                throw new Exception("Can't Construct CWeightForce Instance with null World Reference.");

            this.world = world;
            this.affObject = null;
            this.magnitude = new Vector3();
            this.offset = new Vector3();
        }
        //-----------------------------------------------


        // Methods

        public void Modify()
        {
            if (this.affObject == null)
                throw new Exception("Can't Modify CWeightForce instance with null AffObject Reference.");

            this.magnitude = this.world.Gravity * this.affObject.Mass * new Vector3(0, -1, 0);

        }

        public void ResetOffset()
        {
            this.offset.X = 0;
            this.offset.Y = 0;
            this.offset.Z = 0;
        }

        public void SetOffset(Vector3 offset)
        {
                this.offset = offset;
        }

        //-----------------------------------------------

    }
}
