﻿using System;
using System.Collections.Generic;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio_Lib
{
    public class World3D : I3dWorld
    {

        // Fields
        float viscosity;
        float gravity;
        float temprature;
        Vector3 worldCenter;
        //---------------------------


        // Properties

        public float Viscosity
        {
            get
            {
                return this.viscosity;
            }
            set
            {
                this.viscosity = value;
            }
        }

        public Vector3 WorldCenter
        {
            get
            {
                return this.worldCenter;
            }
            set
            {
                this.worldCenter = value;
            }
        }

        public float Gravity
        {
            get
            {
                return this.gravity;
            }
            set
            {
                this.gravity = value;
            }
        }

        public float Temprature
        {
            get
            {
                return this.temprature;
            }
            set
            {
                this.temprature = value;
            }
        }

        //--------------------------------


        // Constructor
        public World3D()
        {
            this.viscosity = 1.204F;
            this.gravity = 9.8F;
            this.temprature = 20;
            this.worldCenter = new Vector3(0,0,0);
        }

        public World3D(float viscosity, float gravity, float temprature, Vector3 center)
        {
            this.viscosity = viscosity;
            this.gravity = gravity;
            this.temprature = temprature;
            this.worldCenter = center;
        }
        //--------------------------------


        // Methods

        //--------------------------------


    }
}
