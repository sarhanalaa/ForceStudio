﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio_Lib
{
    public class CDragForce : I3dForce
    {

        //*********Attributes**********
        protected I3dObject affObject;
        protected Vector3 magnitude;
        protected Vector3 offset;
        protected bool pullable;
        protected I3dWorld world;
        
        //*********Properties**********
        #region I3dForce Members

        public I3dObject AffObject
        {
            get { return this.affObject; }
            set 
            { 
                this.affObject = value;
            }
        }

        public Vector3 Magnitude
        {
            get { return this.magnitude; }
            set { this.magnitude = value; }  
        }

        public Vector3 Offset
        {
            get { return this.offset; }
            set { this.offset = value; }
        }

        public EActingLevel ActingLevel
        {
            get { return EActingLevel.WORLD; }
        }

        public I3dWorld World
        {
            get { return this.world; }
        }

        #endregion
        //***************************************************

        //Constractors
        public CDragForce(I3dWorld world)
        {
            this.Magnitude = new Vector3(0, 0, 0);
            this.Offset = new Vector3(0, 0, 0);
            this.pullable = true;
            this.affObject = null;
            this.world = world;
            
        }

        //******************Methods*************************

       


        //****overiden methods

        /// <summary>
        /// Modifies the Current Forces State.
        /// </summary>
        /// <param name="args">Arguments used to change force's state.</param>
        public  void Modify()
        {
            if (~affObject.Velocity == 0)
            {
                this.magnitude = new Vector3(0, 0, 0);
                return;
            }
            float V = ~(this.AffObject.Velocity);
            float temp = ((this.World.Viscosity *
                    this.AffObject.GetDragCoefficient(this.AffObject.Velocity) *
                    V * V
                    * this.AffObject.GetFaceArea(this.AffObject.Velocity)) / 2) / ~(this.AffObject.Velocity);
           
            Vector3 vector = -this.AffObject.Velocity;
            vector = temp * vector;
            this.Magnitude = vector;

          
        }


        /// <summary>
        /// Resets the offset to 0. Put the Point-of-Act to the center of mass of affected Objects.
        /// </summary>
        public  void ResetOffset()
        {
            this.Offset = new Vector3(0, 0, 0);             
        }
      

        /// <summary>
        /// Sets the offset of the Point-of-Act to the given differentiation from the center of mass.
        /// </summary>
        /// <param name="offset">The Differentioation from center of mass.</param>
        public  void SetOffset(Vector3 offset)
        {
            this.Offset = offset;
           
        }

        public override string ToString()
        {
            return "Drag Force";
        }
    }
}
