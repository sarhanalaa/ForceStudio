﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio_Lib
{
    public class CThreadTentionForce : I3dForce
    {
        //*********Attributes**********
        public static float round = 0.0005F;
        protected I3dObject affObject;
        protected Vector3 magnitude;
        protected Vector3 offset;
        protected bool pullable;
        protected I3dWorld world;

        //*********Properties***********
        #region I3dForce Members

        public I3dObject AffObject
        {
            get { return this.affObject; }
            set { this.affObject = value; }
        }

        public Vector3 Magnitude
        {
            get { return this.magnitude; }
            set { this.magnitude = value; }
        }

        public Vector3 Offset
        {
            get { return this.offset; }
            set { this.offset = value; }
        }

        public EActingLevel ActingLevel
        {
            get { return EActingLevel.OBJECT; }
        }

        public I3dWorld World
        {
            get { return this.world; }
        }

        #endregion
        //********************************

        //***************Constructor
        public CThreadTentionForce(CPendulumObject affobject)
        {
            Magnitude = new Vector3(0, 0, 0);
            Offset = new Vector3(0, 0, 0);
            pullable = false;
            this.affObject = affobject;
            this.world = this.AffObject.World;
        }
        
        
        //***************Methods***************
        
        public void Modify()
        {

            bool unHang = false;
            Vector3 netForce = new Vector3(0, 0, 0);
            Vector3 Distance = affObject.HangPoint - affObject.Position;

            if (~Distance + round < ((CPendulumObject)affObject).ThreadLength)
                unHang = true;
            else
            {
                unHang = false;

                // Repositining
                if (~Distance != ((CPendulumObject)affObject).ThreadLength)
                {
                    this.affObject.Position = ((-Distance / ~Distance) * (((CPendulumObject)affObject).ThreadLength)) + affObject.HangPoint;
                    Distance = affObject.HangPoint - affObject.Position;
                }

                // Caculating Net Force
                foreach (I3dForce force in affObject.Forces)
                {
                    if (force is CThreadTentionForce)
                        continue;
                    if(force.ActingLevel == EActingLevel.WORLD || force.AffObject == affObject)
                        netForce += force.Magnitude;
                }

                // Checking Existence of Thread Tension
                if (~netForce == 0 || ~affObject.Velocity < 0.05)
                    unHang = true;
                else if ( Vector3.GetCos(affObject.Velocity, Distance) > 0)
                        unHang = true;
            }

            if (unHang)
            {
                if(affObject.Hanged)
                    affObject.UnHang();
                this.magnitude = new Vector3(0, 0, 0);
                return;
            }
            else if (!unHang && !affObject.Hanged)
                affObject.Hang(affObject.HangPoint);

            this.magnitude = ~(netForce * Vector3.GetCos(netForce, Distance)) * (Distance / ~Distance);

        }


        public  void ResetOffset()
        {
            this.Offset = new Vector3(0, 0, 0);
        }


        public  void SetOffset(Vector3 offset)
        {
            this.Offset = offset;
        }

        public override string ToString()
        {
            return "Thread Tension {" + AffObject.ToString() + "}";
        }

    }
}