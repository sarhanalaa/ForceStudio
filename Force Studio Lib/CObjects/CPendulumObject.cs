﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;


namespace Force_Studio_Lib 
{
    public class CPendulumObject : CSphereObject
    {
        
        float threadLength;
        //--------------------------------------------

        //*************Constructor*******************
        public CPendulumObject(string name, float radius, float mass, Vector3 position, Vector3 velocity, Vector3 acceleration,
                      Vector3 angVelocity, Vector3 angAcceleration, Vector3 hangPoint, List<I3dForce> forces, I3dWorld world)
            : base(name, radius, mass, position, velocity, acceleration, angVelocity, angAcceleration, forces, world)
        {

            this.Hang(hangPoint);
            this.threadLength = ~(this.Position - hangPoint);
            this.Forces.Add(new CThreadTentionForce(this));
        }

        public CPendulumObject(string name, float radius, float mass, Vector3 position, Vector3 velocity, Vector3 acceleration,
                      Vector3 angVelocity, Vector3 angAcceleration, Vector3 hangPoint, List<I3dForce> forces, I3dWorld world, float threadLength)
            : this(name, radius, mass, position, velocity, acceleration, angVelocity, angAcceleration, hangPoint, forces, world)
        {
            if (threadLength > 0 && threadLength >= ~(this.Position - hangPoint))
                this.threadLength = threadLength;
        }

        //--------------------------------


        // Properties

        public float ThreadLength
        {
            get { return this.threadLength; }
        }

        //--------------------------------

        
        // Methods

        public override Dictionary<string, object> GenerateCurrentState()
        {
            Dictionary<string, object> state = base.GenerateCurrentState();
            if(!this.Hanged)
                state.Add("Hang Point", this.HangPoint);
            state.Add("Distance", ~(this.Position - this.HangPoint));

            return state;
        }

        public override string QueryState(long cycle)
        {
            String stateString;

            if (!this.stateHistory.ContainsKey(cycle))
                stateString = this.Name + " does not contain a history state for the cycle " + cycle.ToString();
            else
            {
                Dictionary<String, Object> state = this.stateHistory[cycle];
                stateString = "";

                stateString += "Name:\t" + this.Name + Environment.NewLine + Environment.NewLine;
                stateString += "Mass:\t" + state["Mass"] + " kg" + Environment.NewLine;
                stateString += "Position:\t" + state["Position"] + Environment.NewLine;

                stateString += "Velocity:\t" + "|" + state["Velocity"] + "| = "
                                + (~(Vector3)state["Velocity"]).ToString()
                                + " m/s" + Environment.NewLine;

                stateString += "Acceleration:\t" + "|" + state["Acceleration"]
                                + "| = "
                                + (~(Vector3)state["Acceleration"]).ToString()
                                + " m/s2" + Environment.NewLine;

                stateString += "Angular Velocity:\t" + "|"
                                + state["Angular Velocity"] + "| = "
                                + (~(Vector3)state["Angular Velocity"]).ToString()
                                + " rad/s" + Environment.NewLine;

                stateString += "Angular Acceleration:\t" + "|"
                                + state["Angular Acceleration"] + "| = "
                                + (~(Vector3)state["Angular Acceleration"]).ToString()
                                + " rad/s2" + Environment.NewLine;

                stateString += "Hang Point:\t:" + state["Hang Point"].ToString()
                                + Environment.NewLine;

                stateString += "Distance:\t" + state["Distance"].ToString()
                                + " m" + Environment.NewLine;

            }

            return stateString;
        }

        public override Primitive3[] Render()
        {
            Primitive3[] sphere = base.Render();

            Primitive3 thread = new Primitive3(this.ThreadLength.ToString(), EPrimitiveType.DEFAULT);
            thread.VertexOrder = EVertexOrder.LINESTRIP;
            Vertex3 v = new Vertex3();
            v.Position = this.HangPoint;
            thread.Vertices = new List<Vertex3>();
            thread.Vertices.Add(v);

            if ((~(this.Position - this.HangPoint) + CThreadTentionForce.round) < this.ThreadLength)
            {
                v.Position = this.HangPoint + (1 / 2F) * (this.Position + this.HangPoint) - this.Velocity;
                thread.Vertices.Add(v);
            }
            v.Position = this.Position;
            thread.Vertices.Add(v);

            Primitive3[] pendulum = new Primitive3[sphere.Length + 1];
            sphere.CopyTo(pendulum, 0);
            pendulum[pendulum.Length - 1] = thread;

            return pendulum;
        }

        public override void Hang(Vector3 hangPoint)
        {
            base.Hang(hangPoint);
            if (this.ThreadLength <= 0)
                this.threadLength = ~(this.HangPoint - this.Position);
        }
        //--------------------------------

    }
}
