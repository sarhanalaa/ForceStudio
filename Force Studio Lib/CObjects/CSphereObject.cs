﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Force_Studio_Lib;
using ForceLib;
using ForceLib.ForceLib3D;
using System.Windows.Forms;

namespace Force_Studio_Lib
{
    public class CSphereObject : I3dObject , ICollidable, IAnalyzable, IRenderable, IEditable
    {
        
        protected string name;
        protected float radius;
        protected float mass;
        protected Vector3 position;
        protected Vector3 velocity;
        protected Vector3 angVelocity;
        protected Vector3 angAcceleration;
        protected Vector3 acceleration;
        protected Vector3 spinVelocity;
        protected Vector3 hangPoint;
        protected bool hanged;
        protected List<I3dForce> forces;
        protected const float DC = 0.1F;
        protected Vertex3 vertix;
        protected I3dWorld world;
        protected float inertia;
        protected Matrix inertiaTensor;
        protected Dictionary<long, Dictionary<string, object>> stateHistory;
        protected EditingPanel editPanel;
        protected System.Drawing.Color selfColor;
        //--------------------------


        public CSphereObject(string name, float radius, float mass, Vector3 position, Vector3 velocity, Vector3 acceleration,
                      Vector3 angVelocity, Vector3 angAcceleration, List<I3dForce> forces , I3dWorld world)
        {
            this.name = name;
            this.radius = radius;
            this.mass = mass;
            this.position = position;
            this.velocity = velocity;
            this.acceleration = acceleration;
            this.angVelocity = angVelocity;
            this.angAcceleration = angAcceleration;
            if (forces == null)
                this.Forces = new List<I3dForce>();
            else
                this.Forces = forces;
            vertix = new Vertex3();
            vertix.Position = new Vector3(position.X + radius, 0, 0);
            this.inertia = (2F / 5F) * this.Mass * (float)Math.Pow(this.Radius, 2);
            
            float iTVal = 25F / (4 * (float)Math.Pow(this.Mass,2) * (float)Math.Pow(this.Radius,4));

            this.inertiaTensor = new Matrix(3, 3);
            inertiaTensor.Cell[0][0] = iTVal;
            inertiaTensor.Cell[1][1] = iTVal;
            inertiaTensor.Cell[2][2] = iTVal;

            this.world = world;

            Random colorRandomizer = new Random((int)DateTime.Now.Millisecond);

            selfColor = System.Drawing.Color.FromArgb(colorRandomizer.Next(0, 150), colorRandomizer.Next(0, 100), colorRandomizer.Next(20, 127));

            System.Threading.Thread.Sleep(63);

            this.editPanel = new EditingPanel(this);
            this.stateHistory = new Dictionary<long, Dictionary<string, object>>();
        }


        public CSphereObject(string name, float radius, float mass, Vector3 position, Vector3 velocity, Vector3 acceleration,
                      Vector3 angVelocity, Vector3 angAcceleration, Vector3 hangPoint, List<I3dForce> forces, I3dWorld world)
            : this(name, radius, mass, position, velocity, acceleration, angVelocity, angAcceleration, forces, world)
        {
            this.Hang(hangPoint);
        }

        public float Radius
        {
            get { return this.radius; }
            set
            {
                if (value > 0)
                {
                    this.radius = value;
                    this.inertia = (2F / 5F) * this.Mass * (float)Math.Pow(this.Radius, 2);

                    float iTVal = 25F / (4 * (float)Math.Pow(this.Mass, 2) * (float)Math.Pow(this.Radius, 4));

                    this.inertiaTensor = new Matrix(3, 3);
                    inertiaTensor.Cell[0][0] = iTVal;
                    inertiaTensor.Cell[1][1] = iTVal;
                    inertiaTensor.Cell[2][2] = iTVal;
                }
            }
        }

        public void Transform(Matrix trans)
        {
            position = Vector3.Transform(position, trans);
            vertix.Position = Vector3.Transform(vertix.Position, trans);
            /*
            velocity = Vector3.Transform(velocity, trans);
            acceleration = Vector3.Transform(acceleration, trans);
             */
        }

        

        #region I3dObject Members

        public Vector3 Acceleration
        {
            get { return acceleration; }
            set { this.acceleration = value; } 
        }

        public Vector3 AngAcceleration
        {
            get { return angAcceleration; }
            set
            {
                this.angAcceleration = value;

                Vector3 axis = this.HangPoint - this.Position;
                if (!float.IsNaN(Vector3.GetSin(this.angAcceleration, axis)))
                {
                    Vector3 accNormal = Vector3.LeftHandRotaionNormal(axis, this.angAcceleration);
                    this.acceleration = ~(this.angAcceleration * Vector3.GetSin(this.angAcceleration, axis)) * (accNormal);
                }
                else
                    this.acceleration = new Vector3(0, 0, 0);
            }
        }

        public Vector3 AngVelocity
        {
            get { return angVelocity; }
            set
            {
                this.angVelocity = value;

                Vector3 axis = this.HangPoint - this.Position;
                if (!float.IsNaN(Vector3.GetSin(this.angVelocity, axis)))
                {
                    Vector3 velNormal = Vector3.LeftHandRotaionNormal(axis, this.angVelocity);
                    this.velocity = ~(this.angVelocity * Vector3.GetSin(this.angVelocity, axis)) * (velNormal);
                }
                else
                    this.velocity = new Vector3(0, 0, 0);
            }
        }

        public bool Hanged
        {
            get { return this.hanged; }
        }

        public Vector3 HangPoint
        {
            get { return this.hangPoint; }
        }

        public List<I3dForce> Forces
        {
            get { return forces;       }
            set { this.forces = value; }
        }

        public Vertex3[] Vertices
        {
            get { return new Vertex3[] { this.vertix }; }
            set
            {
                if (value != null && value.Count() > 0)
                    this.vertix = value[0];
            }
        }

        public float GetFaceArea(Vector3 velocity)
        {
            return (radius * radius * (float)Math.PI);
        }

        public float GetDragCoefficient(Vector3 velocity)
        {
            return DC;
        }

        public float Mass
        {
            get { return mass; }
            set
            {
                if (value > 0)
                {
                    this.mass = value;
                    this.inertia = (2F / 5F) * this.Mass * (float)Math.Pow(this.Radius, 2);

                    float iTVal = 25F / (4 * (float)Math.Pow(this.Mass, 2) * (float)Math.Pow(this.Radius, 4));

                    this.inertiaTensor = new Matrix(3, 3);
                    inertiaTensor.Cell[0][0] = iTVal;
                    inertiaTensor.Cell[1][1] = iTVal;
                    inertiaTensor.Cell[2][2] = iTVal;
                }
            }
        }

        public Vector3 Position
        {
            get { return position; }
            set { this.position = value; } 
        }

        public Vector3 Velocity
        {
            get { return velocity; }
            set { this.velocity = value; } 
        }

        public Vector3 SpinVelocity
        {
            get { return spinVelocity; }
            set { this.spinVelocity = value; }
        }

        public I3dWorld World
        {
            get { return world; }
        }

        public float GetInertia()
        {
            return this.inertia;
        }

        public Matrix GetInertiaTensor()
        {
            return this.inertiaTensor;
        }

        public virtual void Hang(Vector3 hangPoint)
        {

            if (this.Hanged && hangPoint == this.HangPoint)
                return;
            else if (this.Hanged)
                UnHang();

            this.hangPoint = hangPoint;
            this.hanged = true;

            // Generating AngularVelocity and AngularAcceleration
            Vector3 axis = this.HangPoint - this.Position;


            if (!float.IsNaN(Vector3.GetSin(velocity, axis)))
            {
                Vector3 angVelNormal = Vector3.LeftHandRotaionNormal(this.velocity, axis);
                this.angVelocity = ~(velocity * Vector3.GetSin(velocity, axis)) * (angVelNormal);
            }
            else
                this.angVelocity = new Vector3(0, 0, 0);

            if (!float.IsNaN(Vector3.GetSin(acceleration, axis)))
            {
                Vector3 angAccNormal = Vector3.LeftHandRotaionNormal(this.acceleration, axis);
                this.angAcceleration = ~(acceleration * Vector3.GetSin(acceleration, axis)) * (angAccNormal);
            }
            else
                this.angAcceleration = new Vector3(0, 0, 0);
        }


        public virtual void UnHang()
        {
            if(!Hanged)
                return;

            // Generating Velocity and Acceleration
            Vector3 axis = this.HangPoint - this.Position;

            if (!float.IsNaN(Vector3.GetSin(this.angVelocity, axis)))
            {
                Vector3 velNormal = Vector3.LeftHandRotaionNormal(axis, this.angVelocity);
                this.velocity = ~(this.angVelocity * Vector3.GetSin(this.angVelocity, axis)) * (velNormal);
            }
            else
                this.velocity = new Vector3(0, 0, 0);

            if (!float.IsNaN(Vector3.GetSin(this.angAcceleration, axis)))
            {
                Vector3 accNormal = Vector3.LeftHandRotaionNormal(axis, this.angAcceleration);
                this.acceleration = ~(this.angAcceleration * Vector3.GetSin(this.angAcceleration, axis)) * (accNormal);
            }
            else
                this.acceleration = new Vector3(0, 0, 0);

            this.angVelocity = new Vector3(0, 0, 0);
            this.angAcceleration = new Vector3(0, 0, 0);


            this.hanged = false;

        }

        #endregion

        #region IAnalyzable Members

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public Dictionary<long, Dictionary<string, object>> StateHistory
        {
            get { return this.stateHistory; }
        }

        public virtual String[] HistogramElements
        {
            get { return new String[] { "Position", "Velocity", "Acceleration", "Circular Velocity", "Circular Acceleration" }; }
        }

        public virtual Dictionary<string, object> GenerateCurrentState()
        {
            Dictionary<string, object> state = new Dictionary<string, object>();
            state.Add("Mass", this.Mass);
            state.Add("Position", this.Position);
            state.Add("Velocity", this.Velocity);
            state.Add("Acceleration", this.Acceleration);
            state.Add("Angular Velocity", this.AngVelocity);
            state.Add("Angular Acceleration", this.AngAcceleration);
            state.Add("Hanged", this.Hanged);
            if (this.Hanged)
                state.Add("Hang Point", this.HangPoint);

            return state;
        }

        public void AddState(long cycle, Dictionary<string, object> state)
        {
            if (this.stateHistory.ContainsKey(cycle))
                this.stateHistory.Remove(cycle);
            this.stateHistory.Add(cycle, state);
        }

        public void SetState(long cycle)
        {
            // Checking for Cycle History Existance
            if (!this.StateHistory.ContainsKey(cycle))
                return;
        
            // Setting Object State
            Dictionary<String, Object> state = this.StateHistory[cycle];
            this.Mass = (float)state["Mass"];
            this.Position = (Vector3)state["Position"];
            this.Velocity = (Vector3)state["Velocity"];
            this.Acceleration = (Vector3)state["Acceleration"];
            this.AngVelocity = (Vector3)state["Angular Velocity"];
            this.AngAcceleration = (Vector3)state["Angular Acceleration"];
            if ((bool)state["Hanged"] && state.ContainsKey("Hang Point"))
            {
                this.hanged = true;
                this.hangPoint = (Vector3)state["Hang Point"];
            }
            else
                this.hanged = false;
        }

        public virtual string QueryState(long cycle)
        {
            String stateString;
            
            if (!this.stateHistory.ContainsKey(cycle))
                stateString = this.Name + " does not contain a history state for the cycle " + cycle.ToString();
            else
            {
                Dictionary<String, Object> state = this.stateHistory[cycle];
                stateString = "";

                stateString += "Name:\t" + this.Name + Environment.NewLine + Environment.NewLine;
                stateString += "Mass:\t" + state["Mass"] + " kg" + Environment.NewLine;
                stateString += "Position:\t" + state["Position"] + Environment.NewLine;

                stateString += "Velocity:\t" + "|" + state["Velocity"] + "| = " 
                                + (~(Vector3)state["Velocity"]).ToString() 
                                + " m/s" + Environment.NewLine;

                stateString += "Acceleration:\t" + "|" + state["Acceleration"]
                                + "| = " 
                                + (~(Vector3)state["Acceleration"]).ToString()
                                + " m/s2" + Environment.NewLine;

                stateString += "Angular Velocity:\t" + "|" 
                                + state["Angular Velocity"] + "| = " 
                                + (~(Vector3)state["Angular Velocity"]).ToString()
                                + " rad/s" + Environment.NewLine;

                stateString += "Angular Acceleration:\t" + "|"
                                + state["Angular Acceleration"] + "| = "
                                + (~(Vector3)state["Angular Acceleration"]).ToString()
                                + " rad/s2" + Environment.NewLine;

                if (state.ContainsKey("Hang Point"))
                    stateString += "Hang Point:\t" + state["Hang Point"].ToString()
                                    + Environment.NewLine;
            }

            return stateString;
        }

        #endregion


        public override string ToString()
        {
            return this.Name;
        }


        #region IRenderable Members

        public virtual Primitive3[] Render()
        {
            Primitive3 p = new Primitive3(this.Name, EPrimitiveType.SPHERE);
            p.PrimitiveInfo = new object[] { (object)this.Radius };
            
            Vertex3 v = new Vertex3();
            v.Position = this.Position;
            v.SpecularColor = selfColor;
            v.DiffuseColor = selfColor;
            
            p.Vertices = new List<Vertex3>();
            p.Vertices.Add(v);
            p.Vertices.Add(this.vertix);
            return new Primitive3[] { p };
        }

        #endregion


        #region IEditable Members

        public object[] EditingInfo
        {
            get { return new object[] { (object)editPanel.cycle, (object)this.editPanel.Seek }; }
            set
            {
                if (value.Length != 2 || !(value[1] is SeekDelegate))
                    return;
                try { this.editPanel.cycle = Convert.ToInt64(value[0]); }
                catch { this.editPanel.cycle = 0; }
                this.editPanel.Seek = (SeekDelegate)value[1];

            }
        }

        public System.Windows.Forms.Panel GenerateEditingPanel(object[] info)
        {
            return this.editPanel;
        }

        // ####### Editing Panel Class Definition ########
        public class EditingPanel : Panel
        {
           

            // Fields
            public CSphereObject objRef;
            public SeekDelegate Seek;
            public long cycle;

            // Labels
            Label lblName;
            Label lblMass;
            Label lblRadius;
            Label lblX;
            Label lblY;
            Label lblZ;
            Label lblPosition;
            Label lblVelocity;
            Label lblSpingVelocity;
            Label lblHanged;
            Label lblHangPoint;

            // Text Boxes
            TextBox txtName;
            TextBox txtMass;
            TextBox txtRadius;
            TextBox txtPosX;
            TextBox txtPosY;
            TextBox txtPosZ;
            TextBox txtVelX;
            TextBox txtVelY;
            TextBox txtVelZ;
            TextBox txtSpinVelX;
            TextBox txtSpinVelY;
            TextBox txtSpinVelZ;
            TextBox txtHangX;
            TextBox txtHangY;
            TextBox txtHangZ;

            // Check Boxes
            CheckBox chkHanged;

            // Buttons
            Button btnSave;
            Button btnCancel;

            //----------------------------------

            // Constructors
            public EditingPanel(CSphereObject objRef)
            {
                this.objRef = objRef;
                InitializeComponents();
                this.LocationChanged +=new EventHandler(this.LoadObjectState);
                this.DoubleClick += new EventHandler(this.LoadObjectState);
            }
            //----------------------------------

            // Methods
            private void InitializeComponents()
            {

                // Initializing Panel
                this.Location = new System.Drawing.Point(0, 0);
                this.Size = new System.Drawing.Size(350, 300);
                // Creating Instances

                // Labels
                lblName = new Label();
                lblName.Text = "Name:";
                lblName.AutoSize = true;
                lblName.Location = new System.Drawing.Point(5, 13);
                this.Controls.Add(lblName);
                
                lblMass = new Label();
                lblMass.Text = "Mass:";
                lblMass.AutoSize = true;
                lblMass.Location = new System.Drawing.Point(5, 43);
                this.Controls.Add(lblMass);

                lblRadius = new Label();
                lblRadius.Text = "Radius:";
                lblRadius.AutoSize = true;
                lblRadius.Location = new System.Drawing.Point(5, 73);
                this.Controls.Add(lblRadius);

                lblX = new Label();
                lblX.Text = "X";
                lblX.AutoSize = true;
                lblX.Location = new System.Drawing.Point(185, 10);
                this.Controls.Add(lblX);

                lblY = new Label();
                lblY.Text = "Y";
                lblY.AutoSize = true;
                lblY.Location = new System.Drawing.Point(240, 10);
                this.Controls.Add(lblY);

                lblZ = new Label();
                lblZ.Text = "Z";
                lblZ.AutoSize = true;
                lblZ.Location = new System.Drawing.Point(295, 10);
                this.Controls.Add(lblZ);

                lblPosition = new Label();
                lblPosition.Text = "Position:";
                lblPosition.AutoSize = true;
                lblPosition.Location = new System.Drawing.Point(125, 40);
                this.Controls.Add(lblPosition);

                lblVelocity = new Label();
                lblVelocity.Text = "Velocity:";
                lblVelocity.AutoSize = true;
                lblVelocity.Location = new System.Drawing.Point(125, 70);
                this.Controls.Add(lblVelocity);

                lblSpingVelocity = new Label();
                lblSpingVelocity.Text = "Spin Vel.:";
                lblSpingVelocity.AutoSize = true;
                lblSpingVelocity.Location = new System.Drawing.Point(125, 130);
                this.Controls.Add(lblSpingVelocity);

                lblHanged = new Label();
                lblHanged.Text = "Hanged";
                lblHanged.AutoSize = true;
                lblHanged.Location = new System.Drawing.Point(125, 163);
                this.Controls.Add(lblHanged);

                lblHangPoint = new Label();
                lblHangPoint.Text = "Hang Point:";
                lblHangPoint.AutoSize = true;
                lblHangPoint.Location = new System.Drawing.Point(120, 190);
                this.Controls.Add(lblHangPoint);


                // Text Boxes
                txtName = new TextBox();
                txtName.Location = new System.Drawing.Point(60, 10);
                txtName.Size = new System.Drawing.Size(80, 22);
                this.Controls.Add(txtName);

                txtMass = new TextBox();
                txtMass.Location = new System.Drawing.Point(60, 40);
                txtMass.Size = new System.Drawing.Size(60, 22);
                this.Controls.Add(txtMass);

                txtRadius = new TextBox();
                txtRadius.Location = new System.Drawing.Point(60, 70);
                txtRadius.Size = new System.Drawing.Size(60, 22);
                this.Controls.Add(txtRadius);

                txtPosX = new TextBox();
                txtPosX.Location = new System.Drawing.Point(185, 40);
                txtPosX.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtPosX);

                txtPosY = new TextBox();
                txtPosY.Location = new System.Drawing.Point(240, 40);
                txtPosY.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtPosY);

                txtPosZ = new TextBox();
                txtPosZ.Location = new System.Drawing.Point(295, 40);
                txtPosZ.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtPosZ);

                txtVelX = new TextBox();
                txtVelX.Location = new System.Drawing.Point(185, 70);
                txtVelX.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtVelX);

                txtVelY = new TextBox();
                txtVelY.Location = new System.Drawing.Point(240, 70);
                txtVelY.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtVelY);

                txtVelZ = new TextBox();
                txtVelZ.Location = new System.Drawing.Point(295, 70);
                txtVelZ.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtVelZ);

                txtSpinVelX = new TextBox();
                txtSpinVelX.Location = new System.Drawing.Point(185, 130);
                txtSpinVelX.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtSpinVelX);

                txtSpinVelY = new TextBox();
                txtSpinVelY.Location = new System.Drawing.Point(240, 130);
                txtSpinVelY.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtSpinVelY);

                txtSpinVelZ = new TextBox();
                txtSpinVelZ.Location = new System.Drawing.Point(295, 130);
                txtSpinVelZ.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtSpinVelZ);

                txtHangX = new TextBox();
                txtHangX.Location = new System.Drawing.Point(185, 190);
                txtHangX.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtHangX);

                txtHangY = new TextBox();
                txtHangY.Location = new System.Drawing.Point(240, 190);
                txtHangY.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtHangY);

                txtHangZ = new TextBox();
                txtHangZ.Location = new System.Drawing.Point(295, 190);
                txtHangZ.Size = new System.Drawing.Size(40, 22);
                this.Controls.Add(txtHangZ);

                // Check Boxes
                chkHanged = new CheckBox();
                chkHanged.Location = new System.Drawing.Point(110, 160);
                this.Controls.Add(chkHanged);

                // Buttons
                btnSave = new Button();
                btnSave.Location = new System.Drawing.Point(10, 230);
                btnSave.Size = new System.Drawing.Size(60, 22);
                btnSave.Text = "Save";
                btnSave.Click += new EventHandler(Save);
                this.Controls.Add(btnSave);

                btnCancel = new Button();
                btnCancel.Location = new System.Drawing.Point(90, 230);
                btnCancel.Size = new System.Drawing.Size(60, 22);
                btnCancel.Text = "Cancel";
                btnCancel.Click += new EventHandler(Cancel);
                this.Controls.Add(btnCancel);

            }

            private void Save(object sender, EventArgs e)
            {

                Seek(cycle);
                bool done = true;

                objRef.Name = txtName.Text;
                try { objRef.Mass = Convert.ToSingle(txtMass.Text); }
                catch { done = false; }
                try { objRef.Radius = Convert.ToSingle(txtRadius.Text); }
                catch { done = false; }
                try
                {
                    objRef.Position = new Vector3(Convert.ToSingle(txtPosX.Text),
                                               Convert.ToSingle(txtPosY.Text),
                                               Convert.ToSingle(txtPosZ.Text));
                }
                catch { done = false; }
                try
                {
                    objRef.Velocity = new Vector3(Convert.ToSingle(txtVelX.Text),
                                               Convert.ToSingle(txtVelY.Text),
                                               Convert.ToSingle(txtVelZ.Text));
                }
                catch { done = false; }

                if (chkHanged.Checked)
                {
                    try
                    {
                        objRef.Hang(new Vector3(Convert.ToSingle(txtHangX.Text),
                                                   Convert.ToSingle(txtHangY.Text),
                                                   Convert.ToSingle(txtHangZ.Text)));
                    }
                    catch { done = false; }
                }
                else if (!chkHanged.Checked)
                    objRef.UnHang();

                try
                {
                    objRef.SpinVelocity = new Vector3(Convert.ToSingle(txtSpinVelX.Text),
                                               Convert.ToSingle(txtSpinVelY.Text),
                                               Convert.ToSingle(txtSpinVelZ.Text));
                }
                catch { done = false; }

                if (!done)
                {
                    if (MessageBox.Show("Some Inputs were invalid and they will not be saved." + Environment.NewLine
                        + "Do you want to Continue anyway?", "Saving Changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Form frm = this.FindForm();
                        frm.Controls.Clear();
                        frm.Close();
                    }
                }
                else
                {
                    Form frm = this.FindForm();
                    frm.Controls.Clear();
                    frm.Close();
                }
                    
            }

            private void Cancel(object sender, EventArgs e)
            {
                Form frm = this.FindForm();
                frm.Controls.Clear();
                frm.Close();
            }

            private void LoadObjectState(object sender, System.EventArgs e)
            {
                if (this.objRef == null)
                    return;

                txtName.Text = objRef.Name;
                txtMass.Text = objRef.Mass.ToString();
                txtRadius.Text = objRef.Radius.ToString();

                txtPosX.Text = objRef.Position.X.ToString();
                txtPosY.Text = objRef.Position.Y.ToString();
                txtPosZ.Text = objRef.Position.Z.ToString();

                txtVelX.Text = objRef.Velocity.X.ToString();
                txtVelY.Text = objRef.Velocity.Y.ToString();
                txtVelZ.Text = objRef.Velocity.Z.ToString();

                txtSpinVelX.Text = objRef.SpinVelocity.X.ToString();
                txtSpinVelY.Text = objRef.SpinVelocity.Y.ToString();
                txtSpinVelZ.Text = objRef.SpinVelocity.Z.ToString();

                if (objRef.Hanged)
                {
                    chkHanged.Checked = true;

                    txtHangX.Text = objRef.HangPoint.X.ToString();
                    txtHangY.Text = objRef.HangPoint.Y.ToString();
                    txtHangZ.Text = objRef.HangPoint.Z.ToString();
                }
                else
                    chkHanged.Checked = false;
            }
            //----------------------------------

        }
        //#####################################

        #endregion

        #region ICollidable Members

        public Vector3 colV
        {
            get { return this.Velocity; }
            set { this.Velocity = value; }
        }

        public Vector3 colW
        {
            get { return this.SpinVelocity; }
            set { this.SpinVelocity = value; }
        }

        public Vector3 colP
        {
            get { return this.Position; }
            set
            {
                this.Position = value;
            }
        }

        public float colR
        {
            get { return this.Radius; }
        }

        public float colM
        {
            get { return this.Mass; }
        }

        public Matrix colIinverse
        {
            get { return this.inertiaTensor; }
        }

        #endregion
    }
}