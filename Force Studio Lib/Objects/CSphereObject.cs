﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;

namespace ForceStudioV2
{
    class CSphereObject : I3dObject , ICollidable
    {
       
        private float radius;
        float mass;
        Vector3 position;
        Vector3 velocity;
        Vector3 angVelocity;
        Vector3 angAcceleration;
        Vector3 acceleration;
        List<I3dForce> forces;
        const float PI = 3.141592F;
        const float DC = 0.1F;
        private Vertex3 vertice;
        I3dWorld world;
        //--------------------------


        public CSphereObject(float radius, float mass, Vector3 position, Vector3 velocity, Vector3 acceleration,
                      Vector3 angVelocity, Vector3 angAcceleration, List<I3dForce> forces , I3dWorld world)
        {
            this.radius = radius;
            this.mass = mass;
            this.position = position;
            this.velocity = velocity;
            this.acceleration = acceleration;
            this.angVelocity = angVelocity;
            this.angAcceleration = angAcceleration;
            if (forces == null)
                this.Forces = new List<I3dForce>();
            else
                this.Forces = forces;
            vertice = new Vertex3();
            vertice.Position = new Vector3(position.X + radius, 0, 0);
            this.world = world;
        }

        public float Radius
        {
            get { return this.radius; }
            set { this.radius = value; }
        }

        public Vector3 V
        {
            get { return this.velocity; }
        }

        public Vector3 P
        {
            get { return this.position; }
        }

        public float R
        {
            get { return this.radius; }
        }

        public void Transform(Matrix trans)
        {
            position.Transform(trans);
            vertice.Position.Transform(trans);
        }
        public float GetFaceArea(Vector3 velocity)
        {
            return (radius * radius * PI);
        }

        public float GetDragCoefficient(Vector3 velocity)
        {
            return DC;
        }

        #region I3dObject Members

        public Vector3 Acceleration
        {
            get { return acceleration; }
            set { this.acceleration = value; } 
        }

        public Vector3 AngAcceleration
        {
            get  { return angAcceleration ;        }
            set  {  this.angAcceleration = value ; } 
        }

        public Vector3 AngVelocity
        {
            get { return angVelocity; }
            set { this.angVelocity = value; }
        }

        public List<I3dForce> Forces
        {
            get { return forces;       }
            set { this.forces = value; }
        }

        public Vertex3 Vertice
        {
            get { return vertice; }
            set { this.vertice = value; }
        }

        public float Mass
        {
            get { return mass; }
            set
            {
                if (value > 0)
                    this.mass = value;
            }
        }

        public Vector3 Position
        {
            get { return position; }
            set { this.position = value; } 
        }

        public Vector3 Velocity
        {
            get { return velocity; }
            set { this.velocity = value; } 
        }

        public I3dWorld World
        {
            get { return world; }
        }

        #endregion
    }
}
