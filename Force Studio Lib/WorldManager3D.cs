﻿using System;
using System.Collections.Generic;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio_Lib
{
    public delegate void SeekDelegate(long cycle);

    public class WorldManager3D:I3dWorldManager
    {

        // Delegates

        public delegate void operationOnObject(I3dObject objRef, long cycle);

        //-------------------------------------


        // Events

        public event operationOnObject ObjectChanging;
        public event operationOnObject ObjectChanged;

        //-------------------------------------

        // Fields

        bool stillInCycle;
        float cycleDuration;
        List<I3dObject> worldObjects;
        List<I3dForce> worldForces;
        I3dWorld world;
        PhysicsEngine3D physicsEngine;
        Collision_Detector<I3dObject> collisionDetector;
        
        // Laws
        CNewton2ndLaw Newton2ndLaw;
        CMotionLaw MotionLaw;
        CCollisionImpulseLaw CollisionImpulseLaw;

        //*******************************


        // Properties

        public float CycleDuration
        {
            get
            {
                return this.cycleDuration;
            }
            set
            {
                if (value > 0)
                    cycleDuration = value;

            }
        }

        public List<I3dObject> AllObjects
        {
            get
            {
                return worldObjects;
            }
            set
            {
                if (value != null)
                    worldObjects = value;
            }
        }

        public List<I3dForce> WorldForces
        {
            get
            {
                return worldForces;
            }
            set
            {
                if (value != null)
                    worldForces = value;
            }
        }

        public I3dWorld World
        {
            get { return world; }
        }

        public PhysicsEngine3D PhysicsEngine
        {
            get { return this.physicsEngine; }
        }

        public Collision_Detector<I3dObject> CollisionDetector
        {
            get { return this.collisionDetector; }
        }

        //*******************************


        // Constructers

        public WorldManager3D(I3dWorld world, float cycleDuration)
        {
            if (world == null)
                throw new NullReferenceException("Can't Construct a World Manager instance with null World reference.");
            
            this.world = world;
            if (cycleDuration > 0)
                this.cycleDuration = cycleDuration;
            else
                this.cycleDuration = 1F / 24F;
            this.stillInCycle = false;
            this.worldForces = new List<I3dForce>();
            this.worldObjects = new List<I3dObject>();
            this.physicsEngine = new PhysicsEngine3D();
            this.collisionDetector = new Collision_Detector<I3dObject>();
        }

        //*******************************


        // Methods

        public void Initialize()
        {

            this.stillInCycle = false;

            foreach (IAnalyzable objRef in worldObjects)
                objRef.StateHistory.Clear();

            Newton2ndLaw = new CNewton2ndLaw();
            MotionLaw = new CMotionLaw(this);
            CollisionImpulseLaw = new CCollisionImpulseLaw(1);

            this.WorldForces.Clear();
            this.WorldForces.Add(new CWeightForce(this.world));
            this.WorldForces.Add(new CDragForce(this.world));

            this.physicsEngine = new PhysicsEngine3D();
            this.physicsEngine.AddLaw(Newton2ndLaw);
            this.physicsEngine.AddLaw(MotionLaw);

        }

        public void Cycle(long cycle)
        {

            this.stillInCycle = true;
            if (cycle == 1614)
                stillInCycle = true;
            // Detecting Collisions
            List<List<object>> collisions = new List<List<object>>();
            if (collisionDetector.DetectAllDependingOnVelocity(out collisions, this.AllObjects,
                                      new Collision_Detector<I3dObject>.CollisionDetectorAlgorithm(collisionDetector.DetectSpherical)))
            {

                // Processing Collisions
                foreach (List<object> collision in collisions)
                {
                    if (collision.Count < 3 || !(collision[0] is I3dObject) || !(collision[1] is I3dObject) || !(collision[2] is Vector3))
                        continue;
                    
                    float dif = (float)Math.Round(((((ICollidable)collision[0]).colR + ((ICollidable)collision[1]).colR) - ~(((ICollidable)collision[0]).colP - ((ICollidable)collision[1]).colP)) / 2, 4);
                    if (dif > 0)
                    {
                        Vector3 Dif = ((ICollidable)collision[0]).colP - ((ICollidable)collision[1]).colP;
                        Dif /= ~Dif;
                        Dif *= dif;

                        ((ICollidable)collision[0]).colP += Dif;
                        ((ICollidable)collision[1]).colP -= Dif;
                    }

                    this.CollisionImpulseLaw.LawInfo = new object[] { collision[2] };
                    this.CollisionImpulseLaw.ApplyLaw(new I3dObject[] { (I3dObject)collision[0], (I3dObject)collision[1] });
                }

            }


            foreach (I3dObject objRef in this.AllObjects)
            {
                // Applying Forces to the Object
                physicsEngine.ApplyForces(objRef);

                // Performing ObjectChanging Operations before making changes
                if (this.ObjectChanging != null)
                    this.ObjectChanging(objRef, cycle);

                // Applying Physics to the Object
                physicsEngine.ApplyPhysics(objRef, null);

                // Performing ObjectChanged Operations
                if (this.ObjectChanged != null)
                    this.ObjectChanged(objRef,cycle);

            }
            
            // Check for Collision and apply the law

            this.stillInCycle = false;

        }

        public void Seek(long cycle)
        {
            foreach (IAnalyzable objRef in this.AllObjects)
            {
                objRef.SetState(cycle);
                for (long i = (objRef.StateHistory.Count - 1); i >= cycle; i--)
                {
                    if (!objRef.StateHistory.ContainsKey(i))
                        continue;
                    objRef.StateHistory.Remove(i);
                }
            }
        }
        //*****************************************
    }
}
