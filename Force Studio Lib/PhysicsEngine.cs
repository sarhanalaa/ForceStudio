﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;

namespace Force_Studio_Lib
{
    public class PhysicsEngine3D : IPhysicsEngine<I3dObject, I3dForce>
    {

        // Fields

        List<IPhysicalLaw<I3dObject, I3dForce>> laws;

        //------------------------


        // Properties

        public int Count
        {
            get { return this.laws.Count; }
        }

        //--------------------------------

        
        // Constructors

        public PhysicsEngine3D()
        {
            this.laws = new List<IPhysicalLaw<I3dObject, I3dForce>>();
        }

        //--------------------------------


        // Methods

        public bool HasLaw(IPhysicalLaw<I3dObject, I3dForce> law)
        {
            if (law == null)
                return false;
            if (this.laws.Contains(law))
                return true;

            return false;
        }

        public bool AddLaw(IPhysicalLaw<I3dObject, I3dForce> law)
        {
            if (law == null || this.laws.Contains(law))
                return false;

            try
            {

                this.laws.Add(law);
                
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveLaw(IPhysicalLaw<I3dObject, I3dForce> law)
        {
            if (law == null)
                return false;

            try
            {
                return this.laws.Remove(law);
            }
            catch
            {
                return false;
            }
        }

        public void ApplyForces(I3dObject objectRef)
        {
            if (objectRef == null)
                throw new NullReferenceException("Can't apply forces on null reference object");

            foreach (I3dForce force in objectRef.Forces)
            {
                if (force.ActingLevel == EActingLevel.OBJECT && objectRef != force.AffObject)
                    continue;
                else if (objectRef == force.AffObject)
                    force.Modify();
                else
                {
                    force.AffObject = objectRef;
                    force.Modify();
                }
            }
        }
        
        public void ApplyPhysics(I3dObject objectRef, IPhysicalLaw<I3dObject,I3dForce>[] excludedLaws)
        {

            if (objectRef == null)
                throw new NullReferenceException("Can't apply physics on null reference object");

            foreach (IPhysicalLaw<I3dObject, I3dForce> law in this.laws)
            {
                if (excludedLaws != null && excludedLaws.Contains(law))
                    continue;

                law.ApplyLaw(new I3dObject[] {objectRef} );
            }

        }

        //----------------------------

    }
}
