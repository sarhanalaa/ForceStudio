﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;


namespace ForceStudioV2 
{
    class CPendulumObject : I3dObject , ICollidable
    {
        
        
        CSphereObject sphere;
        Vector3 hangPoint;
        float threadLength;
        //--------------------------------------------

        //*************Constructor*******************
        public CPendulumObject(CSphereObject sphere, Vector3 hangPoint)
        {
            this.sphere = sphere;
            this.threadLength = ~(sphere.Position - hangPoint);
            this.sphere.Forces.Add(new CThreadTentionForce(this));
        }
        //*******Properties*****
        public float ThreadLength
        {
            get { return this.threadLength; }
            set { this.threadLength = value; }
        }

        public CSphereObject Sphere
        {
            get { return this.sphere; }
            set { this.sphere = value; }
        }

        public Vector3 HangPoint
        {
            get { return this.hangPoint; }
            set { this.hangPoint = value; }
        }

        public Vertex3 Vertice
        {
            get { return sphere.Vertice; }
            set { this.sphere.Vertice = value; }
        }

        public Vector3 V
        {
            get { return this.sphere.Velocity; }
        }

        public Vector3 P
        {
            get { return this.sphere.Position; }
        }

        public float R
        {
            get { return this.sphere.Radius; }
        }

        #region I3dObject Members

        public Vector3 Acceleration
        {
            get { return this.sphere.Acceleration ; }
            set { this.sphere.Acceleration = value; }
        }

        public Vector3 AngAcceleration
        {
            get { return sphere.AngAcceleration; }
            set { this.sphere.AngAcceleration = value; }
        }

        public Vector3 AngVelocity
        {
            get { return sphere.AngVelocity; }
            set { this.sphere.AngVelocity = value; }
        }

        public List<I3dForce> Forces
        {
            get { return sphere.Forces; }
            set { this.sphere.Forces = value; }
        }


        public float Mass
        {
            get { return sphere.Mass; }
            set
            {
                if (value > 0)
                    this.sphere.Mass = value;
            }
        }

        public Vector3 Position
        {
            get { return sphere.Position; }
            set { this.sphere.Position = value; }
        }

        public Vector3 Velocity
        {
            get { return sphere.Velocity; }
            set { this.sphere.Velocity = value; }
        }

        public I3dWorld World
        {
            get { return sphere.World; }
        }


        //**********override methods
        public void Transform(Matrix trans)
        {
            this.Sphere.Transform(trans);
            this.sphere.Vertice.Position.Transform(trans);
        }

        public float GetFaceArea(Vector3 velocity)
        {
            return this.Sphere.GetFaceArea(velocity);
        }

        public float GetDragCoefficient(Vector3 velocity)
        {
            return this.Sphere.GetDragCoefficient(velocity);
        }
        #endregion
    }
}
