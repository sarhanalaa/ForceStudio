﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ForceLib;
using ForceLib.ForceLib3D;

namespace ForceLib
{
    public class Collision_Detector<OT>
    {
        public delegate bool CollisionDetectorAlgorithm(out Vector3 collisionPoint, ICollidable obj1, ICollidable obj2);

        public bool DetectSpherical(out Vector3 collisionPoint, ICollidable sphere1 , ICollidable sphere2)
        {

            if ((~sphere1.colV == 0) && (~sphere2.colV == 0))
            {
                collisionPoint = new Vector3();
                return false;
            }
            else
            {
                float distance = ~(sphere1.colP - sphere2.colP);
                if (distance > (sphere1.colR + sphere2.colR))
                {
                    collisionPoint = new Vector3();
                    return false;
                }
                else
                {
                    Vector3 res = sphere2.colP - sphere1.colP;
                    res *= ((distance/2) / ~res);
                    collisionPoint = res + sphere1.colP;
                    return true;
                }
            }
        }


        /// <summary>
        /// Detects all the Collisions between a given list of objects which implement the ICollidable interface, and
        /// returns the collision in a list of the collision where each collision is described with the two objects
        /// and the collision point in the same order.
        /// </summary>
        /// <param name="collisions">The Collisions List out handler.</param>
        /// <param name="objectsList">The objects list to detect collisions between them.</param>
        /// <param name="detectingAlgorithm">The Collision Detection Method used to detect the collisions.</param>
        /// <returns>true if any collisions were found, false if not.</returns>
        public bool DetectAllDependingOnVelocity(out List<List<object>> collisions, List<OT> objectsList, CollisionDetectorAlgorithm detectingAlgorithm)
        {
            bool collisionsFound = false;

            List<List<object>> foundCollisions = new List<List<object>>();
            List<OT> objectsStack = new List<OT>(objectsList);

            foreach (ICollidable objRef in objectsList)
            {
                objectsStack.Remove((OT)objRef);
                if (!(objRef is ICollidable))
                    continue;

                foreach (ICollidable stObjRef in objectsStack)
                {
                    if (stObjRef == objRef)
                        continue;
                    Vector3 colPoint = new Vector3(0, 0, 0);
                    if (detectingAlgorithm(out colPoint, (ICollidable)objRef, stObjRef))
                    {
                        List<object> newCollision = new List<object>();
                        newCollision.Add(objRef);
                        newCollision.Add(stObjRef);
                        newCollision.Add(colPoint);

                        foundCollisions.Add(newCollision);

                        collisionsFound = true;
                    }
                }

            }

            // Sorting Collisions
            //  WAITING ** ** ** //


            collisions = foundCollisions;
            return collisionsFound;
        }

    }
}
