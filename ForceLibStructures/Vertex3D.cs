﻿using System;
using System.Drawing;

namespace ForceLib
{

    namespace ForceLib3D
    {

        public struct Vertex3
        {

            // Fields
            private Vector3 position;
            private Color diffuse;
            private Color specular;
            private PointF texturePosition;
            private String textureName;

            // Properties
            /// <summary>
            /// Gets or Sets the Position of this vertex.
            /// </summary>
            public Vector3 Position
            {
                get { return this.position; }
                set { this.position = value; }
            }

            /// <summary>
            /// Gets or Sets the Texture Positioning Point of this vetix.
            /// </summary>
            public PointF TexturePosition
            {
                get { return this.texturePosition; }
                set { this.texturePosition = value; }
            }

            /// <summary>
            /// Gets or Sets the Diffuse Color of this Vertix
            /// </summary>
            public Color DiffuseColor
            {
                get { return this.diffuse; }
                set { this.diffuse = value; }
            }

            /// <summary>
            /// Gets or Sets the Specular Color of this Vertix.
            /// </summary>
            public Color SpecularColor
            {
                get { return this.specular; }
                set { this.specular = value; }
            }

            public String TextureName
            {
                get { return this.textureName; }
                set { this.textureName = value; }
            }
        }

    }

}