﻿using System;
using System.Collections.Generic;

namespace ForceLib
{

    namespace ForceLib3D
    {
        /// <summary>
        /// Describes the Composition Way of the Primitive's vertices in Simulation View.
        /// </summary>
        public enum EVertexOrder { POINTS, LINELIST, LINESTRIP, LINELOOP,
                                     TRIANGLELIST, TRIANGLEFAN, TRIANGLESTRIP, 
                                     QUADLIST, QUADSTRIP, POLYGON }

        /// <summary>
        /// Cutomizes the Type of the Object to be a predefined Primitive or not (Default).
        /// Expected Primitive's Info Objects:
        /// <list>
        ///     <item>
        ///     SPHERE: { (float)Radius }
        ///     </item>
        ///     <item>
        ///     CYLINDAR: { (float)Radius, (float)Lenght}
        ///     </item>
        ///     <item>
        ///     DISK: { (float)InnerRadius, (float)OuterRadius}
        ///     </item>
        ///     <item>
        ///     PARTIALDISK: { (float)InnerRadius, (float)OuterRadius, (float)StartAngle, (float)SweepAngle}
        ///     </item>
        ///     <item>
        ///     DEFAULT: { }
        ///     </item>
        /// </list>
        /// </summary>
        public enum EPrimitiveType { SPHERE, CYLINDAR, DISK, PARTIALDISK, ARROW, DEFAULT }

        /// <summary>
        /// Represents a graphical primitive that can be used in rendering operations.
        /// </summary>
        public struct Primitive3
        {
            // Fields
            String name;
            EPrimitiveType type;
            Object[] info;
            EVertexOrder vertixOrder;
            List<Vertex3> vertices;
            Dictionary<string, string> textures;
            //---------------------

            // Properties

            /// <summary>
            /// Gets or Sets the name of the Primitive
            /// </summary>
            public String Name
            {
                get { return name; }
                set { name = value; }
            }

            /// <summary>
            /// Gets or Sets the Type of Primitive.
            /// </summary>
            public EPrimitiveType Type
            {
                get { return this.type; }
                set { this.type = value; }
            }

            /// <summary>
            /// Gets or Sets Primitive's Info Objects.
            /// </summary>
            public Object[] PrimitiveInfo
            {
                get { return this.info; }
                set { this.info = value; }
            }

            /// <summary>
            /// Gets or Sets the Composition way of primitive's vertices.
            /// </summary>
            public EVertexOrder VertexOrder
            {
                get { return this.vertixOrder; }
                set { this.vertixOrder = value; }
            }

            /// <summary>
            /// Gets or Sets the Vertices list of the primitive
            /// </summary>
            public List<Vertex3> Vertices
            {
                get { return this.vertices; }
                set { this.vertices = value; }
            }

            /// <summary>
            /// Gets or Sets the Textures Dictionary of the primitive.
            /// </summary>
            public Dictionary<string, string> Textures
            {
                get { return this.textures;}
                set { this.textures = value; }
            }
            //---------------------

            // Constructors
            public Primitive3(String name, EPrimitiveType type)
                : this()
            {
                this.Name = name;
                this.Type = type;
            }

            //---------------------

            // Methods

            public override string ToString()
            {
                return this.name;
            }

            //---------------------
        }

    }
}