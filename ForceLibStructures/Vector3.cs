﻿using System;
using System.Drawing;

namespace ForceLib
{

    public struct Vector3
    {

        // Attributes

        private float x, y, z;

        //--------------------


        // Properties

        public float X
        {
            get { return x; }
            set { x = value; }
        }

        public float Y
        {
            get { return y; }
            set { y = value; }
        }

        public float Z
        {
            get { return z; }
            set { z = value; }
        }

        //--------------------


        // Constructer

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        //--------------------

        // Operators

        public static float operator ~(Vector3 v)
        {
            return ((float)Math.Sqrt(Math.Pow(v.X, 2) + Math.Pow(v.Y, 2) + Math.Pow(v.Z, 2)));
        }

        public static Vector3 operator -(Vector3 v)
        {
            return new Vector3(-v.X, -v.Y, -v.Z);
        }

        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }

        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return (v1 + (-v2));
        }

        public static Vector3 operator *(Vector3 v, float i)
        {
            return new Vector3(v.X * i, v.Y * i, v.Z * i);
        }

        public static Vector3 operator *(float i, Vector3 v)
        {
            return (v * i);
        }

        public static Vector3 operator *(Vector3 v1, Vector3 v2)
        {
            return new Vector3((v1.Y * v2.Z) - (v1.Z * v2.Y), (v1.Z * v2.X) - (v1.X * v2.Z), (v1.X * v2.Y) - (v1.Y * v2.X));
        }

        public static Vector3 operator /(Vector3 v1, float i)
        {
            return new Vector3( (v1.X / i), (v1.Y / i), (v1.Z / i) );
        }

        public static float operator ^(Vector3 v1, Vector3 v2)
        {
            return ((v1.X * v2.X) + (v1.Y * v2.Y) + (v1.Z * v2.Z));
        }

        public static explicit operator float(Vector3 v)
        {
            return ~v;
        }

        public static bool operator ==(Vector3 v1, Vector3 v2)
        {
            if (v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z)
                return true;
            return false;
        }

        public static bool operator !=(Vector3 v1, Vector3 v2)
        {
            return !(v1 == v2);
        }

        //--------------------

        // Operations

        /// <summary>
        /// Adds two or more vectors.
        /// </summary>
        /// <param name="args">Array of vectors to add</param>
        /// <returns>The Add-Resultant vector.</returns>
        public static Vector3 Add(Vector3[] args)
        {
            Vector3 vRes = new Vector3(0F, 0F, 0F);
            foreach (Vector3 v in args)
                vRes += v;
            return vRes;
        }

        /// <summary>
        /// Subtracts two or more vectors.
        /// </summary>
        /// <param name="args">Array of vectors to generate subtract</param>
        /// <returns>The Sub-Resultant Vector</returns>
        public static Vector3 Sub(Vector3[] args)
        {
            Vector3 vRes = new Vector3(0F, 0F, 0F);
            foreach (Vector3 v in args)
                vRes -= v;
            return vRes;
        }



        /// <summary>
        /// Gets the Sine of the angle between two Vectors
        /// </summary>
        /// <param name="vec1">First Vector</param>
        /// <param name="vec2">Second Vector</param>
        public static float GetSin(Vector3 vec1, Vector3 vec2)
        {
            if (~vec1 == 0 || ~vec2 == 0)
                return float.NaN;
            return (float)Math.Sqrt(1 - Math.Pow((float)(vec1 ^ vec2) / (float)(~vec1 * ~vec2), 2));
        }

        /// <summary>
        /// Gets the Cosine of the angle between two Vectors
        /// </summary>
        /// <param name="vec1">First Vector</param>
        /// <param name="vec2">Second Vector</param>
        public static float GetCos(Vector3 vec1, Vector3 vec2)
        {
            if (~vec1 == 0 || ~vec2 == 0)
                return float.NaN;
            return ((float)(vec1 ^ vec2) / (float)(~vec1 * ~vec2));
        }

        public Matrix GetTransMatrix()
        {
            return new Matrix(new float[] { this.X, this.Y, this.Z, 1 }, true);
        }

        public Matrix GetMatrixVer()
        {
            return new Matrix(new float[] { this.X, this.Y, this.Z }, true);
        }

        public Matrix GetMatrixHor()
        {
            return new Matrix(new float[] { this.X, this.Y, this.Z }, false);
        }

        /// <summary>
        /// Transfomrs the vector using the given transformation matrix
        /// </summary>
        /// <param name="v">Vector to Transform</param>
        /// <param name="trans">Transformation Matrix</param>
        /// <returns>Transoformed Vector</returns>
        public static Vector3 Transform(Vector3 v, Matrix trans)
        {
            float x = trans.Cell[0][0] * v.X + trans.Cell[1][0] * v.Y + trans.Cell[2][0] * v.Z + trans.Cell[3][0];
            float y = trans.Cell[0][1] * v.X + trans.Cell[1][1] * v.Y + trans.Cell[2][1] * v.Z + trans.Cell[3][1];
            float z = trans.Cell[0][2] * v.X + trans.Cell[1][2] * v.Y + trans.Cell[2][2] * v.Z + trans.Cell[3][2];

            return new Vector3(x, y, z);
        }

        public override string ToString()
        {
            return "( " + this.X + ", " + this.Y + ", " + this.Z + ")";
        }


        public static Vector3 LeftHandRotaionNormal(Vector3 v1, Vector3 v2)
        {
            Vector3 v = v1 * v2;
            if (~v == 0)
                return new Vector3(0, 0, 0);

            if (v.x < 1)
                v.x = (float)Math.Round(v.X, 4);
            if (v.Y < 1)
                v.y = (float)Math.Round(v.Y, 4);
            if (v.Z < 1)
                v.z = (float)Math.Round(v.Z, 4);
            
            return (v / ~v);
        }

        public static Vector3 PerpendicularNormal(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p)
        {
            Vector3 axis = p1 - p2;

            float A = (float)Math.Round((p2.Y - p1.Y) * (p3.Z - p1.Z) - (p2.Z - p1.Z) * (p3.Y - p1.Y), 2);
            float B = (float)Math.Round((p2.Z - p1.Z) * (p3.X - p1.X) - (p2.X - p1.X) * (p3.Z - p1.Z), 2);
            float C = (float)Math.Round((p2.X - p1.X) * (p3.Y - p1.Y) - (p2.Y - p1.Y) * (p3.X - p1.X), 2);

            Vector3 pointOnPerpendicular = new Vector3();

            if (A == 0)
                pointOnPerpendicular.X = p.X;
            else
                pointOnPerpendicular.X = p.X - 1;

            if (B == 0)
                pointOnPerpendicular.Y = p.Y;
            else if (A != 0)
                pointOnPerpendicular.Y = (pointOnPerpendicular.X - p.X) * (B / A) + p.Y;
            else
                pointOnPerpendicular.Y = p.Y - 1;

            if (C == 0)
                pointOnPerpendicular.Z = p.Z;
            else if (B != 0)
                pointOnPerpendicular.Z = (pointOnPerpendicular.Y - p.Y) * (C / B) + p.Z;
            else if (A != 0)
                pointOnPerpendicular.Z = (pointOnPerpendicular.X - p.X) * (C / A) + p.Z;
            else
                pointOnPerpendicular.Z = p.Z - 1;

            Vector3 RotatedPerpendicularNormal = (pointOnPerpendicular - p) / ~(pointOnPerpendicular - p);

            Matrix rotationMatrix = Matrix.RotationMatrix(axis / ~axis, (float)(Math.PI / -2));

            return Vector3.Transform(RotatedPerpendicularNormal, rotationMatrix);

        }

        //--------------------
    }

}