﻿using System;
using System.Drawing;

namespace ForceLib
{

    public struct Matrix
    {

        // Attributes

        private int rows, cols;
        private float[][] cells;
        private float determinant;

        //----------------

        // Constructers

        public Matrix(int rows, int cols)
        {
            this.rows = rows;
            this.cols = cols;
            this.determinant = 0F;
            this.cells = new float[this.rows][];
            for (int i = 0; i < this.rows; i++)
                this.cells[i] = new float[this.cols];
        }

        public Matrix(float[] arr, bool vertical)
        {
            if (!vertical)
            {
                this.rows = 1;
                this.cols = arr.Length;
                this.determinant = 0;

                this.cells = new float[this.rows][];
                for (int i = 0; i < this.rows; i++)
                    this.cells[i] = new float[this.cols];

                for (int i = 0; i < this.cols; i++)
                {
                    this.cells[0][i] = arr[i];
                }

            }
            else
            {
                this.cols = 1;
                this.rows = arr.Length;
                this.determinant = 0;

                this.cells = new float[this.rows][];
                for (int i = 0; i < this.rows; i++)
                    this.cells[i] = new float[this.cols];

                for (int i = 0; i < this.rows; i++)
                {
                    this.cells[i][0] = arr[i];
                }
            }

            this.CalcDet();
        }

        //----------------

        // Properties

        public int Rows
        {
            get { return this.rows; }
        }

        public int Cols
        {
            get { return this.cols; }
        }

        public float Determinant
        {
            get
            {
                if (this.rows != this.cols) throw new Exception("Calling Determinant for non square matrix");
                else return this.determinant;
            }
        }


        public float[][] Cell
        {
            get { return this.cells; }
        }

        //----------------


        // Operators

        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            if (m1.rows != m2.rows && m1.cols != m2.cols)
                throw new Exception("Adding two matrices with different dimension");
            Matrix res = new Matrix(m1.Rows, m1.Cols);
            for (int i = 0; i < res.Rows; i++)
                for (int j = 0; j < res.Cols; j++)
                    res.Cell[i][j] = m1.Cell[i][j] + m2.Cell[i][j];

            res.CalcDet();

            return res;
        }

        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            if (m1.rows != m2.rows && m1.cols != m2.cols)
                throw new Exception("Subtracting two matrices with different dimension");
            Matrix res = new Matrix(m1.Rows, m1.Cols);
            for (int i = 0; i < res.Rows; i++)
                for (int j = 0; j < res.Cols; j++)
                    res.Cell[i][j] = m1.Cell[i][j] - m2.Cell[i][j];

            res.CalcDet();

            return res;
        }

        public static Matrix operator *(Matrix m, float x)
        {
            Matrix res = new Matrix(m.Rows, m.Cols);
            for (int i = 0; i < res.Rows; i++)
                for (int j = 0; j < res.Cols; j++)
                    res.Cell[i][j] = x * m.Cell[i][j];

            res.CalcDet();

            return res;
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1.cols != m2.rows)
                throw new Exception("Multiply Can't Be Done, Unsuitable Dimensions");
            Matrix res = new Matrix(m1.Rows, m2.Cols);
            for (int i = 0; i < m1.Rows; i++)
                for (int k = 0; k < m2.Cols; k++)
                {
                    res.Cell[i][k] = 0;
                    for (int j = 0; j < m1.Cols; j++)
                        res.Cell[i][k] += m1.Cell[i][j] * m2.Cell[j][k];
                }

            res.CalcDet();

            return res;
        }

        public static Matrix operator /(Matrix m1, Matrix m2)
        {
            Matrix Inv = m2.GetInverted();
            Matrix res = m1 * Inv;

            res.CalcDet();

            return res;
        }

        //----------------


        // Operations

        private float CalcDet()
        {
            if (this.Rows != this.Cols)
                return 0.0F;

            if (this.Rows == 1 && this.Cols == 1)
                this.determinant = this.Cell[0][0];
            else
            {
                for (int i = 0; i < this.Cols; i++)
                {
                    int j = (i % 2 == 0) ? 1 : -1;
                    Matrix minor = this.Minor(0, i);
                    this.determinant += j * this.Cell[0][i] * (minor.CalcDet());
                }
            }
            return this.determinant;
        }

        private Matrix Minor(int row, int col)
        {
            Matrix res = new Matrix(this.Rows - 1, this.Cols - 1);
            int k = 0;
            for (int i = 0; i < this.Rows; i++)
                if (i != row)
                {
                    int l = 0;
                    for (int j = 0; j < this.Cols; j++)
                        if (j != col) res.Cell[k][l++] = this.Cell[i][j];
                    k++;
                }
            return res;
        }

        public Matrix GetInverted()
        {
            float Det = this.CalcDet();
            if (Det == 0)
                throw new Exception("Can't Be Inverted");
            else
            {

                Matrix res = new Matrix(this.Rows, this.Cols);
                for (int i = 0; i < this.Rows; i++)
                    for (int j = 0; j < this.Cols; j++)
                        res.Cell[i][j] = this.Cell[i][j];

                for (int i = 1; i < this.Rows; i++)
                    for (int j = 0; j < i; j++)
                    {
                        float Temp = res.Cell[i][j];
                        res.Cell[i][j] = res.Cell[j][i];
                        res.Cell[j][i] = Temp;
                    }
                for (int i = 0; i < this.Rows; i++)
                    for (int j = 0; j < this.Cols; j++)
                        res.Cell[i][j] /= Det;
                return res;
            }
        }


        /// <summary>
        /// Returns a 4x4 trasformation martix used to apply a rotation arround a given axis
        /// with a given angle in radians.
        /// </summary>
        /// <param name="n">Unit Vector of the Axis.</param>
        /// <param name="o">Rotation Angle, measured in radians.</param>
        /// <returns>Rotation Matrix Arround n by o radians.</returns>
        public static Matrix RotationMatrix(Vector3 n, float o)
        {
            Matrix rotationMatrix = new Matrix(4, 4);

            rotationMatrix.Cell[0][0] = (float)Math.Round((Math.Pow(n.X, 2) * (1 - Math.Cos(o)) + Math.Cos(o)),4);
            rotationMatrix.Cell[0][1] = (float)Math.Round((n.X * n.Y * (1 - Math.Cos(o)) + n.Z * Math.Sin(o)),4);
            rotationMatrix.Cell[0][2] = (float)Math.Round((n.X * n.Z * (1 - Math.Cos(o)) - n.Y * Math.Sin(o)),4);
            rotationMatrix.Cell[0][3] = 0;

            rotationMatrix.Cell[1][0] = (float)Math.Round((n.X * n.Y * (1 - Math.Cos(o)) - n.Z * Math.Sin(o)),4);
            rotationMatrix.Cell[1][1] = (float)Math.Round((Math.Pow(n.Y, 2) * (1 - Math.Cos(o)) + Math.Cos(o)),4);
            rotationMatrix.Cell[1][2] = (float)Math.Round((n.Y * n.Z * (1 - Math.Cos(o)) + n.X * Math.Sin(o)), 4);
            rotationMatrix.Cell[1][3] = 0;

            rotationMatrix.Cell[2][0] = (float)Math.Round((n.X * n.Z * ( (1 - Math.Cos(o)) + n.Y * Math.Sin(o))),4);
            rotationMatrix.Cell[2][1] = (float)Math.Round((n.Y * n.Z * (1 - Math.Cos(o)) - n.X * Math.Sin(o)),4);
            rotationMatrix.Cell[2][2] = (float)Math.Round((Math.Pow(n.Z, 2) * (1 - Math.Cos(o)) + Math.Cos(o)),4);
            rotationMatrix.Cell[2][3] = 0;

            rotationMatrix.Cell[3][0] = 0;
            rotationMatrix.Cell[3][1] = 0;
            rotationMatrix.Cell[3][2] = 0;
            rotationMatrix.Cell[3][3] = 1;

            return rotationMatrix;
        }

        /// <summary>
        /// Returns a 4x4 rotation matrix arround the x, y and z axices, with given 
        /// angles represented by angles vector.
        /// </summary>
        /// <param name="anglesVector">angles represented by a vector.</param>
        /// <returns>Rotation Matrix Arround x, y and z axices.</returns>
        public static Matrix RotationMatrix(Vector3 anglesVector)
        {
            Matrix rotationMatrix = new Matrix(4, 4);

            rotationMatrix.Cell[0][0] = (float) Math.Round((Math.Cos(anglesVector.Y) * Math.Cos(anglesVector.Z)),4);
            rotationMatrix.Cell[0][1] = (float)Math.Round((Math.Cos(anglesVector.Y) * Math.Sin(anglesVector.Z)),4);
            rotationMatrix.Cell[0][2] = (float)Math.Round((-Math.Sin(anglesVector.Y)),4);
            rotationMatrix.Cell[0][3] = 0;

            rotationMatrix.Cell[1][0] = (float)Math.Round((Math.Cos(anglesVector.Z) * Math.Sin(anglesVector.X) * Math.Sin(anglesVector.Y) - Math.Cos(anglesVector.X) * Math.Sin(anglesVector.Z)),4);
            rotationMatrix.Cell[1][1] = (float)Math.Round((Math.Sin(anglesVector.X) * Math.Sin(anglesVector.Y) * Math.Sin(anglesVector.Z) + Math.Cos(anglesVector.X) * Math.Cos(anglesVector.Z)),4);
            rotationMatrix.Cell[1][2] = (float)Math.Round((Math.Cos(anglesVector.Y) * Math.Sin(anglesVector.X)),4);
            rotationMatrix.Cell[1][3] = 0;

            rotationMatrix.Cell[2][0] = (float)Math.Round((Math.Cos(anglesVector.X) * Math.Cos(anglesVector.Z) * Math.Sin(anglesVector.Y) + Math.Sin(anglesVector.X) * Math.Sin(anglesVector.Z)),4);
            rotationMatrix.Cell[2][1] = (float)Math.Round((Math.Cos(anglesVector.X) * Math.Sin(anglesVector.Y) * Math.Sin(anglesVector.Z) - Math.Cos(anglesVector.Z) * Math.Sin(anglesVector.X)),4);
            rotationMatrix.Cell[2][2] = (float)Math.Round((Math.Cos(anglesVector.X) * Math.Cos(anglesVector.Y)),4);
            rotationMatrix.Cell[2][3] = 0;
            
            rotationMatrix.Cell[3][0] = 0;
            rotationMatrix.Cell[3][1] = 0;
            rotationMatrix.Cell[3][2] = 0;
            rotationMatrix.Cell[3][3] = 1;

            return rotationMatrix;
        }
        //----------------

    }

}